<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSalarySettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_settings', function (Blueprint $table) {
            $table->integer('min_basic')->after('grade');
            $table->integer('max_basic')->after('grade');
            $table->integer('min_hra')->after('grade');
            $table->integer('max_hra')->after('grade');
            $table->integer('min_medical')->after('grade');
            $table->integer('max_medical')->after('grade');
            $table->integer('min_food')->after('grade');
            $table->integer('max_food')->after('grade');
            $table->integer('min_mobile')->after('grade');
            $table->integer('max_mobile')->after('grade');
            $table->integer('min_conveyance')->after('grade');
            $table->integer('max_conveyance')->after('grade');
            $table->integer('min_special')->after('grade');
            $table->integer('max_special')->after('grade');
            $table->integer('min_car')->after('grade');
            $table->integer('max_car')->after('grade');
            $table->integer('min_others')->after('grade');
            $table->integer('max_others')->after('grade');
            $table->integer('attendance_bonus')->after('grade')->default(0);
            $table->enum('ot_status',['Yes','No'])->after('grade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_settings', function (Blueprint $table) {
            $table->dropColumn(['min_basic','max_basic','min_hra','max_hra','min_medical','max_medical','min_food',
                'max_food','min_mobile','max_mobile','min_conveyance','max_conveyance','min_special','max_special',
                'min_car','max_car','min_others','max_others','ot_status','attendance_bonus']);
        });
    }
}
