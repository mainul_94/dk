<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHolidayChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('holiday_children', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('holiday_type_id',0,1);
            $table->integer('holiday_id',0,1);
            $table->date('date');
            $table->text('details');
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('holiday_type_id')->references('id')->on('holiday_types');
            $table->foreign('holiday_id')->references('id')->on('holidays');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('holiday_children', function (Blueprint $table) {
            $table->dropForeign(['holiday_type_id']);
            $table->dropForeign(['holiday_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });

        Schema::drop('holiday_children');
    }
}
