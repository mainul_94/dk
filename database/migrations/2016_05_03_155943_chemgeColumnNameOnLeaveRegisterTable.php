<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChemgeColumnNameOnLeaveRegisterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('leave_registers', function (Blueprint $table) {
            $table->dropColumn('start_date')->after('employee_id');
            $table->dropColumn('end_date')->after('employee_id');
            $table->dropColumn('days')->after('employee_id');
            $table->date('p_start_date')->after('employee_id');
            $table->date('p_end_date')->after('employee_id');
            $table->float('p_days')->after('employee_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_registers', function (Blueprint $table) {
            $table->dropColumn('p_start_date')->after('employee_id');
            $table->dropColumn('p_end_date')->after('employee_id');
            $table->dropColumn('p_days')->after('employee_id');
            $table->date('start_date')->after('employee_id');
            $table->date('end_date')->after('employee_id');
            $table->float('days')->after('employee_id');
        });
    }
}
