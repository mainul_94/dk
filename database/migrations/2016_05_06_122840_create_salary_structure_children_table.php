<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalaryStructureChildrenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salary_structure_children', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('salary_structure_id',0,1);
            $table->integer('salary_type_id',0,1);
            $table->double('amount');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('salary_structure_id')->references('id')->on('salary_structures');
            $table->foreign('salary_type_id')->references('id')->on('salary_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_structure_children', function (Blueprint $table) {
            $table->dropForeign(['salary_structure_id']);
            $table->dropForeign(['salary_type_id']);
        });

        Schema::drop('salary_structure_children');
    }
}
