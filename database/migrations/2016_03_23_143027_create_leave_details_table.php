<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leave_register_id',0,1);
            $table->integer('leave_type_id',0,1);
            $table->date('start_date');
            $table->date('end_date');
            $table->float('days');
            $table->enum('payable',['Yes','No']);
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('leave_register_id')->references('id')->on('leave_registers');
            $table->foreign('leave_type_id')->references('id')->on('leave_types');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('leave_details', function (Blueprint $table) {
            $table->dropForeign(['leave_register_id']);
            $table->dropForeign(['leave_type_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });

        Schema::drop('leave_details');
    }
}
