<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProbotionPreodInSalarySettingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('salary_settings', function (Blueprint $table) {
            $table->integer('probation_period_id',0,1)->after('employee_type_id');
            $table->integer('holiday_bonus')->after('employee_type_id')->default(0);
//            $table->foreign('probation_period_id')->references('id')->on('probation_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salary_settings', function (Blueprint $table) {
            $table->dropColumn(['probation_period_id','holiday_bonus']);
//            $table->dropForeign(['probation_period_id']);
        });
    }
}
