<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year',0,1);
            $table->integer('month');
            $table->integer('employee_id',0,1);
//            Earning
            $table->integer('gross_salary')->default(0);
            $table->integer('basic_salary')->default(0);
            $table->integer('hra')->default(0);
            $table->integer('medical')->default(250);
            $table->integer('food_all')->default(650);
            $table->integer('conveyance')->default(200);
            $table->integer('mobile_bill')->default(0);
            $table->integer('special_bill')->default(0);
            $table->integer('car_bill')->default(0);
            $table->integer('others')->default(0);
            $table->integer('incentive')->default(0);
            $table->integer('attendance_bonus')->default(0);
            $table->integer('ab_deduction')->default(0); // GrossOrBasic/TotalDaysOfMonth*total_absent_days
            $table->double('ot_rate')->default(0);
            $table->integer('ot_hours')->default(0);
            $table->integer('ext_ot_hours')->default(0);
            $table->integer('ot_amount')->default(0);
            $table->integer('ext_ot_amount')->default(0);
            $table->integer('holiday_all')->default(0);
            $table->integer('arrear')->default(0);
            $table->integer('fest_bonus')->default(0);
//            Deduction
            $table->integer('advance')->default(0);
            $table->integer('others_deduction')->default(0);
            $table->integer('tax')->default(0);
            $table->integer('revenue_stamp')->default(10);
//            Total
            $table->integer('payable_amount')->default(0);
//            Days
            $table->double('total_days');
            $table->double('p_days');
            $table->double('ab_days');
            $table->double('payable_days');
//            Timestamps
            $table->integer('created_by')->unsigned();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('employee_id')->references('id')->on('employees');
            $table->foreign('created_by')->references('id')->on('users');
            $table->foreign('updated_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('salaries', function (Blueprint $table) {
            $table->dropForeign(['employee_id']);
            $table->dropForeign(['created_by']);
            $table->dropForeign(['updated_by']);
        });

        Schema::drop('salaries');
    }
}
