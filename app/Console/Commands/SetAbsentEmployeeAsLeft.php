<?php

namespace App\Console\Commands;

use App\Employee;
use App\EmployeeCategory;
use Illuminate\Console\Command;

class SetAbsentEmployeeAsLeft extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'employee:left';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $emp_cat = EmployeeCategory::where('title','Lefty')->first();
        $employees = Employee::whereNot('employee_category_id',$emp_cat->id)->get();
        $bar = $this->output->createProgressBar(count($employees));
        foreach ($employees as $employee) {

            $bar->advance();
        }

        $bar->finish();

    }

    public function setEmployeeAsLeft($employee)
    {
        $holidays = $employee->holiday->children()->where();
    }
}
