<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class HolidayType extends Model
{
    protected $fillable = ['name','details','created_by','updated_by'];


    use CommonField;
}
