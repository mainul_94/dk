<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class leaveType extends Model
{
    protected $fillable = ['name','is_maternity','is_earn','is_payable','details','created_by','updated_by'];

    use CommonField;
}
