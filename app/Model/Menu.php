<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    protected $fillable = ['title','slug','icon','url','order_id','parent_id'];


    /**
     * Return Child's Parent ID
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('\App\Menu','parent_id');
    }


    /**
     * Get All childs ID
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function chield()
    {
        return $this->hasMany('\App\Menu','parent_id');
    }


    /**
     * Set Slug
     */

    public function setSlugAttribute()
    {
        $this->attributes['slug'] = $this->getAttributeValue('title');
    }


    public function setParentIdAttribute($data)
    {
        if (empty($data)) {
            $this->attributes['parent_id']=null;
        }
        else{
            $this->attributes['parent_id']=$data;
        }
    }
}
