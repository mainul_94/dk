<?php

namespace App;

use App\Model\CommonField;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = ['name','card_no','old_card_no','organization_id','unit_id','division_id','department_id','section_id',
        'line_id','designation_group_id','designation_id','employee_category_id','employee_type_id','holiday_id',
        'probation_period_id','sex','fathers_name','mothers_name','husband_name','photo','permanent_address','bangla_permanent_address',
        'present_address','bangla_present_address','contact_no','emergency_contact_no','national_id','nationality','date_of_birth',
        'religion','marital_status','number_of_children','name_of_nominees','address_of_nominees','relation_with_nominees',
        'authorized_percent_for_nominees','last_organization','date_of_joining','joining_salary','current_salary','increment_month',
        'increment_date','last_increment_date', 'reason_of_separation','date_of_separation','confirm_date','payment_mode',
        'bank_name','branch_name','account_holder','account_no','gross_salary','basic_salary','hra','medical','food_all',
        'conveyance','mobile_bill','special_bill','car_bill','others','incentive','status','created_by','updated_by'];//'ot_status','attendance_bonus',

    use CommonField;

//    protected $dates =['date_of_joining','date_of_separation','confirm_date'];
//
//    public function setDateOfJoiningAttribute($value)
//    {
//        $this->attributes['date_of_joining'] = Carbon::createFromFormat('m/d/Y', $value);
//    }
//
//    public function setDateOfSeparationAttribute($value)
//    {
//        $this->attributes['date_of_separation'] = Carbon::createFromFormat('m/d/Y', $value);
//    }
//
//    public function setConfirmDateAttribute($value)
//    {
//        $this->attributes['confirm_date'] = Carbon::createFromFormat('m/d/Y', $value);
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function attendance()
    {
        return $this->hasMany('App\Attendance');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getEmployeeOrganizationInfo()
    {

        return $this->hasOne('App\EmployeeOrgProfile');

    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function getEmployeePersonalInfo()
    {

        return $this->hasOne('App\EmployeePersonaInfo');

    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function salaries()
    {
        return $this->hasMany('App\Salary');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function leave()
    {
        return $this->hasMany('App\leaveRegister');
    }


    public function scopeActive($query)
    {
        $active_categories = EmployeeCategory::whereIn('name', ['Regular', 'Lefty', 'New'])->lists('id');
        return $query->whereIn('employee_category_id', $active_categories);
    }

}
