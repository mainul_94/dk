<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Designation extends Model
{
    protected $fillable = ['name','grade','capacity_of_employee','organization_id','unit_id','division_id',
        'department_id','section_id','line_id','designation_group_id','created_by','updated_by'];


    use CommonField;


    public function designationGroup()
    {
        return $this->belongsTo('App\DesignationGroup');
    }


    public function employee()
    {
        return $this->hasMany('App\Employee');
    }
}
