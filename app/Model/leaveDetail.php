<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class leaveDetail extends Model
{
    protected $fillable =['leave_register_id','leave_type_id','start_date','end_date','days','payable','created_by','updated_by'];


    use CommonField;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Get Parent Leave
     */
    public function leaveRegister()
    {
        return $this->belongsTo('App\leaveRegister');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Get Leave Type
     */
    public function leaveType()
    {
        return $this->belongsTo('App\leaveType');
    }
}
