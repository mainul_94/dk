<?php

namespace App;

//use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\DesignationGroup;
use App\Department;
use App\Division;
use App\Line;
use App\Organization;
use App\Section;
use App\Unit;

class ImportEmpoyee extends Excel
{
    public static function createDesignationAndSetRowValues($row,$creator)
    {
        $designation = Designation::where('name','=',$row->designation)->first();
        if (empty($designation)) {
            $designation = new Designation;
            $dg_val = ['name'=>$row->designation,'created_by'=>$creator];
            $dg_group = DesignationGroup::where('name','=',$row->designation_group)->first();
            if (empty($dg_group)) {
                $dg_group = new DesignationGroup;
                $dg_group->fill(['name'=>$row->designation_group,'created_by'=>$creator])->save();
            }
            $dg_val['designation_group_id'] = $dg_group->id;
            $line = Line::where('name','=',$row->line)->first();
            if (empty($line)) {
                $line = new Line;
                $line_val = ['name'=>$row->line,'created_by'=>$creator];
                $section = Section::where('name','=',$row->section)->first();
                if (empty($section)) {
                    $section = new Section;
                    $sec_val = ['name'=>$row->section,'created_by'=>$creator];
                    $dept = Department::where('name','=',$row->department)->first();
                    if (empty($dept)) {
                        $dept = new Department;
                        $dept_val = ['name'=>$row->department,'created_by'=>$creator];

                        $division = Division::where('name','=',$row->devision)->first();
                        if (empty($division)) {
                            $division = new Division;
                            $divi_val = ['name'=>$row->division,'created_by'=>$creator];
                            $unit = Unit::where('name','=',$row->unit)->first();
                            if (empty($unit)) {
                                $unit = new Unit;
                                $unit_val = ['name'=>$row->unit,'created_by'=>$creator];
                                $org = Organization::where('name','=',$row->organization)->first();
                                if (empty($org)) {
                                    $org = new  Organization;
                                    $org->fill(['name'=>$row->organization,'created_by'=>$creator])->save();
                                }
                                $unit_val['organization_id'] = $org->id;
                                $unit->fill($unit_val)->save();
                            }
                            $divi_val['organization_id']= $unit->organization_id;
                            $divi_val['unit_id']= $unit->id;
                            $division->fill($divi_val)->save();
                        }
                        $dept_val['organization_id']= $division->organization_id;
                        $dept_val['unit_id']= $division->unit_id;
                        $dept_val['division_id']= $division->id;
                        $dept->fill($dept_val)->save();
                    }
                    $sec_val['organization_id']= $dept->organization_id;
                    $sec_val['unit_id']= $dept->unit_id;
                    $sec_val['division_id']= $dept->division_id;
                    $sec_val['department_id']= $dept->id;
                    $section->fill($sec_val)->save();
                }
                $line_val['organization_id']= $section->organization_id;
                $line_val['unit_id']= $section->unit_id;
                $line_val['department_id']= $section->department_id;
                $line_val['division_id']= $section->division_id;
                $line_val['section_id']= $section->id;
                $line->fill($line_val)->save();
            }
            $dg_val['organization_id']= $line->organization_id;
            $dg_val['unit_id']= $line->unit_id;
            $dg_val['department_id']= $line->department_id;
            $dg_val['division_id']= $line->division_id;
            $dg_val['section_id']= $line->section_id;
            $dg_val['line_id']= $line->id;
            $dg_val['grade']= $row->grade;
            $designation->fill($dg_val)->save();
        }
        $row['designation_id'] = $designation->id;
        $emp_cat = EmployeeCategory::where('name','=',$row->employee_category)->first();
        if (empty($emp_cat)) {
            $emp_cat = new EmployeeCategory;
            $emp_cat->fill(['name'=>$row->employee_category,'created_by'=>$creator])->save();
        }
        $row['employee_category_id'] = $emp_cat->id;
        $holiday_list = Holiday::where('name','=',$row->holiday)->first();
        if (empty($holiday_list)) {
            $holiday_list = new Holiday;
            $holiday_list->fill(['name'=>$row->holiday,'created_by'=>$creator])->save();
        }
        $row['holiday_id'] = $holiday_list->id;
        $emp_type = EmployeeType::where('name','=',$row->employee_type)->first();
        if (empty($emp_type)) {
            $emp_type = new EmployeeType;
            $emp_type->fill(['name'=>$row->employee_type,'created_by'=>$creator])->save();
        }
        $row['employee_type_id'] = $emp_type->id;

        $proation = ProbationPeriod::where('name','=',$row->probation_period)->first();
        if (empty($proation)) {
            $proation = new ProbationPeriod;
            $proation->fill(['name'=>$row->probation_period,'created_by'=>$creator])->save();
        }
        $row['probation_period_id'] = $proation->id;
        $row['old_card_no']=$row->card_no;
        $row['joining_salary'] = $row->gross_salary;
        $row['current_salary'] = $row->gross_salary;
        $row['basic_salary'] = $row->basic;
        $row['date_of_joining'] = Carbon::parse($row->date_of_join);
        $row['date_of_separation'] = Carbon::parse($row->date_of_separation);
        return $row;
    }
}
