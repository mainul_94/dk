<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class SalaryStructure extends Model
{
    protected $fillable = ['employee_id','start_date','end_date','total_earning','total_deduction','gross_total',
        'status','created_by','updated_by'];

    use CommonField;


    /**
     * Get Employee
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }


    /**
     * Get Children
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\SalaryStructureChild');
    }
}
