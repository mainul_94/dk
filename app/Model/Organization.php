<?php

namespace App;

use App\Http\Requests\Request;
use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
    protected $fillable = ['name','address','created_by','updated_by'];

    use CommonField;


    /**
     * Relation with Unit
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */

    public function unit()
    {
        return $this->hasOne('App\Unit');
    }


    /*public function setCreatedByAttribute($value)
    {
        $this->attributes['created_by'] = Request::user()->id;
    }*/

}