<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    /**
     * @param $date
     * @param $section
     * @param $designation
     * @return array|\Illuminate\Database\Eloquent\Collection|static[]
     */
    public static function attendanceSummary($date,$section,$designation)
    {
        if (!empty($designation)) {
            $designation = Designation::findOrFail($designation);
            $section = $designation->section;
        }else if (!empty($section)) {
            $section = Section::findOrFail($section);
        }else{
            $sections = Section::all();
        }
        if (!empty($designation)) {
            $rows = [];
            $section['total'] = 0;
            $section['present'] = 0;
            $section['absent'] = 0;
            $row = Report::attendanceRenderRow($section,$designation,$date);
            array_push($rows,collect($row));
            $section['designations'] = collect($rows);
        } elseif (!empty($section) && empty($designation)) {
            $designations = Designation::where('section_id',$section->id)->get();
            $rows = [];
            $section['total'] = 0;
            $section['present'] = 0;
            $section['absent'] = 0;
            foreach ($designations as $designation) {
                $row = Report::attendanceRenderRow($section,$designation,$date);
                array_push($rows,collect($row));
            }
            $section['designations'] = collect($rows);
        }else
        {
            foreach ($sections as $section) {
                $designations = Designation::where('section_id',$section->id)->get();
                $rows = [];
                $section['total'] = 0;
                $section['present'] = 0;
                $section['absent'] = 0;
                foreach ($designations as $designation) {
                    $row = Report::attendanceRenderRow($section,$designation,$date);
                    array_push($rows,collect($row));
                }
                $section['designations'] = collect($rows);
            }
        }

        if (empty($sections)) {
            $sections = [$section];
        }

        return $sections;
    }

    /**
     * @param $section
     * @param $designation
     * @param $date
     * @return array
     */
    public static function attendanceRenderRow($section,$designation,$date)
    {
        $attendance = Attendance::where('date',$date)->whereIn('employee_id',$designation->employee->lists('id'))->get();
        $total = $attendance->count();
        $present = $attendance->where('status','Present')->count();
        $absent = $total - $present;
        $section->total += $total;
        $section->present += $present;
        $section->absent += $absent;
        return ['name'=>$designation->name,'total'=>$total,'present'=>$present,'absent'=>$absent];
    }

    
}
