<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class EmployeeLeaveAssign extends Model
{
    protected $fillable = ['employee_id','leave_type_id','leave_days','year','created_by','updated_by'];

    use CommonField;

    /**
     * Relation with Employee
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }

    /**
     * Relation with leaveType
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaveType()
    {
        return $this->belongsTo('App\leaveType');
    }
}
