<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class SalarySetting extends Model
{
    protected $fillable = ['designation_id','department_id','section_id','employee_type_id','all_bonus','grade',
        'created_by','updated_by','min_basic','max_basic','min_hra','max_hra','min_medical','max_medical','min_food',
        'max_food','min_mobile','max_mobile','min_conveyance','max_conveyance','min_special','max_special',
        'min_car','max_car','min_others','max_others','attendance_bonus','ot_status','probation_period_id','holiday_bonus'];

    use CommonField;


    public function children()
    {
        return $this->hasMany('App\SalarySettingChild');
    }
}
