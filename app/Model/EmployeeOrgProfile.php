<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class EmployeeOrgProfile extends Model
{
    protected $fillable = ['name_of_nominees','address_of_nominees','relation_with_nominees','authorized_percent_for_nominees',
        'last_organization','date_of_joining','joining_salary','current_salary','increment_month','reason_of_separation',
        'date_of_separation','confirm_date','payment_mode','bank_name','branch_name','account_holder','account_no','employee_id',
        'created_by','updated_by'];


    use CommonField;
}
