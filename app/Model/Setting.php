<?php

namespace App;
use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    protected $fillable = ['property','value','created_by','updated_by'];

    use CommonField;
}
