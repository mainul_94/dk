<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalarySettingChild extends Model
{
    protected $fillable =['salary_setting_id','salary_type_id','min_amount','max_amount'];
}
