<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class EmployeePersonaInfo extends Model
{
    protected $fillable =['fathers_name','mothers_name','husband_name','photo','permanent_address','bangla_permanent_address',
        'present_address','bangla_present_address','contact_no','emergency_contact_no','national_id','nationality',
        'date_of_birth','religion','marital_status','number_of_children','employee_id','created_by','updated_by'];


    use CommonField;



}
