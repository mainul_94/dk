<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    protected $fillable = ['name','address','organization_id','unit_id','division_id','department_id','created_by','updated_by'];




    use CommonField;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function employee()
    {
        return $this->hasMany('App\Employee');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function allLines()
    {
        return $this->hasMany('App\Line');
    }
}
