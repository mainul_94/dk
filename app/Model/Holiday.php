<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class Holiday extends Model
{
    protected $fillable = ['name','year','created_by','updated_by'];


    use CommonField;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany('App\HolidayChild','holiday_id','id')->orderBy('date','asc');
    }
}
