<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Model\CommonField;
class Attendance extends Model
{

    protected $fillable = ['employee_id','date','in_time','out_time','remark','status','duration','let_time','overtime','created_by','updated_by'];

    protected $visible = ['employee_id','date','status','duration','let_time','overtime'];

    use CommonField;

    /**
     * Realation with Employee
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function employee()
    {
       return $this->belongsTo('App\Employee');
    }


    /**
     * Relation with User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /*public function user()
    {
        return $this->belongsTo('App\User');
    }*/

    public function scopeAttendanceFiltersReport($query, $month, $year, $request)
    {
        $query->whereMonth('date','=',$month);
        $query->whereYear('date','=',$year);
        $employee = $request->get('employee_id');
        if ($employee) {
            if (gettype($employee) == 'array') {
                $query->whereIn('employee_id',$employee);
            }else{
                $query->where('employee_id',$employee);
            }
        }
        return $query;
    }
}
