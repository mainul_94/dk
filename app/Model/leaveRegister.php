<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class leaveRegister extends Model
{
    protected $fillable = ['employee_id','p_start_date','p_end_date','p_days','leave_registers','status',
        'is_permanent_save','leave_approval','created_by','updated_by'];

    use CommonField;


    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     * Get Leave Details
     */
    public function details()
    {
        return $this->hasMany('App\leaveDetail');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * Get Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Employee');
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function leaveApproval()
    {
        return $this->belongsTo('App\User','leave_approval');
    }


    public static function earnLeaveOpening($employee_id)
    {
//        Need to check 1 year Pass;
        $employee = Employee::find($employee_id);
        if ((date('Y')-date('Y',strtotime($employee->date_of_joining)))<1) {
            return 0;
        }
//        Need to check 1 year Pass;
        $opening = DB::table('earn_leave_opening_balance')->where('employee_id',$employee_id)->first();
        $balance_day = 0;
        if ($opening) {
            $balance_day = $opening->balance_days - $opening->payment_days;
        }

        $total_present = Attendance::where([['employee_id','=',$employee_id],['status','=','Present']])->count();

        if ($total_present) {
            $balance_day += $total_present/18;
        }


        $leave = leaveType::where('is_earn',1)->first();
        $leave->leave_type_id = $leave->id;
        if ($leave) {
            $tmp = leaveDetail::where([
                ['leave_details.leave_type_id','=',$leave->leave_type_id]
            ])
                ->leftJoin('leave_registers','leave_details.leave_register_id','=','leave_registers.id')
                ->where('leave_registers.employee_id','=',$employee_id)
                ->where('leave_registers.status','=',"Approved")
                ->where('leave_registers.is_permanent_save','=',"Yes")
                ->sum('leave_details.days');
            $balance_day -= $tmp;
        }
        $leave->leave_days = $balance_day;
        return $leave;
    }
}
