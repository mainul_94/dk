<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalaryStructureChild extends Model
{
    protected $fillable =['salary_structure_id','salary_type_id','amount'];


    /**
     * Get Salary Structure
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salaryStructure()
    {
        return $this->belongsTo('App\SalaryStructure');
    }


    /**
     * Get Salary Type
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function salaryType()
    {
        return $this->belongsTo('App\SalaryType');
    }
}
