<?php

namespace App;

use App\Model\CommonField;
use Illuminate\Database\Eloquent\Model;

class ProbationPeriod extends Model
{
    protected $fillable = ['name','created_by','updated_by'];


    use CommonField;
}
