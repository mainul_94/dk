<?php

namespace App\Http\Controllers;

use App\Import;
use App\ImportEmpoyee;
use Illuminate\Http\Request;

use App\Http\Requests;
use phpDocumentor\Reflection\Types\Null_;

class AttendanceImportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('import_attendance.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $file = null;
        if ($request->file('file')->isValid()) {
            $file = $request->file('file')->move(storage_path('app/public/'),time().$request->file('file')->getClientOriginalName());
        }
        $this->updatedBy = $request->user()->id;
        $this->messages = [];
        if ($file->getMimeType() == 'text/plain' &&  $file->getExtension() == "txt") {
            $rows = fopen($file->getRealPath(),'r');
            echo "<pre>";
            while (!feof($rows)) {
                $line = fgets($rows);
                $line = str_replace("\t",' ',$line);
                $row = array_values(array_filter(explode(" ",$line)));
                if (count($row) >= 3) {
                    $row = collect([
                        'card_no' => $row[0],
                        'd_card' => str_replace('-', '', $row[1]),
                        't_card' => str_replace(':', '', $row[2])
                    ]);
                    array_push($this->messages,Import::importAttendance($row,$this->updatedBy));
                }
            }

        }else if ($file->getMimeType() == "application/octet-stream" ||
            ($file->getMimeType() == "text/plain" && $file->getExtension() == "csv")) {
            Import::load($file->getRealPath(),function($reander){
                foreach ($reander->get() as $key=>$row) {
                    array_push($this->messages,Import::importAttendance($row,$this->updatedBy));
                }
            });
        }

        return redirect()->back()->with('message',$this->messages);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
