<?php

namespace App\Http\Controllers;

use App\Holiday;
use App\HolidayChild;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;

class HolidayChildController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = HolidayChild::all();
        return view('holiday.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('holiday.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'date'=>'required',
            'name'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        $saveData = HolidayChild::create($values);

        return response()->json([
            "type"=>"success",
            "msg"=>"Successfully Saved",
            "id"=>$saveData->id
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('holiday.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return response()->json([
            "type"=>"success",
            "msg"=>"Successfully Updated"
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return response()->json([
            "type"=>"danger",
            "msg"=>"Successfully Delated"
        ]);
    }

    public function multipleInsert(Request $request,$holiday)
    {
        $this->validate($request,[
            'day'=>'required',
            'purpose'=>'required',
            'type'=>'required'
        ]);
        $year = (int) $holiday->year?$holiday->year:date('Y');
        $newDate = Carbon::create($year);
        for ($i = 1; $i <=52; $i++) {

            $haveEntry = HolidayChild::where('date',$newDate->nthOfYear($i,$request->get('day')))->get();
            if (count($haveEntry)>0) {
                continue;
            }

            HolidayChild::create([
                'name'=>$request->get('purpose'),
                'holiday_type_id'=>$request->get('type'),
                'details'=>$request->get('details'),
                'holiday_id'=>$holiday->id,
                'date'=>$newDate->nthOfYear($i,$request->get('day')),
                'created_by' => $request->user()->id
            ]);
        }


        if ($request->ajax()) {
            return response()->json([
                "type"=>"success",
                "msg"=>"Successfully Created"
            ]);
        }

        return redirect()->action('HolidayController@edit',[$holiday->id])->with('message',['type'=>'success','msg'=>'Successfully Saved']);


    }


}
