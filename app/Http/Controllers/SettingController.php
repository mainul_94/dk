<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Organization;
use Illuminate\Http\Request;

use App\Http\Requests;

use App\Setting;
use Illuminate\Support\Facades\DB;


class SettingController extends Controller
{
    //function for index//
    public function index()
    {
        $setting = Setting::all();
        return view('setting.listview',compact('setting'));
    }

    //function for create/
    public function  create()
    {
        return view('setting.create');

    }

    //function for create//
    public function store(Request $request)
    {
        $this->validate($request,[
            'property'=>'required',
            'value'=>'required'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        Setting::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);

    }

    //function for edit//
    public function edit($id)
    {
        return view('setting.edit',compact('id'));
    }

    public function update(Request $request, $id)
    {
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('SettingController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }


    /**
     * Get value from Api
     * @param Request $request
     */

    public function getValue(Request $request)
    {
        if (!$request->ajax()) {
            return true;
        }
        $data = DB::table($request->get('table'))->select($request->get('select'));

        $filters = $request->get('filters');
        if (!empty($request->get('search'))) {
            if (!$filters) {
                $filters =[[$request->get('search_key'),'like','%'.$request->get('search').'%']];
            }else{
                array_push($filters,[$request->get('search_key'),'like','%'.$request->get('search').'%']);
                return response()->json($filters);

            }

        }
        if (!empty($filters)) {
            $data->where($filters);
        }
        return response()->json($data->get());
    }


    /**
     * @param Request $request
     * $request->get('field') as String or Array
     * $request->get('filters') as Array Ex. ['votes', '=', 100]
     * @param $table
     * @return bool|\Illuminate\Http\JsonResponse
     */
    public function getValues(Request $request)
    {
        if (!$request->ajax()) {
            return true;
        }

        $data = DB::table($request->get('table'));
        if ($request->get('field')) {
            $data->select($request->get('field'));
        }

        if ($request->get('filters')) {
            $data->where($request->get('filters'));
        }
        return response()->json($data->get());
    }

}
