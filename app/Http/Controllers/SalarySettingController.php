<?php

namespace App\Http\Controllers;

use App\Designation;
use App\SalarySetting;
use App\SalarySettingChild;
use Illuminate\Http\Request;

use App\Http\Requests;

class SalarySettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = SalarySetting::all();
        return view('salary_setting.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if ($request->get('designation')) {
            $designation = Designation::findOrFail($request->get('designation'));
        }else{
            $designation = false;
        }

        return view('salary_setting.create',compact('designation'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validation($request);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        $salarySetting = new SalarySetting;
        $salarySetting->fill($values)->save();

        /*$child_val = ['salary_setting_id'=>$salarySetting->id];
        foreach ($values['salary_type_id'] as $key => $val) {
            $child_val['salary_type_id'] = $val;
            $child_val['min_amount'] = $values['min_amount'][$key];
            $child_val['max_amount'] = $values['max_amount'][$key];
            SalarySettingChild::create($child_val);
        }*/


        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('salary_setting.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SalarySetting $id)
    {
        /*foreach ($id->children as $detail) {
            if (in_array($detail->id,$request->get('cid'))) {
                continue;
            } else {
                $detail->delete();
            }
        }
        foreach ($request->get('salary_type_id') as $key=>$val) {
            if (empty($val)) {
                continue;
            }
            $values = [];
            $values['salary_type_id'] = $val;
            $values['min_amount'] = $request->get('min_amount')[$key];
            $values['max_amount'] = $request->get('max_amount')[$key];
            $values['id'] = $request->get('cid')[$key];
            if (!empty($values['id'])) {
                $leave = SalarySettingChild::find($values['id']);
            }else {
                $leave = new SalarySettingChild;
                $leave->fill(['salary_setting_id'=>$id->id]);
            }
            $leave->fill($values);
            $leave->save();
        }*/
        $this->validation($request);
        $id->fill($request->all());
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->update();
        return redirect()->to(action('SalarySettingController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('SalarySettingController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }


    /**
     * @param $request
     */
    public function validation($request)
    {
        $this->validate($request,[
            'designation_id'=>'required',
            'min_basic'=>'numeric|max:'.$request->get('max_basic'),
            'min_hra'=>'numeric|max:'.$request->get('max_hra'),
            'min_medical'=>'numeric|max:'.$request->get('max_medical'),
            'min_food'=>'numeric|max:'.$request->get('max_food'),
            'min_mobile'=>'numeric|max:'.$request->get('max_mobile'),
            'min_conveyance'=>'numeric|max:'.$request->get('max_conveyance'),
            'min_special','numeric|max:'.$request->get('max_special'),
            'min_car'=>'numeric|max:'.$request->get('max_car'),
            'min_others'=>'numeric|max:'.$request->get('max_others')
        ]);
    }


    public function getSalarySettingByDesignation(Request $request)
    {
        if (empty($request->get('designation_id'))){
            return response()->json([
                'Designatin Required'
            ]);
        }

        return SalarySetting::where('designation_id',$request->designation_id)->first();
        
    }
}
