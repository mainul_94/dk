<?php

namespace App\Http\Controllers;

use App\leaveType;
use Illuminate\Http\Request;

use App\Http\Requests;

class LeaveTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rows = leaveType::all();
        return view('leave_type.listview',compact('rows'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('leave_type.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'=>'required',
            'is_earn'=>'unique:leave_types',
            'is_maternity'=>'unique:leave_types'
        ]);
        $values = $request->all();
        $values['created_by'] = $request->user()->id;
        leaveType::create($values);

        if ($request->ajax()) {
            return response()->json([
                'message'=>'Success'
            ]);
        }

        return redirect()->back()->with('message',['type'=>'success','msg'=>'Successfully Saved']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return view('leave_type.edit',compact('id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, leaveType $id)
    {
        $values = $request->all();
        if (empty($request->get('is_payable')) && $id->is_payable) {
            $values['is_payable'] = null;
        }
        if (empty($request->get('is_earn')) && $id->is_earn) {
            $values['is_earn'] = null;
        }
        if (empty($request->get('is_maternity')) && $id->is_maternity) {
            $values['is_maternity'] = null;
        }
        $id->fill($values);
        $id->fill(['updated_by'=>$request->user()->id]);
        $id->save();
        return redirect()->to(action('LeaveTypeController@edit',$id))->with('message',['type'=>'success','msg'=>'Successfully Updated']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $id->delete();
        return redirect()->to(action('LeaveTypeController@index'))->with('message',['type'=>'danger','msg'=>'Successfully Deleted']);
    }
}
