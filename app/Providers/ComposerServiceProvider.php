<?php

namespace App\Providers;

use App\leaveType;
use App\Menu;
use App\SalaryType;
use App\Setting;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('layouts._partial.sidebar',function($view){
            $menus = Menu::where('parent_id',null)->orderBy('order_id')->get();
            $view->with('menus',$menus);
        });

        view()->composer('*', function ($view) {
            $settings = Setting::lists('value','property')->toJson();
            $view->with('settings',$settings);
        });

        view()->composer(['leave_assign._form','leave_assign.multiple'], function ($view) {
            $leaveTypes = leaveType::all();
            $view->with('leaveTypes',$leaveTypes);
        });

        view()->composer(['salary_structure._form','salary_setting._form'], function ($view) {
            $salaryTypes = SalaryType::all();
            $view->with('salaryTypes',$salaryTypes);
        });

        view()->composer(['salary.*','report.salary','report.salary_designation_wise','report.payslip',
            'report.job_card', 'pdf.salary', 'pdf.ext_salary'], function ($view) {
            $years = array_combine(range(date("Y"), 1910), range(date("Y"), 1910));
            $months = array(
                1=>'January',
                2=>'February',
                3=>'March',
                4=>'April',
                5=>'May',
                6=>'June',
                7=>'July ',
                8=>'August',
                9=>'September',
                10=>'October',
                11=>'November',
                12=>'December',
            );
            $view->with('years',$years)
                ->with('months',$months);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
