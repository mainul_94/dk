<?php

namespace App\Providers;

use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
        $router->model('organization','App\Organization');
        $router->model('menu','App\Menu');
        $router->model('setting','App\Setting');
        $router->model('attendance','App\Attendance');
        $router->model('unit','App\Unit');
        $router->model('division','App\Division');
        $router->model('department','App\Department');
        $router->model('section','App\Section');
        $router->model('line','App\Line');
        $router->model('designation_group','App\DesignationGroup');
        $router->model('designation','App\Designation');
        $router->model('employee_category','App\EmployeeCategory');
        $router->model('employee_type','App\EmployeeType');
        $router->model('probation_period','App\ProbationPeriod');
        $router->model('holiday_type','App\HolidayType');
        $router->model('holiday','App\Holiday');
        $router->model('holiday_child','App\HolidayChild');
        $router->model('employee','App\Employee');
        $router->model('leave_type','App\leaveType');
        $router->model('leave_assign','App\EmployeeLeaveAssign');
        $router->model('leave_register','App\leaveRegister');
        $router->model('salary_type','App\SalaryType');
        $router->model('salary_structure','App\SalaryStructure');
        $router->model('salary_setting','App\SalarySetting');
        $router->model('salary','App\Salary');
        $router->model('user','App\User');
        $router->model('error_log','App\ImportError');
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');
        });
    }
}
