var InitiateHolidayChildTableTable = function() {
    return {
        init: function(parent_id) {
            //Datatable Initiating
            var oTable = $('#holidayChildTable').dataTable({
                "aLengthMenu": [
                    [5, 15, 20, 100, -1],
                    [5, 15, 20, 100, "All"]
                ],
                "iDisplayLength": 5,
                "sPaginationType": "bootstrap",
                "paging":false,
                "filter":false,
                //"sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                "oTableTools": {
                    "aButtons": [
                        "copy",
                        "print",
                        {
                            "sExtends": "collection",
                            "sButtonText": "Save <i class=\"fa fa-angle-down\"></i>",
                            "aButtons": ["csv", "xls", "pdf"]
                        }
                    ],
                    "sSwfPath": "assets/swf/copy_csv_xls_pdf.swf"
                },
                "language": {
                    "search": "",
                    "sLengthMenu": "_MENU_",
                    "oPaginate": {
                        "sPrevious": "Prev",
                        "sNext": "Next"
                    }
                },
                "aoColumns": [
                    null,
                    null,
                    null,
                    null,
                    null,
                    { "bSortable": false }
                ]
            });

            var isEditing = null;

            //Add New Row
            $('#holidayChildTable_new').click(function(e) {
                e.preventDefault();
                var aiNew = oTable.fnAddData([
                    '', '', '', '', '',
                    '<a href="#" class="btn btn-success btn-xs save"><i class="fa fa-edit"></i> Save</a> <a href="#" class="btn btn-warning btn-xs cancel" data-mode="new"><i class="fa fa-times"></i> Cancel</a>'
                ]);
                var nRow = oTable.fnGetNodes(aiNew[0]);
                editAddedRow(oTable, nRow);
                isEditing = nRow;
            });

            //Delete an Existing Row
            $('#holidayChildTable').on("click", 'a.delete', function(e) {
                e.preventDefault();

                var nRow = $(this).parents('tr')[0];
                /*if (confirm("Are You Sure To Delete This Row?") == false) {
                    return;
                }*/
                swal({
                    title: "Did you want to Delete?",
                    type: "error",
                    showCancelButton: true,
                }, function (inConfurm) {
                    if (inConfurm) {
                        $.ajax({
                            url:'/holiday-child/'+$(nRow).attr('data-id'),
                            method:'DELETE',
                            success: function (r) {
                                statusMsg({data:r});
                                oTable.fnDeleteRow(nRow);
                            },
                            complete: function (xhr) {
                                if (xhr.status !=200 ){
                                    statusMsg({data:{type:"danger",msg:"Sorry Unable to Delete Please Contact with Administrator"}});
                                }
                            }
                        });
                    }
                });


            });

            //Cancel Editing or Adding a Row
            $('#holidayChildTable').on("click", 'a.cancel', function(e) {
                e.preventDefault();
                if ($(this).attr("data-mode") == "new") {
                    var nRow = $(this).parents('tr')[0];
                    oTable.fnDeleteRow(nRow);
                    isEditing = null;
                } else {
                    restoreRow(oTable, isEditing);
                    isEditing = null;
                }
            });

            //Edit A Row
            $('#holidayChildTable').on("click", 'a.edit', function(e) {
                e.preventDefault();
                var nRow = $(this).parents('tr')[0];

                if (isEditing !== null && isEditing != nRow) {
                    restoreRow(oTable, isEditing);
                    editRow(oTable, nRow);
                    isEditing = nRow;
                } else {
                    editRow(oTable, nRow);
                    isEditing = nRow;
                }
            });

            //Save an Editing Row
            $('#holidayChildTable').on("click", 'a.save', function(e) {
                e.preventDefault();
                if (this.innerHTML.indexOf("Save") >= 0) {
                    saveRow(oTable, isEditing);
                    //Some Code to Highlight Updated Row
                }
            });



            function restoreRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);

                for (var i = 0, iLen = jqTds.length; i < iLen; i++) {
                    oTable.fnUpdate(aData[i], nRow, i, false);
                }

                oTable.fnDraw();
            }

            function editRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                var max_width = jqTds.parents('table').width() / jqTds.length;
                jqTds[0].innerHTML = '<input style="max-width: 40px;" class="form-control input-small" disabled="disabled" value="'+aData[0]+'" />';
                jqTds[1].innerHTML = '<input name="date" style="max-width: '+max_width+'px;" type="date" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<select name="type" style="min-width: '+max_width+'px;" class="form-control"><option value="'+$(jqTds[2]).attr('data-value')+'">'+aData[2]+'</option></select>';
                jqTds[3].innerHTML = '<input name="name" style="max-width: '+max_width+'px;" type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<textarea name="details" style="max-width: '+max_width+'px;" type="text" class="form-control input-small">'+aData[4]+'</textarea>';
                jqTds[5].innerHTML = '<a href="#" class="btn btn-success btn-xs save"><i class="fa fa-save"></i> Save</a> <a href="#" class="btn btn-warning btn-xs cancel"><i class="fa fa-times"></i> Cancel</a>';
                /*$(jqTds[1]).datepicker({
                    viewMode:0
                });*/
                getvalueForSelect2(jqTds[2].firstChild,'holiday_types',['id','name'],[],'id','name', {id:$(jqTds[2]).attr('data-value'),text:aData[2]});
            }

            function editAddedRow(oTable, nRow) {
                var aData = oTable.fnGetData(nRow);
                var jqTds = $('>td', nRow);
                var max_width = jqTds.parents('table').width() / jqTds.length;
                jqTds[0].innerHTML = '<input style="max-width: 40px;" class="form-control input-small" disabled="disabled" value="'+aData[0]+'" />';
                jqTds[1].innerHTML = '<input name="date" style="max-width: '+max_width+'px;" type="date" class="form-control input-small" value="' + aData[1] + '">';
                jqTds[2].innerHTML = '<select name="type" style="min-width: '+max_width+'px;" class="form-control"><option selected="selected" value="'+$(jqTds[2]).attr('data-value')+'">'+aData[2]+'</option></select>';
                jqTds[3].innerHTML = '<input name="name" style="max-width: '+max_width+'px;" type="text" class="form-control input-small" value="' + aData[3] + '">';
                jqTds[4].innerHTML = '<textarea name="details" style="max-width: '+max_width+'px;" class="form-control input-small">'+aData[4]+'</textarea>';
                jqTds[5].innerHTML = aData[5];
                /*$(jqTds[1]).datepicker({
                 viewMode:0
                 });*/
                getvalueForSelect2(jqTds[2].firstChild,'holiday_types',['id','name'],[],'id','name', {id:$(jqTds[2]).attr('data-value'),text:aData[2]});
            }

            function statusMsg(data) {
                $('<div class="col-xs-12">\
                            <div class="alert alert-'+data.data.type+' fade in">\
                            <button class="close" data-dismiss="alert">×</button>\
                        <i class="fa-fw fa fa-check"></i>'+data.data.msg+'</div>\
                        </div>').prependTo("#holidayChildTable_wrapper");
            }

            function saveRow(oTable, nRow) {
                var jqInputs = $('input,select,textarea', nRow);
                var row_id = $(nRow).attr('data-id');
                saveDat(jqInputs,row_id, function (data) {
                    if (data.success) {
                        statusMsg(data);
                        var type = $(jqInputs[2]).find('option[value="'+jqInputs[2].value+'"]').text();
                        oTable.fnUpdate('#', nRow, 0, false);
                        oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                        oTable.fnUpdate(type, nRow, 2, false);
                        $(nRow).find('td:nth-child(3)').attr('data-value',jqInputs[2].value);
                        oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                        oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                        oTable.fnUpdate('<a href="#" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>', nRow, 5, false);
                        oTable.fnDraw();

                        if (data.data.id) {
                            $(nRow).attr('data-id',data.data.id);
                        }

                        isEditing = null;
                    }else {
                        return
                    }
                });
            }

            function cancelEditRow(oTable, nRow) {
                var jqInputs = $('input,select,textarea', nRow);
                oTable.fnUpdate(jqInputs[0].value, nRow, 0, false);
                oTable.fnUpdate(jqInputs[1].value, nRow, 1, false);
                oTable.fnUpdate(jqInputs[2].value, nRow, 2, false);
                oTable.fnUpdate(jqInputs[3].value, nRow, 3, false);
                oTable.fnUpdate(jqInputs[4].value, nRow, 4, false);
                oTable.fnUpdate('<a href="#" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a> <a href="#" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>', nRow, 5, false);
                oTable.fnDraw();
            }

            function saveDat(data,row_id,callback) {
                var method = (row_id?'PATCH':'POST');
                var url = (row_id?'/holiday-child/'+row_id:'/holiday-child');
                var rearange_data ={};
                rearange_data['name']=data[3].value;
                rearange_data['date']=data[1].value;
                rearange_data['holiday_type_id']=data[2].value;
                rearange_data['holiday_id']=parent_id;
                rearange_data['details']=data[4].value;
                $.ajax({
                    url:url,
                    method:method,
                    datatype:"JSON",
                    data:rearange_data,
                    success: function (r) {
                        callback({success:1,data:r})
                    },
                    complete: function (xhr) {
                        if (xhr.status == 422) {
                            $.each(xhr.responseJSON, function (index,value){
                                for (var i = 0 ; i < data.length-1; i++) {
                                    if ($(data[i]).attr('name')==index) {
                                        $(data[i]).after('<span class="help-block">'+value[0]+'</span>').parent().addClass('has-error');
                                    }
                                }
                            })
                        }
                        if (xhr.status !=200 ){
                            callback({success:0})
                        }
                    }
                });

            }

        }

    };
}();