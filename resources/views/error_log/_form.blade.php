<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('import_for') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('import_for','Name',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('import_for',null,['class'=>'form-control']) !!}
        {!! $errors->first('import_for','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('message') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('message','Message',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('message',null,['class'=>'form-control']) !!}
        {!! $errors->first('message','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('status','Status',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('status',['Pending'=>'Pending', 'On Hold'=>'On Hold', 'Fixed'=>'Fixed'],null,['class'=>'form-control']) !!}
        {!! $errors->first('status','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>
