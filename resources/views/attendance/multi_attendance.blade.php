<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/19/16
 * Time: 6:22 PM
 */
?>
@extends('layouts.layout')
@section('content')
<div class="row">
    <div class="col-lg-12 col-sm-12 col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-left bordered-darkorange">
                <span class="widget-caption">Filter Employee</span>
            </div>
            <div class="widget-body bordered-left bordered-warning">
                {!! Form::open(['action'=>'AttendanceController@multiAttendance','id'=>'get_employee','class'=>'form-inline','method'=>'get']) !!}
                    <div class="form-group col-sm-4">
                        {!! Form::label('line','Line',['class'=>'sr-only']) !!}
                        {!! Form::select('line',$lines,$line_id,['class'=>'form-control','style'=>'width: 100%;']) !!}
                    </div>
                    <div class="form-group col-sm-3">
                        {!! Form::label('employee_category','Type',['class'=>'sr-only']) !!}
                        {!! Form::select('employee_category',$emp_categories,$emp_category,['class'=>'form-control','style'=>'width: 100%;']) !!}
                    </div>
                    <div class="form-group col-sm-3">
                        {{--<label class="sr-only" for="data">Date</label>--}}
                        {!! Form::label('date','Date',['class'=>'sr-only']) !!}
                        {!! Form::date('date',$date,['class'=>'form-control']) !!}
                        {{--<input type="date" name="date" class="" id="date" placeholder="Date">--}}
                    </div>
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-info get_employee">Get Employee</button>
                        {{--<button type="submit" class="btn btn-warning create">Edit</button>--}}
                    </div>
                    <div class="clearfix"></div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
{{--Data View--}}

<div class="row">
    <div class="col-xs-12">
        <div class="well with-header  with-footer">
            <div class="header bg-blue">
                Employee List
            </div>
            <div class="col-sx-12" id="set_value_section">
                <div class="col-sm-4">
                    <lable for="all_intime">In Time</lable>
                    <input type="text" placeholder="00:00:00" class="form-control" id="all_intime" name="all_intime">
                </div>
                <div class="col-sm-4">
                    <lable for="all_intime">Out Time</lable>
                    <input type="text" placeholder="00:00:00" class="form-control" id="all_outtime" name="all_outtime">
                </div>
                <div class="col-sm-4">
                    {{--<label for="set_value"> Create / Edit Multi Attendance</label>--}}
                    <br>
                    <button class="btn btn-info" id="set_value">Set Value</button>
                </div>
            </div>
            <table class="table table-hover">
                <thead class="bordered-darkorange">
                <tr>
                    <th>
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="check_all" id="check_all" class="colored-success">
                                <span class="text"></span>
                            </label>
                        </div>
                    </th>
                    <th>Employee ID{{--Card No--}}</th>
                    <th>In Time</th>
                    <th>Out Time</th>
                    <th>Duration</th>
                    <th>Late Time</th>
                    <th>Overtime</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="multi_attendance_table">
                @if($employees)
                    @foreach($employees as $employee)
                        <tr>
                            {!! Form::model($employee) !!}
                            <td>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="check" id="check" class="colored-success check">
                                        <span class="text"></span>
                                    </label>
                                </div>
                            </td>
                            <td>
                                {!! Form::text('employee_id',null,['class'=>'form-control','readonly'=>'readonly','id'=>'employee_id']) !!}
                                {!! Form::hidden('attendance_id',null,['class'=>'form-control','readonly'=>'readonly','id'=>'employee_id']) !!}
                                {!! Form::hidden('date',null,['class'=>'form-control','readonly'=>'readonly','id'=>'employee_id']) !!}
                            </td>
                            <td>
                                {!! Form::text('in_time',null,['class'=>'form-control','placeholder'=>'00:00:00','id'=>'in_time']) !!}
                            </td>
                            <td>
                                {!! Form::text('out_time',null,['class'=>'form-control','placeholder'=>'00:00:00','id'=>'out_time']) !!}
                            </td>
                            <td>
                                {!! Form::text('duration',null,['class'=>'form-control','placeholder'=>'00:00:00','readonly'=>'readonly','id'=>'duration']) !!}
                            </td>
                            <td>
                                {!! Form::text('let_time',null,['class'=>'form-control','placeholder'=>'00:00:00','readonly'=>'readonly','id'=>'let_time']) !!}
                            </td>
                            <td>
                                {!! Form::text('overtime',null,['class'=>'form-control','placeholder'=>'00:00:00','readonly'=>'readonly','id'=>'overtime']) !!}
                            </td>
                            <td>
                                {!! Form::text('status',null,['class'=>'form-control','readonly'=>'readonly','id'=>'status']) !!}
                            </td>
                            {!! Form::close() !!}
                        </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="8"><h2>No Data</h2></td>
                    </tr>
                @endif
                </tbody>
            </table>

            <div class="footer">
                <button class="btn btn-success" id="save_all">Save</button>
            </div>
        </div>

    </div>
</div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='line']",'lines',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_category']",'employee_categories',['id','name'],[],'id','name');
        /**
         * On Click Check All
         */
        $("#check_all").on('click', function () {
            if ($(this).prop("checked" )) {
                $(".check").prop("checked",true);
            }else {
                $(".check").prop("checked",false);
            }
        });

        /**
         * On Click Check
         */
        $(".check").on('click', function () {
            if ($(this).prop("checked" )) {
                if ($("#check:not(:checked)").length <= 0 ) {
                    $("#check_all").prop("checked",true);
                }else {
                    $("#check_all").prop("checked",false);
                }
            }else {
                $("#check_all").prop("checked",false);
            }
        });


//        Set Multiple Value

        function time_second_to_format(time) {
            return [
                Math.floor(time / 3600), // an hour has 3600 seconds
                Math.floor((time % 3600) / 60), // a minute has 60 seconds
                time % 60
            ].map(function(v) {
                return v < 10 ? '0' + v : v;
            }).join(':').split('.')[0];
        }

        $('#set_value').on('click', function () {
            if (($('#all_intime').val() == "") && ($('#all_outtime').val())=="") {
                return
            }
            $("#multi_attendance_table tr").each(function () {
                if ($(this).find('.check').prop("checked")) {
                    var intime_in_second = Math.abs(toSeconds($('#all_intime').val())-(Math.random() * 900) + 1);
                    var out_in_second = Math.abs(toSeconds($('#all_outtime').val())+(Math.random() * 900) + 1);
                    $(this).find('[name="in_time"]').val(time_second_to_format(intime_in_second)).trigger('change');
                    $(this).find('[name="out_time"]').val(time_second_to_format(out_in_second)).trigger('change');
                }
            });
        });

        //        Call Calculation for Late Time OverTime Status
        var settings = {!! $settings !!}
        //time calculation
            $("[name='out_time']").on('change',function () {
                var intime = $(this).parents('tr').find("#in_time").val();
                var outtime = $(this).val();
                setDuration(intime, outtime,$(this).parents('tr').find('#duration'));
                setOvertime(intime, outtime, ('office_duration_time' in settings ? settings.office_duration_time:''),
                        $(this).parents('tr').find('#overtime'));
            });
        //time calculation
        $("[name='in_time']").on('change',function () {
            var intime = $(this).val();
            var outtime = $(this).parents('tr').find("#out_time").val();
            setLate(intime, ('office_opening_time' in settings ? settings.office_opening_time:''), ('attendance_buffer_time' in settings ? settings.attendance_buffer_time:''),
            $(this).parents('tr').find('#let_time'));
            setDuration(intime, outtime,$(this).parents('tr').find('#duration'));
            setOvertime(intime, outtime, ('office_duration_time' in settings ? settings.office_duration_time:''),
                    $(this).parents('tr').find('#overtime'));
        });

        $('[name="duration"]').on('change', function () {
            if ('lunch_break' in settings) {
                var lunchbrackSplit = settings.lunch_break.split('-');
                var lunchDuration = toSeconds(lunchbrackSplit[0],lunchbrackSplit[1])

                var duration_array = false;
                if (toSeconds($(this).parents('tr').find('#out_time').val())<=toSeconds(lunchbrackSplit[0])) {
                    duration_array = calDuration('00:00:00', $(this).val());
                }else if (toSeconds($(this).parents('tr').find('#out_time').val())<=toSeconds(lunchbrackSplit[1])) {
                    var tmp_duration_array = calDuration(lunchbrackSplit[0], $(this).parents('tr').find('#out_time').val());
                    var tmp_du = tmp_duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    duration_array = calDuration(tmp_du, $(this).val());
                }else {
                    var tmp_duration_array = calDuration(lunchbrackSplit[0], lunchbrackSplit[1]);
                    var tmp_du = tmp_duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    duration_array = calDuration(tmp_du, $(this).val());
                }

                if (duration_array) {
                    // 0 padding and concatation
                    duration = duration_array.map(function(v) {
                        return v < 10 ? '0' + v : v;
                    }).join(':');
                    $(this).val(duration);
                }

            }
            if (!('min_working_hours' in settings)) {
                return false
            }

            if (toSeconds($(this).val()) < toSeconds(settings.min_working_hours)) {
                $(this).parents('tr').find('#status').val('Absent')
            }else {
                $(this).parents('tr').find('#status').val('Present')
            }
        });


        $('#save_all').on('click', function () {
            var rows = [];
            $("#multi_attendance_table tr").each(function () {
                var data = $(this).find('form').serializeArray();
                var row = {};
                for (var i = 0; i <data.length; i++) {
                    row[data[i]['name']]=data[i]['value']
                }
                rows.push(row)
            });
            if (rows.length <1) {
                return
            }
            $.ajax({
                method:"POST",
                url:"{{ action('AttendanceController@multiAttendanceSave') }}",
                dataType:'json',
                data:{rows:rows,date:"{{ empty($_REQUEST['date'])?date('Y-m-d'):$_REQUEST['date'] }}"},
                success: function (data) {
                    console.log(data)
                    swal({
                        title: "Success Fully Saved",
                        type: "success"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });
                },
                complete: function (xhr,status) {
                }
            });
        });
    </script>
@endsection