<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/10/16
 * Time: 11:41 PM
 */
?>
<tr>
    <td>{!! $sl !!}</td>
    <td>{!! $row->employee->card_no !!}</td>
    <td>{!! $row->date !!}</td>
    <td>{!! $row->in_time !!}</td>
    <td>{!! $row->out_time !!}</td>
    <td>{!! $row->duration !!}</td>
    <td>{!! $row->let_time !!}</td>
    <td>{!! $row->overtime !!}</td>
    <td>
        <span class="label label-{{ $row->status=="Present"?'success':'danger' }}">
            {!! $row->status !!}
        </span>
    </td>
    <td>{!! $row->createdBy->name !!}</td>
    <td>
        @if($row->updateBy)
            {!! $row->updateBy->name !!}
        @endif
    </td>
    <td class="text-center">

        <a href="{!! action('AttendanceController@edit',$row->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
    </td>
</tr>
