<?php
/**
 * Created by PhpStorm.
 * User: raju
 * Date: 3/28/16
 * Time: 11:43 PM
 */
?>

@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-bottom bordered-blue">
                    <span class="widget-caption">Update Setting</span>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            {!! Form::model($id,['action'=>['AttendanceController@update',$id->id],'method'=>'PATCH']) !!}
                            @include('attendance._form')
                            {!! Form::close() !!}
                        </div>
                        <div class="col-sm-2" id="employee_details">
                            <div class="photo"></div>
                            <div class="details"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection