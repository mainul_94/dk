<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-sm-6 {{ $errors->has('year') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('year','Year',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('year',null,['class'=>'form-control']) !!}
        {!! $errors->first('year','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('month') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('month','Month',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('month',null,['class'=>'form-control']) !!}
        {!! $errors->first('month','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('gross_salary') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('gross_salary','Gross Salary',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('gross_salary',null,['class'=>'form-control']) !!}
        {!! $errors->first('gross_salary','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('basic_salary') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('basic_salary','Basic Salary',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('basic_salary',null,['class'=>'form-control']) !!}
        {!! $errors->first('basic_salary','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('hra') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('hra','House Rant',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('hra',null,['class'=>'form-control']) !!}
        {!! $errors->first('hra','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('medical') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('medical','Medical',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('medical',null,['class'=>'form-control']) !!}
        {!! $errors->first('medical','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('food_all') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('food_all','Food',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('food_all',null,['class'=>'form-control']) !!}
        {!! $errors->first('food_all','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('conveyance') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('conveyance','Conveyance',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('conveyance',null,['class'=>'form-control']) !!}
        {!! $errors->first('conveyance','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('mobile_bill') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('mobile_bill','Mobile Bill',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('mobile_bill',null,['class'=>'form-control']) !!}
        {!! $errors->first('mobile_bill','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('special_bill') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('special_bill','Special Bill',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('special_bill',null,['class'=>'form-control']) !!}
        {!! $errors->first('special_bill','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('car_bill') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('car_bill','Car Bill',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('car_bill',null,['class'=>'form-control']) !!}
        {!! $errors->first('car_bill','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('others') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('others','Others',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('others',null,['class'=>'form-control']) !!}
        {!! $errors->first('others','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('incentive') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('incentive','Incentive',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('incentive',null,['class'=>'form-control']) !!}
        {!! $errors->first('incentive','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('attendance_bonus') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('attendance_bonus','Attendance Bonus',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('attendance_bonus',null,['class'=>'form-control']) !!}
        {!! $errors->first('attendance_bonus','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ab_deduction') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ab_deduction','Advance Deduction',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ab_deduction',null,['class'=>'form-control']) !!}
        {!! $errors->first('ab_deduction','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ot_rate') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ot_rate','OT Rate',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ot_rate',null,['class'=>'form-control']) !!}
        {!! $errors->first('ot_rate','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ot_hours') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ot_hours','OT Hours',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ot_hours',null,['class'=>'form-control']) !!}
        {!! $errors->first('ot_hours','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ext_ot_hours') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ext_ot_hours','Extra OT Hours',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ext_ot_hours',null,['class'=>'form-control']) !!}
        {!! $errors->first('ext_ot_hours','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ot_amount') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ot_amount','OT Amount',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ot_amount',null,['class'=>'form-control']) !!}
        {!! $errors->first('ot_amount','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ext_ot_amount') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ext_ot_amount','Extra OT Amount',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ext_ot_amount',null,['class'=>'form-control']) !!}
        {!! $errors->first('ext_ot_amount','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('holiday_all') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('holiday_all','Holidays',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('holiday_all',null,['class'=>'form-control']) !!}
        {!! $errors->first('holiday_all','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('arrear') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('arrear','Arrear',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('arrear',null,['class'=>'form-control']) !!}
        {!! $errors->first('arrear','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('fest_bonus') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('fest_bonus','Fastible Bonus',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('fest_bonus',null,['class'=>'form-control']) !!}
        {!! $errors->first('fest_bonus','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('advance') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('advance','Advance',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('advance',null,['class'=>'form-control']) !!}
        {!! $errors->first('advance','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('total_leave') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('total_leave','Total Leave',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('total_leave',null,['class'=>'form-control']) !!}
        {!! $errors->first('total_leave','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('p_days') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('p_days','Present Days',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('p_days',null,['class'=>'form-control']) !!}
        {!! $errors->first('p_days','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('ab_days') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('ab_days','Absent Days',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('ab_days',null,['class'=>'form-control']) !!}
        {!! $errors->first('ab_days','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('payable_leave') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('payable_leave','Payable Leave',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('payable_leave',null,['class'=>'form-control']) !!}
        {!! $errors->first('payable_leave','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('without_pay_leave') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('without_pay_leave','Not Payable Leave',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('without_pay_leave',null,['class'=>'form-control']) !!}
        {!! $errors->first('without_pay_leave','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('others_deduction') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('others_deduction','Others Deduction',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('others_deduction',null,['class'=>'form-control']) !!}
        {!! $errors->first('others_deduction','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('tax') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('tax','Tax',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('tax',null,['class'=>'form-control']) !!}
        {!! $errors->first('tax','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('revenue_stamp') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('revenue_stamp','Revenue Stamp',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('revenue_stamp',null,['class'=>'form-control']) !!}
        {!! $errors->first('revenue_stamp','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('payable_amount') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('payable_amount','Payable Amount',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('payable_amount',null,['class'=>'form-control']) !!}
        {!! $errors->first('payable_amount','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-sm-6 {{ $errors->has('total_days') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('total_days','Total Days',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('total_days',null,['class'=>'form-control']) !!}
        {!! $errors->first('total_days','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-sm-6 {{ $errors->has('payable_days') ? 'has-error':''}}">
    <div class="col-sm-4 text-right">
        {!! Form::label('payable_days','Payable Day',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-8">
        {!! Form::text('payable_days',null,['class'=>'form-control']) !!}
        {!! $errors->first('payable_days','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>
