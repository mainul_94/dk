<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/7/16
 * Time: 8:09 PM
 */
$sl = 1;
?>
@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="well with-header">
                <div class="header bg-info">
                    <div class="row">
                        <div class="col-sm-7 col-xs-12">
                            User Create
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-1">
                        {!! Form::open(['action'=>['UserController@store']]) !!}
                        @include('user._form')
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection