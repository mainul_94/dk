<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('name','Name',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('email') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('email','Email',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('email',null,['class'=>'form-control']) !!}
        {!! $errors->first('email','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('password') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('password','Password',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::password('password',['class'=>'form-control']) !!}
        {!! $errors->first('password','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('password_confirmation') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('password_confirmation','Confirm Password',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::password('password_confirmation',['class'=>'form-control']) !!}
        {!! $errors->first('password_confirmation','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('status','Status',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('status',['ACTIVE'=>'ACTIVE','INACTIVE'=>'INACTIVE'],null,['class'=>'form-control']) !!}
        {!! $errors->first('status','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>


@section('footer_script')
    <script>
    getvalueForSelect2("select[name='organization_id']",'organizations',['id','name'],[],'id','name');
    </script>
@endsection