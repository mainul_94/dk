<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/25/17
 * Time: 9:42 PM
 */?>

<tr>
	<td>{!! $key !!}</td>
	<td>{!! $row->employee->old_card_no !!}</td>
	<td>{!! $row->employee->name !!}</td>
	<td>{!! $row->employee->designation->name !!}</td>
	<td>
		{!! $row->employee->designation->grade !!}
	</td>
	<td>
		{!! date('d-M-y',strtotime($row->employee->date_of_joining)) !!}
	</td>
	<td>{!! $row->basic_salary !!}</td>
	<td>
		{!! $row->hra !!}
	</td>
	<td>
		{!! $row->medical !!}
	</td>
	<td>{!! $row->conveyance !!}</td>
	<td>
		{!! $row->food_all !!}
	</td>
	<td> {!! $row->gross_salary !!}</td>

	<td>{!! round($row->ot_rate,2) !!}</td>
	<td>{!! $row->ext_ot_hours !!}</td>
	<td>{!! $row->ext_ot_amount !!}</td>
	<td>{!! ((int) $row->payable_amount + (int) $row->revenue_stamp + (int) $row->advance) - (int) $row->ext_ot_amount !!}</td>
	<td></td>
</tr>
