<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/22/17
 * Time: 9:57 PM
 */
?>
		<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Salary Sheet Print</title>
	<style>
		h2 {
			margin: 0;
		}
		.page-break {
			page-break-after: always;
		}
		table {
			border-spacing: 0;
			border-collapse: collapse;
			table-layout: fixed;
			width: 100%;
		}
		td {
			border: 1px solid gray;
			text-align: center;
			font-family: 'Open Sans', 'Helvetica Neue', Helvetica, Arial, sans-serif;
			font-weight: 300;
			font-size: 8pt;
			vertical-align: middle;
		}
		table>tbody>tr>td {
			height: 30px;
		}

		thead > tr > td {
			background-color: lightgrey;
		}

		.rotate-90c {
			/* Something you can count on */
			white-space: nowrap;
		}

		.rotate-90c > div {
			transform:rotate(270deg);
		}

		.page-wrapper {
			position: relative;
			overflow: hidden;
		}

		.page_footer {
			position: absolute;
			bottom: 0;
			font-size: 8pt;
		}
	</style>
</head>
<body>
@include('pdf._ext_table_header', ['comp'=>$comp])
<tbody>
@foreach($rows as $key=>$row)
	@php($key++)
	@include('pdf._ext_table_row')
	@if($key % 21 == 0)
		@include('pdf._ext_table_footer', ['page_break'=> count($rows) != $key])
		@include('pdf._ext_table_header', ['comp'=>$comp, 'month' => $month, 'months'=>$months, 'year' => $year,
		'years'=> $years, 'sections' => $sections])
		<tbody>
		@endif
		@endforeach
		@include('pdf._ext_table_footer', ['page_break'=> false])
</body>
</html>
