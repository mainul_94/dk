<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 1/23/17
 * Time: 6:49 PM
 */?>

<table class="page-wrapper">
	<caption class="info">
		<h2>DK Global Fashion Ware Limited</h2>
		<p> Ext. OT Sheet for the Month of:  {{ array_key_exists((int) $month,$months)?$months[(int) $month]:'' }} - {{ array_key_exists($year,$years)?$years[$year]:'' }}</p>
		<p style="text-align: left">Section: {!! $sections[1] or ""!!}</p>
	</caption>
<thead>
<tr>
	<td width="1%">Sl No</td>
	<td width="2%">Card No</td>
	<td width="10%">Name</td>
	<td width="5%">Designation</td>
	<td width="1%" class="rotate-90c">
		<div>
			<span>Grade</span>
		</div>
	</td>
	<td width="4%">Date of Joining</td>
	<td width="3%">Basic</td>
	<td width="3%">House Rent</td>
	<td width="2%">M/A</td>
	<td width="2%">T/A</td>
	<td width="2%">F/A</td>
	<td width="3%">Gross Salary</td>

	<td width="3%">
		OT Rate
	</td>
	<td width="3%">
		OT Hours
	</td>
	<td width="3%">
		OT Amount
	</td>
	<td width="3%">Net Paid</td>
	<td width="5%">Signature</td>
</tr>
</thead>