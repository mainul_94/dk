<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 5/24/16
 * Time: 8:10 PM
 */
        ?>

@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-lg-12 col-sm-12 col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-left bordered-darkorange">
                    <span class="widget-caption">Filter Employee</span>
                </div>
                <div class="widget-body bordered-left bordered-warning">
                    <form class="form-inline" role="form" id="get_employee" action="{!! action('LeaveAssignController@getMultiple') !!}">
                        <div class="form-group col-sm-6">
                            <label class="sr-only" for="line">Line</label>
                            <select name="line" id="line" class="form-control" style="width: 100%;">
                                <option value=""></option>
                            </select>
                        </div>
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-info get_employee">Get Employee</button>
                            {{--<button type="submit" class="btn btn-warning create">Edit</button>--}}
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    <hr>
                    <div class="cl-xs-12">
                        <div class="col-sm-6">
                            <input type="number" placeholder="Year" name="year" class="form-control">
                        </div>
                        <div class="col-sm-4">
                            <button class="btn btn-success" id="save_all">Save</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{--Leave Types--}}

    <div class="col-xs-12">
        <table class="table table-striped table-bordered table-hover" id="simpledatatable">
            <thead>
            <tr>
                <th>SL</th>
                <th>Leave Type</th>
                <th>Allow Days</th>
            </tr>
            </thead>
            <tbody id="leaveTypes">
            @forelse($leaveTypes as $key=>$leaveType)
                @if($leaveType->is_earn)
                    @continue
                @else
                    <tr data-isMaternity="{!! $leaveType->is_maternity !!}" data-id="{!! $leaveType->id !!}">
                        <td>{!! $key+1 !!}</td>
                        <td>{!! Form::open() !!}{!! Form::select('leave_types',[$leaveType->id=>$leaveType->name],$leaveType->id,
                    ['class'=>'form-control','readonly' =>'readonly']) !!}</td>
                        <td>{!! Form::number('leave_days',null,['class'=>'form-control']) !!} {!! Form::close() !!}</td>
                    </tr>
                @endif
            @empty
                <tr>
                    <td colspan="3"><h2>Can't find any Leave Type</h2></td>
                </tr>
            @endforelse
            </tbody>
        </table>
        <br>
    </div>

    {{--Data View--}}

    <div class="row">
        <div class="col-xs-12">
            <div class="well with-header  with-footer">
                <div class="header bg-blue">
                    Employee List
                </div>
                <table class="table table-hover">
                    <thead class="bordered-darkorange">
                    <tr>
                        <th>
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="check_all" id="check_all" class="colored-success">
                                    <span class="text"></span>
                                </label>
                            </div>
                        </th>
                        <th>Employee ID{{--Card No--}}</th>
                        <th>Employee Name</th>
                        <th>Photo</th>
                    </tr>
                    </thead>
                    <tbody id="multi_attendance_table">
                    @if($employees)
                        @foreach($employees as $employee)
                            <tr>
                                {!! Form::model($employee) !!}
                                <td>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" name="check" value="1" id="check" class="colored-success check">
                                            <span class="text"></span>
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    {!! Form::text('id',null,['class'=>'form-control','readonly'=>'readonly','id'=>'employee_id']) !!}
                                </td>
                                <td>
                                    {!! $employee->name !!}
                                </td>
                                <td>
                                    <img src="{!! url($employee->getEmployeePersonalInfo->photo) !!}" alt="Photo">
                                </td>

                                {!! Form::close() !!}
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            <td colspan="8"><h2>No Data</h2></td>
                        </tr>
                    @endif
                    </tbody>
                </table>

            </div>

        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='line']",'lines',['id','name'],[],'id','name');
        /**
         * On Click Check All
         */
        $("#check_all").on('click', function () {
            if ($(this).prop("checked" )) {
                $(".check").prop("checked",true);
            }else {
                $(".check").prop("checked",false);
            }
        });

        /**
         * On Click Check
         */
        $(".check").on('click', function () {
            if ($(this).prop("checked" )) {
                if ($("#check:not(:checked)").length <= 0 ) {
                    $("#check_all").prop("checked",true);
                }else {
                    $("#check_all").prop("checked",false);
                }
            }else {
                $("#check_all").prop("checked",false);
            }
        });


        function dataSerialize(selector){
            var rows = [];
            $(selector).each(function () {
                var data = $(this).find('form').serializeArray();
                var row = {};
                for (var i = 0; i <data.length; i++) {
                    row[data[i]['name']]=data[i]['value']
                }
                rows.push(row)
            });
            return rows
        }
        //        Call Calculation for Late Time OverTime Status
        var settings = {!! $settings !!}
        $('#save_all').on('click', function () {
            var rows = dataSerialize("#multi_attendance_table tr");
            var leavesAssigns = dataSerialize("#leaveTypes tr");
            if (rows.length <1 || leavesAssigns.length <1) {
                return
            }
            $.ajax({
                method:"POST",
                url:"{{ action('LeaveAssignController@postMultiple') }}",
                dataType:'json',
                data:{employees:rows,leavesAssigns:leavesAssigns,'year':$('input[name="year"]').val()},
                success: function (data) {
                    console.log(data);
                    swal({
                        title: "Success Fully Saved",
                        type: "success"
                    }, function (isConfirm) {
                        if (isConfirm) {
                            location.reload();
                        }
                    });
                },
                complete: function (xhr,status) {
                }
            });
        });
    </script>
@endsection