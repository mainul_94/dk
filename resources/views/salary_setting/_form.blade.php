<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif

<div class="table-reponsive">
    <table class="table">
        <thead>
        <tr>
            <th>Designation</th>
            <th>Department</th>
            <th>Section</th>
            <th>Employee Type</th>
            {{--<th>All Bonus</th>--}}
            <th>Probtion Period</th>
            <th width="10%">Holiday Bonus</th>
            <th width="10%">Attendance Bonus</th>
            <th width="9%">OT Status</th>
            <th width="5%">Grade</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>
                <div class="form-group {{ $errors->has('designation_id') ? 'has-error':''}}">
                    @if(!empty($id))
                        <?php $designation_id = [$id->designation->id => $id->designation->name]  ?>
                    @else
                        <?php $designation_id = []  ?>
                    @endif
                    @if(!empty($designation))
                        {{--*/ $designation_id= [$designation->id => $designation->name] /*--}}
                        {{--*/ $designation= $designation->id  /*--}}
                    @else
                        {{--*/ $designation= null  /*--}}
                    @endif
                    {!! Form::select('designation_id',$designation_id,$designation,['class'=>'form-control']) !!}
                    {!! $errors->first('designation_id','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('department_id') ? 'has-error':''}}">
                    @if(!empty($id))
                        <?php $department_id = [$id->department->id => $id->department->name]  ?>
                    @else
                        <?php $department_id = []  ?>
                    @endif
                    {!! Form::select('department_id',$department_id,null,['class'=>'form-control','readonly'=>'readonly']) !!}
                    {!! $errors->first('department_id','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('section_id') ? 'has-error':''}}">
                    @if(!empty($id))
                        <?php $section_id = [$id->section->id => $id->section->name]  ?>
                    @else
                        <?php $section_id = []  ?>
                    @endif
                    {!! Form::select('section_id',$section_id,null,['class'=>'form-control','readonly'=>'readonly']) !!}
                    {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('employee_type_id') ? 'has-error':''}}">
                    @if(!empty($id))
                        <?php $employee_type_id = [$id->employeeType->id => $id->employeeType->name]  ?>
                    @else
                        <?php $employee_type_id = []  ?>
                    @endif
                    {!! Form::select('employee_type_id',$employee_type_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('employee_type_id','<span class="help-block">:message</span>') !!}
                </div>
            </td>

            <td>
                <div class="form-group {{ $errors->has('probation_period_id') ? 'has-error':''}}">
                    @if(!empty($id) and !empty($id->probationPeriod))
                        <?php $probation_period_id= [$id->probationPeriod->id => $id->probationPeriod->name]  ?>
                    @else
                        <?php $probation_period_id= [] ?>
                    @endif
                    {!! Form::select('probation_period_id',$probation_period_id,null,['class'=>'form-control']) !!}
                    {!! $errors->first('probation_period_id','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('holiday_bonus') ? 'has-error':''}}">
                    {!! Form::number('holiday_bonus',null,['class'=>'form-control']) !!}
                    {!! $errors->first('holiday_bonus','<span class="help-block">:message</span>') !!}
                </div>
            </td>


            <td>
                <div class="form-group {{ $errors->has('attendance_bonus') ? 'has-error':''}}">
                    {!! Form::number('attendance_bonus',null,['class'=>'form-control']) !!}
                    {!! $errors->first('attendance_bonus','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('ot_status') ? 'has-error':''}}">
                    {!! Form::select('ot_status',['Yes'=>'Yes','No'=>'No'],null,['class'=>'form-control']) !!}
                    {!! $errors->first('ot_status','<span class="help-block">:message</span>') !!}
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('grade') ? 'has-error':''}}">
                    {!! Form::text('grade',null,['class'=>'form-control','readonly'=>'readonly']) !!}
                    {!! $errors->first('grade','<span class="help-block">:message</span>') !!}
                </div>
            </td>
        </tr>
        </tbody>
    </table>

    <div class="col-sm-8 col-sm-offset-2">
        <table class="table">
            <thead>
            <tr class="info">
                <th width="40%">Salary Type</th>
                <th width="30%">Min Amount</th>
                <th width="30%">Max Amount</th>
            </tr>
            </thead>
            <tbody>

            {{--@if(!empty($id))--}}
            {{-- $childs = collect($id->children->toArray()) --}}
            {{--@endif--}}
            {{--@forelse($salaryTypes as $key=>$salaryType)--}}
            {{--@if(!empty($childs))--}}
            {{-- $child = $childs->where('salary_type_id',$salaryType->id) --}}
            {{--@if(count($child)>0)--}}
            {{--{!! Form::model($child[$child->keys()[0]]) !!}--}}
            {{--@endif--}}
            {{--@else--}}
            {{-- $child = null --}}
            {{--@endif--}}
            {{--<tr>--}}
            {{--<td>--}}
            {{--<div class="form-group {{ $errors->has('salary_type_id[]') ? 'has-error':''}}">--}}
            {{--{!! Form::select('salary_type_id[]',[$salaryType->id=>$salaryType->name],null,['class'=>'form-control']) !!}--}}
            {{--@if(count($child)>0)--}}
            {{--{!! Form::hidden('cid[]',$child[$child->keys()[0]]['id']) !!}--}}
            {{--@else--}}
            {{--{!! Form::hidden('cid[]',null) !!}--}}
            {{--@endif--}}

            {{--{!! $errors->first('salary_type_id[]','<span class="help-block">:message</span>') !!}--}}
            {{--</div>--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--<div class="form-group {{ $errors->has('min_amount[]') ? 'has-error':''}}">--}}
            {{--{!! Form::number('min_amount[]',null,['class'=>'form-control']) !!}--}}
            {{--{!! $errors->first('min_amount[]','<span class="help-block">:message</span>') !!}--}}
            {{--</div>--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--<div class="form-group {{ $errors->has('max_amount[]') ? 'has-error':''}}">--}}
            {{--{!! Form::number('max_amount[]',null,['class'=>'form-control']) !!}--}}
            {{--{!! $errors->first('max_amount[]','<span class="help-block">:message</span>') !!}--}}
            {{--</div>--}}
            {{--</td>--}}
            {{--</tr>--}}
            {{--@empty--}}
            {{--<tr>--}}
            {{--<td colspan="4"><h2>No Salary Type Avalable </h2></td>--}}
            {{--</tr>--}}
            {{--@endforelse--}}
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_basic','Basic Salary') !!}
                </td>
                <td class="form-group {{ $errors->has('min_basic') ? 'has-error':''}}">
                    {!! Form::text('min_basic',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_basic') ? 'has-error':''}}">
                    {!! Form::text('max_basic',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_hra','House Rent') !!}
                </td>
                <td class="form-group {{ $errors->has('min_hra') ? 'has-error':''}}">
                    {!! Form::text('min_hra',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_hra') ? 'has-error':''}}">
                    {!! Form::text('max_hra',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_medical','Medical') !!}
                </td>
                <td class="form-group {{ $errors->has('min_medical') ? 'has-error':''}}">
                    {!! Form::text('min_medical',250,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_medical') ? 'has-error':''}}">
                    {!! Form::text('max_medical',250,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_food','Food') !!}
                </td>
                <td class="form-group {{ $errors->has('min_food') ? 'has-error':''}}">
                    {!! Form::text('min_food',650,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_food') ? 'has-error':''}}">
                    {!! Form::text('max_food',650,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_conveyance','Conveyance') !!}
                </td>
                <td class="form-group {{ $errors->has('min_conveyance') ? 'has-error':''}}">
                    {!! Form::text('min_conveyance',200,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_conveyance') ? 'has-error':''}}">
                    {!! Form::text('max_conveyance',200,['class'=>'form-control','readonly'=>'readonly']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_mobile','Mobile Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('min_mobile') ? 'has-error':''}}">
                    {!! Form::text('min_mobile',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_mobile') ? 'has-error':''}}">
                    {!! Form::text('max_mobile',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_special','Special Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('min_special') ? 'has-error':''}}">
                    {!! Form::text('min_special',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_special') ? 'has-error':''}}">
                    {!! Form::text('max_special',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_car','Car Bill') !!}
                </td>
                <td class="form-group {{ $errors->has('min_car') ? 'has-error':''}}">
                    {!! Form::text('min_car',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_car') ? 'has-error':''}}">
                    {!! Form::text('max_car',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            <tr>
                <td class="text-right text-success">
                    {!! Form::label('min_others','Others') !!}
                </td>
                <td class="form-group {{ $errors->has('min_others') ? 'has-error':''}}">
                    {!! Form::text('min_others',null,['class'=>'form-control']) !!}
                </td>
                <td class="form-group {{ $errors->has('max_others') ? 'has-error':''}}">
                    {!! Form::text('max_others',null,['class'=>'form-control']) !!}
                </td>
            </tr>
            {{--<tr>--}}
            {{--<td class="text-right text-success">--}}
            {{--{!! Form::label('incentive','Incentive') !!}--}}
            {{--</td>--}}
            {{--<td class="form-group {{ $errors->has('incentive') ? 'has-error':''}}">--}}
            {{--{!! Form::text('incentive',null,['class'=>'form-control']) !!}--}}
            {{--{!! $errors->first('incentive','<span class="help-block">:message</span>') !!}--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--{!! Form::text('min_incentive',null,['class'=>'form-control']) !!}--}}
            {{--</td>--}}
            {{--<td>--}}
            {{--{!! Form::text('max_incentive',null,['class'=>'form-control']) !!}--}}
            {{--</td>--}}
            {{--</tr>--}}
            </tbody>
        </table>
    </div>
</div>



<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
    {{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>


@section('footer_script')
    <script>
        getvalueForSelect2("select[name='designation_id']",'designations',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_type_id']",'employee_types',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='probation_period_id']",'probation_periods',['id','name'],[],'id','name');



        $("select[name='designation_id']").on('change', function () {
            getValue("{!! url('/designation') !!}/"+$(this).val(), function (data) {
                $('[name="grade"]').val(data.grade);
                if (data.department_id) {
                    getValue("{!! url('/department') !!}/"+data.department_id, function (dp_data) {
                        $('[name="department_id"]').html('<option value="'+dp_data.id+'">'+dp_data.name+'</option>')
                                .val(data.department_id);
                    });
                }
                if (data.section_id) {
                    getValue("{!! url('/section') !!}/"+data.section_id, function (se_data) {
                        $('[name="section_id"]').html('<option value="'+se_data.id+'">'+se_data.name+'</option>')
                                .val(data.section_id);
                    });
                }
            });
        });
        @if(!empty($designation))
            $('[name="designation_id"]').change();
        @endif
    </script>
@endsection