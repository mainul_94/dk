<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:52 PM
 */
?>

@extends('layouts.layout')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="widget">
            <div class="widget-header bordered-bottom bordered-blue">
                <span class="widget-caption">Edit Holiday</span>
            </div>
            <div class="widget-body">
                <div class="row">
                    {!! Form::model($id,['action'=>['HolidayController@update',$id->id],'method'=>'PATCH']) !!}
                    <div class="col-sm-8 col-sm-offset-2">
                        @include('holiday._form')
                    </div>
                    <div class="col-xs-12">
                        @include('holiday._form_child')
                    </div>
                    {!! Form::close() !!}
                    @include('holiday._form_child_model')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<!--Page Related Scripts-->
<script src="{!! asset('assets/js/datetime/bootstrap-datepicker.js') !!}"></script>
{{--<script src="{!! asset('assets/js/datetime/bootstrap-timepicker.js') !!}"></script>--}}
{{--<script src="{!! asset('assets/js/datetime/moment.js') !!}"></script>--}}
{{--<script src="{!! asset('assets/js/datetime/daterangepicker.js') !!}"></script>--}}
{{--Table--}}
<script src="{!! asset('assets/js/datatable/jquery.dataTables.min.js') !!}"></script>
<script src="{!! asset('assets/js/datatable/ZeroClipboard.js') !!}"></script>
<script src="{!! asset('assets/js/datatable/dataTables.tableTools.min.js') !!}"></script>
<script src="{!! asset('assets/js/datatable/dataTables.bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/js/datatable/datatables-holiday-init.js') !!}"></script>
<script>
    getvalueForSelect2("select[name='type']",'holiday_types',['id','name'],[],'id','name');
    InitiateHolidayChildTableTable.init("{{ $id->id }}");
</script>
@endsection