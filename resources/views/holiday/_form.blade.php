<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('name','Name',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('year') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('year','Year') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('year',null,['class'=>'form-control']) !!}
        {!! $errors->first('year','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>
{{--

@section('footer_script')
    <script src="{!! asset('assets/js/datetime/bootstrap-datepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/datetime/bootstrap-timepicker.js') !!}"></script>
    <script src="{!! asset('assets/js/datetime/moment.js') !!}"></script>
    <script src="{!! asset('assets/js/datetime/daterangepicker.js') !!}"></script>
    <script>
        var checkout = $('[name="year"]').datepicker();
    </script>
@endsection--}}
