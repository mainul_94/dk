<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/3/16
 * Time: 7:52 PM
 */
?>

<div class="table-toolbar">
    <a id="holidayChildTable_new" href="javascript:void(0);" class="btn btn-default">Add New Row</a>
    <a id="holidayChildTable_new_multiple" href="" class="btn btn-default"
       data-toggle="modal" data-target="#holidayChildTable_new_multiple_model">Add New Multiple Row</a>
{{--    {{ $id->children }}--}}
</div>
<table class="table table-striped table-hover table-bordered" id="holidayChildTable">
    <thead>
    <tr role="row">
        <th>Sl. No</th>
        <th>Date</th>
        <th>Type</th>
        <th>purpose</th>
        <th>Details</th>
        <th>Action</th>
    </tr>
    </thead>

    <tbody>
    @if($id->children)
        @foreach($id->children as $child)
            <tr data-id="{{$child->id}}">
                <td>#</td>
                <td>{{ $child->date }}</td>
                @if($child->type)
                    <td data-value="{{ $child->type->id }}">{!! $child->type->name !!}</td>
                @else
                    <td data-value=""></td>
                @endif
                <td>{!! $child->name !!}</td>
                <td>{!! $child->details !!}</td>
                <td>
                    <a href="#" class="btn btn-info btn-xs edit"><i class="fa fa-edit"></i> Edit</a>
                    <a href="#" class="btn btn-danger btn-xs delete"><i class="fa fa-trash-o"></i> Delete</a>
                </td>
            </tr>
        @endforeach
    @endif
    </tbody>
</table>