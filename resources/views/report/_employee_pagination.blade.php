<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/30/16
 * Time: 2:36 AM
 */
?>
@php
	$url = str_replace('page='.$employees->currentPage(),'',request()->fullUrl());
	$find   = '?';
	$pos = strpos($url, $find);
	$url = $pos!==false?$url:$url.'?';
	$url = str_replace('&&','&',$url.'&page=');
	$totals = ceil($employees->total()/$employees->perPage());
	$start = $employees->currentPage()-4 >=1? $employees->currentPage()-4:1;
	$last = $employees->currentPage()+4 <=$employees->lastPage()? $employees->currentPage()+4:$employees->lastPage();
	$totals = $totals >= $last+4 ? $last+4:$totals;
@endphp
<ul class="pagination">
	@if($employees->currentPage() <=1)
		<li class="disabled"><span rel="prev">«</span></li>
	@else
		<li><a href="{{ $url.($employees->currentPage()-1) }}" rel="prev">«</a></li>
	@endif

	@for($page = $start; $page <= $totals; $page++)
		@if($employees->currentPage() === $page)
			<li class="active"><span>{{ $employees->currentPage() }}</span></li>
		@else
			<li><a href="{{ $url.$page }}">{{ $page }}</a></li>
		@endif
	@endfor
	@if($employees->currentPage() < $employees->lastPage())
		<li><a href="{{ $url.($employees->currentPage()+1) }}" rel="next">»</a></li>
	@else
		<li class="disabled"><span rel="next">»</span></li>
	@endif
</ul>
<div class="clearfix"></div>