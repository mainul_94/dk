<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/24/16
 * Time: 9:00 PM
 */
?>
@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Over Time Summary Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@OtSummery'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('from_date') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('from_date','From Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::date('from_date',$from_date,['class'=>'form-control']) !!}
                                {!! $errors->first('from_date','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-4 {{ $errors->has('to_date') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('to_date','To Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::date('to_date',$to_date,['class'=>'form-control']) !!}
                                {!! $errors->first('to_date','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-resposive reportForPrint">
                        <table class="table table-stript table-bordered" id="OtSummery">

                            <caption>
                                <div class="text-center">
                                   <h2>DK Global Fashion Ware Limited</h2>
                                    <p>Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
                                </div>
                                {{--<p>
                                    Attendance Summary for the Date of : {{ date('d-M-Y',strtotime($date)) }}
                                    <span class="pull-right">
                                        {{ date('d/m/Y') }}
                                    </span>
                                </p>--}}
                            </caption>
                            <thead>
                            <tr>
                                <th>SL</th>
                                <th>Line</th>
                                <th>Employees</th>
                                <th>Ot Hours</th>
                                <th>Ext Ot Hours</th>
                            </tr>
                            </thead>
                            <tbody>
                            @inject('line', '\App\Line')
                            @php $sl = 1 @endphp
                            @foreach($lines as $key=>$row)
                                <tr>
                                    <td>{!! $sl++ !!}</td>
                                    <td>{!! $line->find($key)->name !!}</td>
                                    <td>{!! $row['employees'] !!}</td>
                                    <td>{!! $row['ot_hours'] !!}</td>
                                    <td>{!! $row['ext_ot_hours'] !!}</td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tr>
                                <th></th>
                                <th>Total</th>
                                <th>{!! $totals['employees'] !!}</th>
                                <th>{!! $totals['ot_hours'] !!}</th>
                                <th>{!! $totals['ext_ot_hours'] !!}</th>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='designation_id']",'designations',['id','name'],[['section_id']],'id','name');
    </script>
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateAttendanceSummaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#OtSummery').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateAttendanceSummaryReportTable.init();
    </script>
@endsection