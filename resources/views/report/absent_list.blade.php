<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/24/16
 * Time: 9:00 PM
 */
?>
@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Daily Absent Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['action'=>['ReportController@dailyAttendance'],'method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('date') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('date','Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::date('date',$date,['class'=>'form-control']) !!}
                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('section_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('section_id','Section') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
                                {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('employee_id','Employee') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('employee_id',$employees,$employee_id,['class'=>'form-control']) !!}
                                {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-resposive reportForPrint">
                        <table class="table table-stript table-bordered" id="attendanceSummary">

                            <caption>
                                <div class="text-center">
                                    <h2><b>DK Global Fashion Ware Limited</b></h2>
                                    <p>Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
									<p><b>Daily Absent Report</b></p>
                                </div>
                               
                                     Date : {{ date('d-M-Y',strtotime($date)) }}
                                    {{--<span class="pull-right">
                                        {{ date('d/m/Y') }}
                                    </span>--}}
                                
                            </caption>
                            <thead>
                            <tr>
                                <th width="10%" style="text-align:center;">Section</th>
                                <th width="10%" style="text-align:center;">ID</th>
                                <th width="15%" style="text-align:center;">Name</th>
                                <th width="15%" style="text-align:center;">Designation</th>
                                <th width="10%" style="text-align:center;">In Time</th>
                                <th width="10%" style="text-align:center;">L Atten</th>
                                <th width="5%" style="text-align:center;">Total Absent</th>
                                <th width="10%" style="text-align:center;">Status</th>
                                <th width="10%" style="text-align:center;">Signature</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($rows)
                                @foreach($rows as $row)
                                    <tr>
                                        <th>Section :</th>{{--Secton Name--}}
                                        <th>{!! $row->name !!}{{--*/ $section_total = 0; /*--}}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        {{--<th></th>--}}
                                        <th></th>
                                    </tr>
                                    @foreach($row->allLines as $line)
                                        <tr>
                                            <th>Line NO :</th>{{--Line Name--}}
                                            <th>{!! $line->name !!}{{--*/ $line_total = 0; /*--}}</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            {{--<th></th>--}}
                                            <th></th>
                                        </tr>
                                        @foreach($line->employee()->active()->get() as $employee)
                                            {{--*/ $attendance = $employee->attendance->where('date',$date)->where('status','Present'); /*--}}
                                            @include('report._absent_list_emp_row',['employee'=>$employee,
                                            'designation'=>$employee->designation,'attendance'=> $attendance])
                                            @if(count($attendance)<= 0)
                                            {{--*/ $section_total++; $line_total++ /*--}}
                                            @endif
                                        @endforeach
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            {{--<th></th>--}}
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th class="text-right">Block Total</th>
                                            <th>{{$line_total}}</th>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        {{--<th></th>--}}
                                        <th></th>
                                        <th></th>
                                        <th class="text-right">Section Total</th>
                                        <th>{{$section_total}}</th>
                                    </tr>
									
                                @endforeach
                            @elseif($employee)
                                {{--*/ $attendance = $employee->attendance->where('date',$date)->where('status','Present'); /*--}}
                                @include('report._absent_list_emp_row',['employee'=>$employee,
                                                'designation'=>$employee->designation,'attendance'=> $attendance,'show_sec'=>1])
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
	Authorization Signature
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_id']",'employees',['id','name'],[],'id','name');
    </script>
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateAttendanceSummaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#attendanceSummary').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateAttendanceSummaryReportTable.init();
    </script>
@endsection