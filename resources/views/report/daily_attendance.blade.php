<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 7/24/16
 * Time: 9:00 PM
 */
?>
@extends('layouts.layout')
@section('content')
    <div class="row">
        <div class="col-xs-12 col-md-12">
            <div class="widget">
                <div class="widget-header ">
                    <span class="widget-caption">Daily Attendance Report</span>
                </div>
                <div class="widget-body">
                    <div class="col-xs-12">
                        {!! Form::open(['route'=>'daily-attendance','method'=>'GET']) !!}
                        <div class="form-group col-xs-4 {{ $errors->has('date') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('date','Date',['class'=>'field-required']) !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::date('date',$date,['class'=>'form-control']) !!}
                                {!! $errors->first('date','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-3 {{ $errors->has('section_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('section_id','Section') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('section_id',$sections,$section_id,['class'=>'form-control']) !!}
                                {!! $errors->first('section_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>
                        <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                            <div class="col-sm-4 text-right">
                                {!! Form::label('employee_id','Employee') !!}
                            </div>
                            <div class="col-sm-8">
                                {!! Form::select('employee_id',$employees,$employee_id,['class'=>'form-control']) !!}
                                {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                            </div>
                        </div>

                        <div class="col-xs-1">
                            <div class="col-xs-10 pull-right">
                                <button type="submit" class="btn btn-info">Filter</button>
                            </div>
                        </div>
                        {!! Form::close() !!}

                    </div>
                    <div class="clearfix"></div>
                    <div class="table-resposive reportForPrint">
                        <table class="table table-stript table-bordered" id="attendanceSummary">

                            <caption>
                                <div class="text-center">
                                    <h2>DK Global Fashion Ware Limited</h2>
                                    <p>Molla Super Market,Beron,Jamgora,Ashulia,Saver,Dhaka</p>
                                </div>
                                <p>
                                    Daily Attendance Report for the Date of : {{ date('d-M-Y',strtotime($date)) }}
                                    <span class="pull-right">
                                        {{ date('d/m/Y') }}
                                    </span>
                                </p>
                            </caption>
                            <thead>
                            <tr>
                                <th>Section</th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Designation</th>
                                <th></th>
                                <th>InTime</th>
                                <th>OutTime</th>
                                <th>Remark</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($rows)
                                @foreach($rows as $row)
                                    <tr>
                                        <th>{!! $row->name !!}</th>{{--Secton Name--}}
                                        <th>{{--*/ $section_total = 0; /*--}}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                    @foreach($row->allLines as $line)
                                        <tr>
                                            <th>{!! $line->name !!}</th>{{--Line Name--}}
                                            <th>{{--*/ $line_total = 0; /*--}}</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        @foreach($line->allDesignations as $designation)
                                            {{--*/ $designation_total = 0; /*--}}
                                            @foreach($designation->employee as $employee)
                                                {{--*/ $line_total++; $designation_total++; $section_total++; /*--}}
                                                @include('report._daily_attendance.emp_row',['employee'=>$employee,
                                                'designation'=>$designation])
                                            @endforeach
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th>{!! $designation->name !!}</th>
                                                <th>{{$designation_total}}</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <th>Line Total</th>
                                            <th>{{$line_total}}</th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    @endforeach
                                    <tr>
                                        <th></th>
                                        <th>{{$section_total}}</th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                @endforeach
                            @elseif($employee)
                                @include('report._daily_attendance.emp_row',['employee'=>$employee,
                                                'designation'=>$employee->designation,'section'=>$employee->section->name])
                            @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_script')
    <script>
        getvalueForSelect2("select[name='section_id']",'sections',['id','name'],[],'id','name');
        getvalueForSelect2("select[name='employee_id']",'employees',['id','name'],[],'id','name');
    </script>
    <script src="{{ url('assets/js/datatable/jquery.dataTables.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ url('assets/js/datatable/dataTables.bootstrap.min.js') }}"></script>
    <script>
        var InitiateAttendanceSummaryReportTable = function() {
            return {
                init: function() {
                    var oTable = $('#attendanceSummary').dataTable({
                        "sDom": "Tflt<'row DTTTFooter'<'col-sm-6'i><'col-sm-6'p>>",
                        // "aLengthMenu": [
                        //     [5, 15, 20, -1],
                        //     [5, 15, 20, "All"]
                        // ],
                        // "iDisplayLength": 10,
                        "oTableTools": {
                            "aButtons": [
                                "print"
                            ]
                        },
                        "language": {
                            "search": "",
                            "sLengthMenu": "_MENU_",
                            "oPaginate": {
                                "sPrevious": "Prev",
                                "sNext": "Next"
                            }
                        },
                        "searching": false,
                        "paging":   false,
                        "ordering": false,
                        "info":     false
                    });

                    /*$("tfoot input").keyup(function() {
                     /!* Filter on the column (the index) of this element *!/
                     oTable.fnFilter(this.value, $("tfoot input").index(this));
                     });*/
                    $('.DTTT_button_print').on('click',function (event) {
                        event.preventDefault();
                        window.print();
                    });
                }
            };
        }();
        InitiateAttendanceSummaryReportTable.init();
    </script>
@endsection