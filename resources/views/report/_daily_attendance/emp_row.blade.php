<tr>
    <td>@if(!empty($section)){!! $section !!}@endif</td>
    <td>{!! $employee->card_no !!}</td>
    <td>{!! $employee->name !!}</td>
    <td>{!! $designation->name !!}</td>
    {{--*/ $attendance = $employee->attendance->where('date',$date) /*--}}
    @if(count($attendance)>0)
        <td>{!! $attendance[0]->status == "Present"?"P":"A" !!}</td>
        <td>{!! $attendance[0]->in_time !!}</td>
        <td>{!! !empty($maxOt)? noneComponentTime($attendance[0]->out_time,$attendance[0]->overtime,$maxOt)[0]: $attendance[0]->out_time !!}</td>
        <td>{!! $attendance[0]->remark !!}</td>
    @else
        <td>A</td>
        <td></td>
        <td></td>
        <td></td>
    @endif

</tr>