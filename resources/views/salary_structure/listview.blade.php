<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
?>


@extends('layouts.layout')


@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="well with-header">
            <div class="header bg-info">
                <div class="row">
                    <div class="col-sm-7 col-xs-12">
                        Salary Structure List
                    </div>
                </div>
            </div>

            <div class="col-xs-12 text-right padding-bottom-20px">
                <a href="{!! action('SalaryStructureController@create') !!}" class="btn btn-info">Create</a>
            </div>
            @if(!empty(session()->has('message')))
                <div class="col-xs-12">
                    <div class="alert alert-{{session('message')['type']}} fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-check"></i>
                        <strong>Success</strong> {!! session('message')['msg'] !!}
                    </div>
                </div>
            @endif
            @if(count($rows)>0)
            <table class="table table-hover table-bordered">
                <thead class="bordered-blue">
                <tr>
                    <th>Sl.No</th>
                    <th>Employee</th>
                    <th>Start Date</th>
                    <th>End Date</th>
                    <th>Total Earning</th>
                    <th>Total Deduction</th>
                    <th>Gross Total</th>
                    <th>Status</th>
                    <th>Created By</th>
                    <th>updated By</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @foreach($rows as $row)
                        <tr>
                            <td>{!! $sl !!} <?php $sl++; ?></td>
                            <td>{!! $row->employee->name !!}</td>
                            <td>{!! $row->start_date !!}</td>
                            <td>{!! $row->end_date !!}</td>
                            <td>{!! $row->total_earning !!}</td>
                            <td>{!! $row->total_deduction !!}</td>
                            <td>{!! $row->gross_total !!}</td>
                            <td>{!! $row->status !!}</td>
                            <td>{!! $row->createdBy->name !!}</td>
                            <td>
                                @if($row->updateBy)
                                    {!! $row->updateBy->name !!}
                                @endif
                            </td>
                            <td class="text-center">
                                <a href="{!! action('SalaryStructureController@edit',$row->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
                                {!! delete_data('SalaryStructureController@destroy',$row->id) !!}
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            @else
                <h3 class="text-muted text-center">
                    No Data
                </h3>
            @endif
        </div>

    </div>
</div>
@endsection