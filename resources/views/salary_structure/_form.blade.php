<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif



<div class="form-group col-xs-12 {{ $errors->has('employee_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('employee_id','Employee',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $employee_id = [$id->employee->id => $id->employee->name]  ?>
        @else
            <?php $employee_id = []  ?>
        @endif
        {!! Form::select('employee_id',$employee_id,null,['class'=>'form-control',(!empty($id)?'disabled="disabled"':'')]) !!}
        {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('start_date') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('start_date','Start Date') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::date('start_date',null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('start_date','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('end_date') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('end_date','End Date') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::date('end_date',null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('end_date','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('total_earning') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('total_earning','Total Earning') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::number('total_earning',null,['class'=>'form-control','readonly'=>'readonly']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('total_earning','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('total_deduction') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('total_deduction','Total Detuction') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::number('total_deduction',null,['class'=>'form-control','readonly'=>'readonly']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('total_deduction','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('gross_total') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('gross_total','Gross Total') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::number('gross_total',null,['class'=>'form-control','readonly'=>'readonly']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('gross_total','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('status','Status') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::select('status',['Active'=>'Active','Inactive'=>'Inactive'],null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('status','<span class="help-block">:message</span>') !!}
    </div>
</div>

<table class="table">
    <thead>
        <tr class="info">
            <th>Salary Type</th>
            <th>Amount</th>
            <th>Min Amount</th>
            <th>Max Amount</th>
        </tr>
    </thead>
    <tbody>
        @forelse($salaryTypes as $salaryType)
            <tr data-salry-type-id="{{ $salaryType->id }}">
                <td>
                    @if(!empty($types))
                        @foreach($types as $type)
                            @if($type['salary_type_id'] == $salaryType->id)
                                {!! Form::model($type) !!}
                                {!! Form::hidden('ch_id[]',null) !!}
                            @endif
                        @endforeach
                    @endif

                    <div class="form-group {{ $errors->has('salary_type_id[]') ? 'has-error':''}}">
                        {!! Form::select('salary_type_id[]',[$salaryType->id=>$salaryType->name],null,['class'=>'form-control']) !!}
                        {!! $errors->first('salary_type_id[]','<span class="help-block">:message</span>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group {{ $errors->has('amount[]') ? 'has-error':''}}">
                        {!! Form::number('amount[]',null,['class'=>'form-control','data-earning'=>$salaryType->earning,
                        'data-deduction'=>$salaryType->deduction]) !!}
                        {!! $errors->first('amount[]','<span class="help-block">:message</span>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group {{ $errors->has('min_amount[]') ? 'has-error':''}}">
                        {!! Form::number('min_amount[]',null,['class'=>'form-control','disabled'=>'disabled']) !!}
                        {!! $errors->first('min_amount[]','<span class="help-block">:message</span>') !!}
                    </div>
                </td>
                <td>
                    <div class="form-group {{ $errors->has('max_amount[]') ? 'has-error':''}}">
                        {!! Form::number('max_amount[]',null,['class'=>'form-control','disabled'=>'disabled']) !!}
                        {!! $errors->first('max_amount[]','<span class="help-block">:message</span>') !!}
                    </div>
                </td>
            </tr>
        @empty
            <tr>
                <td colspan="4"><h2>No Salary Type Avalable </h2></td>
            </tr>
        @endforelse
    </tbody>
</table>


<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>


@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','name'],[],'id','name');
        $('[name="status"]').select2();

//        On Employee Change Effect
        $("select[name='employee_id']").on('change', function () {
            getValue("{!! url('employee') !!}/"+$(this).val(), function (data) {
                if (data) {
                    $.ajax({
                        url:"{!! action('SettingController@getValues') !!}",
                        method:"GET",
                        data:{
                            table:'salary_settings',
                            fields:"*",
                            filters:[
                                ['designation_id','=',data.designation_id],
                                ['employee_type_id','=',data.employee_type_id]
                            ]
                        },
                        success: function (r) {
                            if (r[0]) {
                                $.ajax({
                                    url:"{!! action('SettingController@getValues') !!}",
                                    method:"GET",
                                    data:{
                                        table:'salary_setting_children',
                                        fields:"*",
                                        filters:[
                                            ['salary_setting_id','=',r[0].id]
                                        ]
                                    },
                                    success: function (datar) {
                                        if (datar) {
                                            for (x in datar) {
                                                var $parent = $('tr[data-salry-type-id="'+datar[x].salary_type_id+'"]');
                                                $parent.find('[name="min_amount[]"]').val(datar[x].min_amount);
                                                $parent.find('[name="max_amount[]"]').val(datar[x].max_amount);
                                            }
                                        }
                                    }
                                });
                            }
                        }
                    });
                }
            });
        });
//        On Amount Change Effect
        $('[name="amount[]"]').on('change', function () {
            var value = $(this).val();
            var $parent = $(this).parents('tr');
            var min_val = $parent.find('[name="min_amount[]"]').val();
            var max_val = $parent.find('[name="max_amount[]"]').val();
            if (parseFloat(value) > parseFloat(max_val)) {
                $(this).val(0);
                swal({
                    title:"Input Value must be less then"+max_val
                })
            }else if (parseFloat(value) < parseFloat(min_val)) {
                $(this).val(0);
                swal({
                    title:"Input Value must be geater then"+min_val
                })
            }else {
                calculate_Totals()
            }
        });


//        calculate_Totals
        function calculate_Totals() {
            var total_earning = 0;
            var total_deduction = 0;
            $('[name="amount[]"]').each(function () {
                if ($(this).data('earning')){
                    total_earning += parseFloat($(this).val()) || 0;
                }
                else if ($(this).data('deduction')){
                    total_deduction += parseFloat($(this).val()) || 0;
                }
            });
            $('[name="total_earning"]').val(total_earning);
            $('[name="total_deduction"]').val(total_deduction);
            $('[name="gross_total"]').val(total_earning-total_deduction);
        }
    </script>
@endsection