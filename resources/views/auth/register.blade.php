@extends('layouts.app')

@section('content')
    <div class="register-container animated fadeInDown">
        <div class="logobox">
            <img src="{!! asset('assets/img/logo.png') !!}" alt="" />
        </div>
        <div class="registerbox bg-white">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                {!! csrf_field() !!}
                <div class="registerbox-title">Register</div>

                <div class="registerbox-caption ">Please fill in your information</div>
                <div class="registerbox-textbox">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <input type="text" class="form-control" name="name" value="{{ old('name') }}"  placeholder="Name">
                        @if ($errors->has('name'))
                            <span class="help-block">
                                  <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="registerbox-textbox">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="registerbox-textbox">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" name="password" placeholder="Enter Password">

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="registerbox-textbox">
                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" name="password_confirmation"  placeholder="Confirm Password">

                        @if ($errors->has('password_confirmation'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password_confirmation') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="registerbox-textbox no-padding-bottom">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="colored-primary" checked="checked">
                            <span class="text darkgray">I agree to the Company <a class="themeprimary">Terms of Service</a> and Privacy Policy</span>
                        </label>
                    </div>
                </div>
                <div class="registerbox-submit">
                    <button type="submit" class="btn btn-primary">
                        <i class="fa fa-btn fa-user"></i>Register
                    </button>
                    {{--<input type="button" class="btn btn-primary pull-right" value="SUBMIT">--}}
                </div>
            </form>
        </div>
    </div>
@endsection
