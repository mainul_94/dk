@extends('layouts.app')

@section('content')
    <div class="login-container animated fadeInDown">
        <div class="logobox">
            <img src="{!! asset('assets/img/logo.png') !!}" alt="" />
        </div>
        <div class="loginbox bg-white">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {!! csrf_field() !!}
                {{--<div class="loginbox-title">SIGN IN</div>--}}
                {{--<div class="loginbox-social">
                    <div class="social-title ">Connect with Your Social Accounts</div>
                    <div class="social-buttons">
                        <a href="#" class="button-facebook">
                            <i class="social-icon fa fa-facebook"></i>
                        </a>
                        <a href="#" class="button-twitter">
                            <i class="social-icon fa fa-twitter"></i>
                        </a>
                        <a href="#" class="button-google">
                            <i class="social-icon fa fa-google-plus"></i>
                        </a>
                    </div>
                </div>--}}
                {{--<div class="loginbox-or">
                    <div class="or-line"></div>
                    <div class="or">OR</div>
                </div>--}}
                <div class="loginbox-textbox">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" name="email" placeholder="Email" value="{{ old('email') }}">
                        @if ($errors->has('email'))
                            <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                        @endif
                    </div>
                </div>
                <div class="loginbox-textbox">
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input type="password" class="form-control" name="password" placeholder="Password">

                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                    </div>
                </div>
                {{--<div class="loginbox-textbox">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" class="colored-primary" name="remember" checked="checked">
                            <span class="text darkgray">Remember Me</span>
                        </label>
                    </div>
                </div>
                <div class="loginbox-forgot">
                    <a href="{{ url('/password/reset') }}">Forgot Password?</a>
                </div>--}}
                <div class="loginbox-submit">
                    <input type="submit" class="btn btn-primary btn-block" value="Login">
                </div>
                
            </form>
        </div>
    </div>
@endsection
