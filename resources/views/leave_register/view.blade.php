<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 5/5/16
 * Time: 12:23 AM
 */
?>

@extends('layouts.layout')

@section('content')

    <div class="row">
        <div class="col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-bottom bordered-blue">
                    <span class="widget-caption">Employee Leave Register</span>
                </div>

                <div class="widget-body">
                    <div class="col-xs-12 text-right padding-bottom-20px">
                        <a href="{!! action('LeaveRegisterController@index') !!}" class="btn btn-info">Back to List View</a>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            {{--<div class="col-sm-4"><strong class="pull-right">Employee</strong></div>
                            <div class="col-sm-8">{!! $id->employee->name !!}</div>
                            <div class="col-sm-4"><strong class="pull-right">Start Date</strong></div>
                            <div class="col-sm-8">{!! $id->p_start_date !!}</div>
                            <div class="col-sm-4"><strong class="pull-right">End Date</strong></div>
                            <div class="col-sm-8">{!! $id->p_end_date !!}</div>
                            <div class="col-sm-4"><strong class="pull-right">Days</strong></div>
                            <div class="col-sm-8">{!! $id->p_end_date !!}</div>
                            <div class="col-sm-4"><strong class="pull-right">Status</strong></div>
                            <div class="col-sm-8">{!! $id->status !!}</div>--}}
                            <table class="table">
                                <tr>
                                    <th width="200">Employee</th>
                                    <td>{!! $id->employee->name !!}</td>
                                </tr>
                                <tr>
                                    <th>Start Date</th>
                                    <td>{!! $id->p_start_date !!}</td>
                                </tr>
                                <tr>
                                    <th>End Date</th>
                                    <td>{!! $id->p_end_date !!}</td>
                                </tr>
                                <tr>
                                    <th>Days</th>
                                    <td>{!! $id->p_days !!}</td>
                                </tr>
                            </table>
                            <table class="table">
                                <tr class="success">
                                    <th>Leave Type</th>
                                    <th>Start Date</th>
                                    <th>End Date</th>
                                    <th>Days</th>
                                    <th>Payable</th>
                                </tr>
                                @foreach($id->details as $detail)
                                    <tr>
                                        <td>{!! $detail->leaveType->name !!}</td>
                                        <td>{!! $detail->start_date !!}</td>
                                        <td>{!! $detail->end_date !!}</td>
                                        <td>{!! $detail->days !!}</td>
                                        <td>{!! $detail->payable !!}</td>
                                    </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection