<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/27/16
 * Time: 8:58 AM
 */
        ?>

<table class="table" id="avalableLeaveView">
    <thead>
        <tr>
            <th>Leave Type</th>
            <th>Day</th>
        </tr>
    </thead>
    <tbody>
        @if(!empty($id) && !empty($avalableLeaves))
            @foreach($avalableLeaves as $avalableLeave)
                <tr>
                    <td>{!! $avalableLeave->name !!}</td>
                    <td>{!! $avalableLeave->leave_days !!}</td>
                </tr>
            @endforeach
        @endif
    </tbody>
</table>
