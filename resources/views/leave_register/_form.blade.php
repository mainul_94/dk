<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">×</button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif

<div class="form-group col-xs-12 {{ $errors->has('employee_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('employee_id','Employee',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $employee_id = [$id->employee->id => $id->employee->name]  ?>
        @else
            <?php $employee_id = []  ?>
        @endif
        {!! Form::select('employee_id',$employee_id,null,['class'=>'form-control']) !!}
        {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('p_start_date') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('p_start_date','Start Date',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::date('p_start_date',null,['class'=>'form-control']) !!}
        {!! $errors->first('p_start_date','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('p_end_date') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('p_end_date','End Date',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::date('p_end_date',null,['class'=>'form-control']) !!}
        {!! $errors->first('p_end_date','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('p_days') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('p_days','Days',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::number('p_days',null,['class'=>'form-control','readonly'=>'readonly']) !!}
        {!! $errors->first('p_days','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('leave_approval') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('leave_approval','Approval',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        @if(!empty($id))
            <?php $leave_approval = [$id->leaveApproval->id => $id->leaveApproval->name]  ?>
        @else
            <?php $leave_approval = []  ?>
        @endif
        {!! Form::select('leave_approval',$leave_approval,null,['class'=>'form-control']) !!}
        {!! $errors->first('leave_approval','<span class="help-block">:message</span>') !!}
    </div>
</div>
@if(!empty($id) && auth()->user()->id == $id->leave_approval)
<div class="form-group col-xs-12 {{ $errors->has('status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('status','Status',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('status',['Pending'=>'Pending','Approved'=>'Approved','Rejected'=>'Rejected'],null,['class'=>'form-control']) !!}
        {!! $errors->first('status','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('is_permanent_save') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('is_permanent_save','Permanent Save',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('is_permanent_save',['Yes'=>'Yes','No'=>'No'],null,['class'=>'form-control']) !!}
        {!! $errors->first('is_permanent_save','<span class="help-block">:message</span>') !!}
    </div>
</div>
@endif
<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>
