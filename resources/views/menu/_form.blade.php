<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('title') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('title','Title',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('title',null,['class'=>'form-control']) !!}
        {!! $errors->first('title','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('icon') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('icon','Icon') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('icon',null,['class'=>'form-control']) !!}
        {!! $errors->first('icon','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('url') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('url','URL') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('url',null,['class'=>'form-control']) !!}
        {!! $errors->first('url','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('parent_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('parent_id','Parent') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('parent_id',$menus,null,['class'=>'form-control']) !!}
        {!! $errors->first('parent_id','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('order_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('order_id','Order') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::number('order_id',null,['class'=>'form-control']) !!}
        {!! $errors->first('order_id','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>

@section('footer_script')


    <script>
        $('select[name="parent_id"]').select2();
    </script>
@endsection
