<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 5/20/16
 * Time: 10:46 PM
 */
        ?>
@extends('layouts.layout')

@section('content')
    <div class="row">
        <div class="col-xs-12">
            <div class="widget">
                <div class="widget-header bordered-bottom bordered-blue">
                    <span class="widget-caption">Import Employee From Excel</span>
                </div>
                <div class="widget-body">
                    <div class="row">
                        <div class="col-sm-8 col-sm-offset-2">
                            @if(!empty(session()->has('message')))
                                <div class="col-xs-12">
                                    <div class="alert alert-{{session('message')['type']}} fade in">
                                        <button class="close" data-dismiss="alert">
                                            ×
                                        </button>
                                        <i class="fa-fw fa fa-check"></i>
                                        <strong>Success</strong> {!! session('message')['msg'] !!}
                                    </div>
                                </div>
                            @endif
                            {!! Form::open(['action'=>['EmployeeController@importEmployee'],'files'=>true]) !!}
                                {!! Form::file('file') !!}
                                <br>
                                {{--{!! Form::submit('Save') !!}--}}
                                <div class="col-xs-12">
                                    <div class="col-xs-10 pull-right">
                                        <button type="submit" class="btn btn-blue">Save</button>
                                    </div>
                                    {{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection