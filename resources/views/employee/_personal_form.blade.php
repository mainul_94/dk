<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/8/16
 * Time: 6:09 PM
 */
?>
{{--@if(isset($id))--}}
{{--{!! Form::model($id->getEmployeePersonalInfo,['file'=>true]) !!}--}}
{{--@endif--}}
<div class="form-group col-xs-12 {{ $errors->has('fathers_name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('fathers_name','Father\'s Name') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('fathers_name',null,['class'=>'form-control']) !!}
        {!! $errors->first('fathers_name','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('mothers_name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('mothers_name','Mother\'s Name') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('mothers_name',null,['class'=>'form-control']) !!}
        {!! $errors->first('mothers_name','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('husband_name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('husband_name','Housband\'s Name') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('husband_name',null,['class'=>'form-control']) !!}
        {!! $errors->first('husband_name','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('photo') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('photo','Photo') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::file('photo') !!}
        {!! $errors->first('photo','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('permanent_address') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('permanent_address','Permanent Address') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('permanent_address',null,['class'=>'form-control']) !!}
        {!! $errors->first('permanent_address','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('bangla_permanent_address') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('bangla_permanent_address','Permanent Address (Bangla)') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('bangla_permanent_address',null,['class'=>'form-control']) !!}
        {!! $errors->first('bangla_permanent_address','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('present_address') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('present_address','Present Address') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('present_address',null,['class'=>'form-control']) !!}
        {!! $errors->first('present_address','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('bangla_present_address') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('bangla_present_address','Present Address (Bangla)') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('bangla_present_address',null,['class'=>'form-control']) !!}
        {!! $errors->first('bangla_present_address','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('contact_no') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('contact_no','Contact No') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('contact_no',null,['class'=>'form-control']) !!}
        {!! $errors->first('contact_no','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('emergency_contact_no') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('emergency_contact_no','Emergency Contact') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('emergency_contact_no',null,['class'=>'form-control']) !!}
        {!! $errors->first('emergency_contact_no','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('national_id') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('national_id','National ID') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('national_id',null,['class'=>'form-control']) !!}
        {!! $errors->first('national_id','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('nationality') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('nationality','Nationality') !!}
    </div>
    <div class="col-sm-10">
            <?php $nationality = []  ?>
        {!! Form::select('nationality',$nationality,null,['class'=>'form-control']) !!}
        {!! $errors->first('nationality','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('date_of_birth') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('date_of_birth','Date of Birth') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('date_of_birth',null,['class'=>'form-control']) !!}
        {!! $errors->first('date_of_birth','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('religion') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('religion','Religion') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('religion',['Islam'=>'Islam','Judaism'=>'Judaism','Christianity'=>'Christianity','Hinduism'=>'Hinduism',
        'Buddhism'=>'Buddhism','Others'=>'Others'],null,['class'=>'form-control']) !!}
        {!! $errors->first('religion','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('marital_status') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('marital_status','Marital Status') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::select('marital_status',['Yes'=>'Yes', 'No'=>'No'],null,['class'=>'form-control']) !!}
        {!! $errors->first('marital_status','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('number_of_children') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('number_of_children','Number of Children') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('number_of_children',null,['class'=>'form-control']) !!}
        {!! $errors->first('number_of_children','<span class="help-block">:message</span>') !!}
    </div>
</div>
