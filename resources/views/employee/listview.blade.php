<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:05 PM
 */
$sl = 1;
if (!empty(request()->get('page'))) {
    $sl += (request()->get('page')-1) *15;
}
?>


@extends('layouts.layout')


@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="well with-header">
            <div class="header bg-info">
                <div class="row">
                    <div class="col-sm-7 col-xs-12">
                        Employee List
                    </div>
                </div>
            </div>

            <div class="col-xs-12 padding-bottom-20px">
                {!! Form::open(['action'=>'EmployeeController@index', 'method'=>'get']) !!}
                <div class="form-group col-xs-4 {{ $errors->has('employee_id') ? 'has-error':''}}">
                    <div class="col-sm-4 text-right">
                        {!! Form::label('employee_id','Employee') !!}
                    </div>
                    <div class="col-sm-8">
                        {!! Form::select('employee_id',!empty($employees)?$employees:[],!empty($employee_id)?$employee_id:null,['class'=>'form-control']) !!}
                        {!! $errors->first('employee_id','<span class="help-block">:message</span>') !!}
                    </div>
                </div>
                @if(!empty($employee_id))
                <div class="col-xs-4">
                    <a href="{{ action('EmployeeController@index') }}" class="btn btn-warning">Reset</a>
                </div>
                @endif
                {!! Form::close() !!}
                <a href="{!! action('EmployeeController@create') !!}" class="btn btn-info pull-right">Create</a>
            </div>
            @if(!empty(session()->has('message')))
                <div class="col-xs-12">
                    <div class="alert alert-{{session('message')['type']}} fade in">
                        <button class="close" data-dismiss="alert">
                            ×
                        </button>
                        <i class="fa-fw fa fa-check"></i>
                        <strong>Success</strong> {!! session('message')['msg'] !!}
                    </div>
                </div>
            @endif
            @if(count($rows)>0)
            <table class="table table-hover table-bordered">
                <thead class="bordered-blue">
                <tr>
                    <th>Sl.No</th>
					<th>ID</th>
                    <th>Name</th>
                    <th>Organization</th>
                    <th>Unit</th>
                    <th>Address</th>
                    <th>Created By</th>
                    <th>updated By</th>
                    <th>Action</th>
                </tr>
                </thead>
                <tbody>
                    @if(!empty($rows->id))
                        @include('employee._row',['row'=>$rows])
                    @else
                        @foreach($rows as $row)
                            @include('employee._row',['row'=>$row])
                            {{--*/ $sl++ /*--}}
                        @endforeach
                    @endif
                </tbody>
            </table>
            @else
                <h3 class="text-muted text-center">
                    No Data
                </h3>
            @endif
        </div>
        <div>
            @if(empty($rows->id))
                {!! $rows->render() !!}
            @endif
        </div>

    </div>
</div>
@endsection
@section('footer_script')
    <script>
        getvalueForSelect2("select[name='employee_id']",'employees',['id','old_card_no'],[],'id','old_card_no');
        $("select[name='employee_id']").on('change',function () {
            $(this).parents('form').submit();
        });
    </script>
@endsection