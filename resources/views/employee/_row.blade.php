<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 8/10/16
 * Time: 11:13 PM
 */
?>
<tr>
    <td>{!! $sl !!}</td>
	<td>{!! $row->old_card_no !!}</td>
    <td>{!! $row->name !!}</td>
    <td>@if($row->organization){!! $row->organization->name !!}@endif</td>
    <td>@if($row->unit){!! $row->unit->name !!}@endif</td>
    <td>{!! $row->address !!}</td>
    <td>{!! $row->createdBy->name !!}</td>
    <td>
        @if($row->updateBy)
            {!! $row->updateBy->name !!}
        @endif
    </td>
    <td class="text-center">
        <a href="{!! action('EmployeeController@edit',$row->id) !!}" ><i class="fa fa-pencil-square-o text-danger"></i></a>
        {!! delete_data('EmployeeController@destroy',$row->id) !!}
    </td>
</tr>
