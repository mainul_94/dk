<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 4/8/16
 * Time: 6:09 PM
 */
?>
{{--@if(isset($id))--}}
{{--{!! Form::model($id->getEmployeeOrganizationInfo,['enctype'=>'multipart/form-data', 'file'=>true]) !!}--}}
{{--@endif--}}
<div class="form-group col-xs-12 {{ $errors->has('name_of_nominees') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('name_of_nominees','Name of Nominees') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('name_of_nominees',null,['class'=>'form-control']) !!}
        {!! $errors->first('name_of_nominees','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('address_of_nominees') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('address_of_nominees','Address of Nominees') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('address_of_nominees',null,['class'=>'form-control']) !!}
        {!! $errors->first('address_of_nominees','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('relation_with_nominees') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('relation_with_nominees','Relation with Nominees') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('relation_with_nominees',null,['class'=>'form-control']) !!}
        {!! $errors->first('relation_with_nominees','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('authorized_percent_for_nominees') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('authorized_percent_for_nominees','Percent for Authorized') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('authorized_percent_for_nominees',null,['class'=>'form-control']) !!}
        {!! $errors->first('authorized_percent_for_nominees','<span class="help-block">:message</span>') !!}
    </div>
</div>


<div class="form-group col-xs-12 {{ $errors->has('last_organization') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('last_organization','Last Organization') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('last_organization',null,['class'=>'form-control']) !!}
        {!! $errors->first('last_organization','<span class="help-block">:message</span>') !!}
    </div>
</div>


