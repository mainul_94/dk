<?php
/**
 * Created by PhpStorm.
 * User: mainul
 * Date: 3/25/16
 * Time: 4:53 PM
 */
 ?>

@if(!empty(session()->has('message')))
    <div class="col-xs-12">
        <div class="alert alert-{{session('message')['type']}} fade in">
            <button class="close" data-dismiss="alert">
                ×
            </button>
            <i class="fa-fw fa fa-check"></i>
            <strong>Success</strong> {!! session('message')['msg'] !!}
        </div>
    </div>
@endif


<div class="form-group col-xs-12 {{ $errors->has('name') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('name','Name',['class'=>'field-required']) !!}
    </div>
    <div class="col-sm-10">
        {!! Form::text('name',null,['class'=>'form-control']) !!}
        {!! $errors->first('name','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('is_maternity') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('is_maternity','Is Maternity') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::checkbox('is_maternity',1,null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('is_maternity','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('is_earn') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('is_earn','Is Earn') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::checkbox('is_earn',1,null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('is_earn','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('is_payable') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('is_payable','Is Payable') !!}
    </div>
    <div class="col-sm-10">
        <label>
            {!! Form::checkbox('is_payable',1,null,['class'=>'form-control']) !!}
            <span class="text"></span>
        </label>
        {!! $errors->first('is_payable','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="form-group col-xs-12 {{ $errors->has('details') ? 'has-error':''}}">
    <div class="col-sm-2 text-right">
        {!! Form::label('details','Details') !!}
    </div>
    <div class="col-sm-10">
        {!! Form::textarea('details',null,['class'=>'form-control']) !!}
        {!! $errors->first('details','<span class="help-block">:message</span>') !!}
    </div>
</div>

<div class="col-xs-12">
    <div class="col-xs-10 pull-right">
        <button type="submit" class="btn btn-blue">Save</button>
    </div>
{{--    <a href="{!! action('OrganizationController@index') !!}" class="btn btn-warning">Cancel</a>--}}
</div>
