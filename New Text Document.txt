//        Check value Not Min or Max
        function checkValueMinMax(selector) {
            var value = $(selector).val();
            var $parent = $(selector).parents('tr');
            if (parseFloat(value) < parseFloat($parent.find('[name^="min_"]').val())) {
                swal({
                    title:"The Value of field \""+$('[for="'+$(selector).attr('name')+'"]')
                            .text()+"\" Must be geater then "+$parent.find('[name^="min_"]').val()
                },function (isConfirm) {
                    if (isConfirm) {
                        setValueAsZero([selector])
                    }
                });
            }
            if (parseFloat(value) > parseFloat($parent.find('[name^="max_"]').val())) {
                swal({
                    title:"The Value of field \""+$('[for="'+$(selector).attr('name')+'"]')
                            .text()+"\" Must be less then "+$parent.find('[name^="max_"]').val()
                },function (isConfirm) {
                    if (isConfirm) {
                        setValueAsZero([selector])
                    }
                });
            }
        }

        $('[name="basic_salary"]').on('change',function () {
            checkValueMinMax(this)
        });