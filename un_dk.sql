-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 21, 2016 at 09:13 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `un_dkk`
--

-- --------------------------------------------------------

--
-- Table structure for table `attendances`
--

CREATE TABLE IF NOT EXISTS `attendances` (
`id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `in_time` time NOT NULL,
  `out_time` time NOT NULL,
  `duration` time NOT NULL,
  `let_time` time NOT NULL,
  `overtime` time NOT NULL,
  `remark` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Absent','Present') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Present',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE IF NOT EXISTS `departments` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `name`, `address`, `organization_id`, `unit_id`, `division_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, ' HR & Admin', '', 1, 1, 1, 1, 2, '2016-07-21 07:14:03', '2016-11-18 08:02:10', NULL),
(2, 'Commercial', '', 1, 1, 1, 1, NULL, '2016-07-21 07:14:12', '2016-07-21 07:14:12', NULL),
(3, 'Production', '', 1, 1, 1, 1, NULL, '2016-07-21 07:14:21', '2016-07-21 07:14:21', NULL),
(4, 'Maintenance', '', 1, 1, 1, 1, NULL, '2016-07-21 07:14:29', '2016-07-21 07:14:29', NULL),
(5, 'Merchandising', '', 1, 1, 1, 1, NULL, '2016-07-21 07:14:38', '2016-07-21 07:14:38', NULL),
(6, 'Store', '', 1, 1, 1, 1, NULL, '2016-07-21 07:14:49', '2016-07-21 07:14:49', NULL),
(7, 'Washing', '', 1, 1, 1, 1, NULL, '2016-07-21 07:15:00', '2016-07-21 07:15:00', NULL),
(8, 'Quality', '', 1, 1, 1, 1, NULL, '2016-07-21 07:15:09', '2016-07-21 07:15:09', NULL),
(9, 'Sample', '', 1, 1, 1, 1, NULL, '2016-07-21 07:15:21', '2016-07-21 07:15:21', NULL),
(10, 'Account / Finance', '', 1, 1, 1, 1, NULL, '2016-07-21 07:15:34', '2016-07-21 07:15:34', NULL),
(11, 'Admin', '', 1, 1, 1, 1, NULL, '2016-07-21 07:17:55', '2016-07-21 07:17:55', NULL),
(12, 'General Staff', '', 1, 1, 1, 1, NULL, '2016-07-21 07:23:55', '2016-07-21 07:23:55', NULL),
(13, 'Compliance', '', 1, 1, 1, 2, NULL, '2016-11-18 07:53:18', '2016-11-18 07:53:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designations`
--

CREATE TABLE IF NOT EXISTS `designations` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `capacity_of_employee` int(11) NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `line_id` int(10) unsigned NOT NULL,
  `designation_group_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=120 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designations`
--

INSERT INTO `designations` (`id`, `name`, `grade`, `capacity_of_employee`, `organization_id`, `unit_id`, `division_id`, `department_id`, `section_id`, `line_id`, `designation_group_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'JSMO', '5', 100, 1, 1, 1, 3, 16, 1, 6, 1, 2, '2016-07-21 08:11:32', '2016-11-18 08:23:58', NULL),
(2, 'MO', '4', 100, 1, 1, 1, 3, 16, 2, 6, 1, NULL, '2016-07-21 08:18:40', '2016-07-21 08:18:40', NULL),
(3, 'SR.MO', '6', 100, 1, 1, 1, 3, 16, 3, 6, 1, 2, '2016-07-21 21:03:17', '2016-11-18 08:24:15', NULL),
(4, 'GMO', '5', 100, 1, 1, 1, 3, 16, 3, 6, 1, 2, '2016-07-21 21:03:54', '2016-11-18 08:25:23', NULL),
(5, 'Asst.Operator', '7', 100, 1, 1, 1, 3, 16, 3, 6, 1, 2, '2016-07-21 21:04:26', '2016-11-18 06:34:51', NULL),
(6, '.', '7', 100, 1, 1, 1, 3, 16, 3, 6, 1, 2, '2016-07-21 21:05:09', '2016-11-18 06:54:39', NULL),
(7, 'Line Iron Man', '7', 100, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-21 21:05:50', '2016-07-21 21:05:50', NULL),
(8, 'Production Manager', '0', 3, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-21 21:07:44', '2016-07-21 21:07:44', NULL),
(9, 'Supervisor', '0', 5, 1, 1, 1, 3, 16, 5, 6, 1, 1, '2016-07-21 21:08:35', '2016-07-21 21:18:54', NULL),
(10, 'Line Chief', '0', 100, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-21 21:09:41', '2016-07-21 21:09:41', NULL),
(14, 'Jr. Officer', '0', 4, 1, 1, 1, 1, 2, 30, 4, 1, NULL, '2016-07-21 21:26:54', '2016-07-21 21:26:54', NULL),
(15, 'Manager', '0', 5, 1, 1, 1, 1, 9, 14, 4, 1, NULL, '2016-07-21 21:27:29', '2016-07-21 21:27:29', NULL),
(16, 'Sr. Officer', '0', 10, 1, 1, 1, 1, 9, 14, 4, 1, NULL, '2016-07-21 21:27:59', '2016-07-21 21:27:59', NULL),
(17, 'Officer', '0', 20, 1, 1, 1, 1, 9, 14, 4, 1, NULL, '2016-07-21 21:29:41', '2016-07-21 21:29:41', NULL),
(18, 'Jr. Officer', '0', 3, 1, 1, 1, 1, 9, 14, 4, 1, NULL, '2016-07-21 21:35:18', '2016-07-21 21:35:18', NULL),
(19, 'Welfare Officer', '0', 12, 1, 1, 1, 13, 20, 31, 16, 1, 2, '2016-07-21 21:35:48', '2016-11-18 07:56:43', NULL),
(20, 'Manager', '0', 2, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:36:17', '2016-07-21 21:36:17', NULL),
(21, 'Asst. Manager', '0', 100, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:37:09', '2016-07-21 21:37:09', NULL),
(22, 'Sr. Executive', '0', 13, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:37:34', '2016-07-21 21:37:34', NULL),
(23, 'Sr. Officer', '0', 20, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:38:02', '2016-07-21 21:38:02', NULL),
(24, 'Executive', '0', 12, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:38:26', '2016-07-21 21:38:26', NULL),
(25, 'Officer', '0', 20, 1, 1, 1, 2, 4, 9, 5, 1, NULL, '2016-07-21 21:39:00', '2016-07-21 21:39:00', NULL),
(26, 'Manager', '0', 20, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-21 21:40:42', '2016-07-21 21:40:42', NULL),
(27, 'In Charge', '0', 10, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-21 21:41:57', '2016-07-21 21:41:57', NULL),
(28, 'Supervisor', '0', 20, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-21 21:42:29', '2016-07-21 21:42:29', NULL),
(29, 'Cutting Asst', '0', 20, 1, 1, 1, 3, 5, 10, 6, 1, 2, '2016-07-21 21:42:56', '2016-11-18 06:57:38', NULL),
(32, 'Quality Inspector', '4', 23, 1, 1, 1, 8, 14, 10, 11, 1, 2, '2016-07-21 21:45:45', '2016-11-18 07:04:58', NULL),
(33, 'Jr. Fusing Operator', '0', 20, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-21 21:46:15', '2016-07-21 21:46:15', NULL),
(35, 'Jr. Cutter', '5', 20, 1, 1, 1, 3, 5, 10, 6, 1, 2, '2016-07-21 21:47:11', '2016-11-18 08:55:28', NULL),
(38, 'Manager', '1', 0, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-21 21:48:53', '2016-11-18 08:56:11', NULL),
(40, 'In Charge', '1', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-21 21:55:36', '2016-11-18 08:57:07', NULL),
(41, 'Supervisor', '2', 12, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:15:21', '2016-11-18 08:57:50', NULL),
(42, 'Auditor', '7', 12, 1, 1, 1, 3, 7, 12, 6, 1, NULL, '2016-07-22 02:16:43', '2016-07-22 02:16:43', NULL),
(43, 'Packer', '4', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:17:12', '2016-11-18 09:07:23', NULL),
(46, 'G. P. Q', '2', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:21:29', '2016-11-18 09:09:47', NULL),
(48, 'Jr. Iron Man', '5', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:22:18', '2016-11-18 09:10:56', NULL),
(49, 'Iron Man', '4', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:22:43', '2016-11-18 07:23:27', NULL),
(50, 'Finishing Asst', '7', 20, 1, 1, 1, 3, 7, 12, 6, 1, 2, '2016-07-22 02:23:09', '2016-11-18 07:23:58', NULL),
(53, 'In Charge', '0', 10, 1, 1, 1, 4, 6, 11, 7, 1, 1, '2016-07-22 02:24:58', '2016-07-22 02:32:42', NULL),
(54, 'Electrician', '1', 20, 1, 1, 1, 4, 6, 11, 7, 1, 2, '2016-07-22 02:33:11', '2016-11-18 09:14:24', NULL),
(55, 'Jr. Electrician', '0', 20, 1, 1, 1, 4, 6, 11, 7, 1, NULL, '2016-07-22 02:33:41', '2016-07-22 02:33:41', NULL),
(56, 'Manager', '1', 2, 1, 1, 1, 4, 11, 21, 7, 1, 2, '2016-07-22 02:34:56', '2016-11-18 09:39:44', NULL),
(57, 'Machines', '0', 20, 1, 1, 1, 4, 11, 21, 7, 1, NULL, '2016-07-22 02:35:27', '2016-07-22 02:35:27', NULL),
(58, 'Machine In Charge', '1', 20, 1, 1, 1, 4, 11, 21, 7, 1, 2, '2016-07-22 02:35:54', '2016-11-18 07:59:12', NULL),
(59, 'Jr. Mechanic', '1', 10, 1, 1, 1, 4, 11, 21, 7, 1, 2, '2016-07-22 02:36:22', '2016-11-18 09:43:07', NULL),
(60, 'Merchandiser Manager', '1', 10, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:37:07', '2016-11-18 09:43:53', NULL),
(61, 'Sr. Merchandiser', '1', 10, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:37:36', '2016-11-18 09:44:57', NULL),
(62, 'Merchandiser', '2', 20, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:38:07', '2016-11-18 09:45:49', NULL),
(63, 'Jr. Merchandiser', '2', 20, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:38:30', '2016-11-18 09:46:47', NULL),
(64, 'Asst. Manager', '2', 20, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:38:53', '2016-11-18 09:47:31', NULL),
(65, 'Store In Charge', '1', 20, 1, 1, 1, 6, 17, 27, 9, 1, 2, '2016-07-22 02:39:37', '2016-11-18 09:48:20', NULL),
(66, 'Store Keeper', '1', 20, 1, 1, 1, 6, 17, 27, 9, 1, 2, '2016-07-22 02:40:08', '2016-11-18 09:53:28', NULL),
(67, 'Asst. Store Keeper', '7', 20, 1, 1, 1, 6, 17, 27, 9, 1, NULL, '2016-07-22 02:40:31', '2016-07-22 02:40:31', NULL),
(68, 'Wash Technician', '7', 20, 1, 1, 1, 7, 18, 28, 10, 1, NULL, '2016-07-22 02:41:04', '2016-07-22 02:41:04', NULL),
(69, 'Security In Charge', '0', 20, 1, 1, 1, 1, 9, 14, 4, 1, NULL, '2016-07-22 02:41:59', '2016-07-22 02:41:59', NULL),
(70, 'Quality Inspector', '4', 20, 1, 1, 1, 8, 14, 25, 11, 1, 2, '2016-07-22 02:42:41', '2016-11-18 09:58:51', NULL),
(71, 'Sr. Quality Inspector', '3', 20, 1, 1, 1, 8, 14, 25, 11, 1, 2, '2016-07-22 02:43:07', '2016-11-18 09:59:54', NULL),
(72, 'Quality Supervisor', '2', 20, 1, 1, 1, 8, 14, 25, 11, 1, 2, '2016-07-22 02:44:31', '2016-11-18 08:04:17', NULL),
(73, 'Quality Controller', '2', 20, 1, 1, 1, 8, 14, 25, 11, 1, 2, '2016-07-22 02:44:56', '2016-11-18 10:08:30', NULL),
(76, 'Asst. Merchandiser', '2', 5, 1, 1, 1, 5, 12, 22, 8, 1, 2, '2016-07-22 02:47:03', '2016-11-18 10:10:05', NULL),
(78, 'Operation Manager', '0', 20, 1, 1, 1, 11, 3, 30, 14, 1, 2, '2016-07-22 02:47:57', '2016-11-18 08:05:46', NULL),
(79, 'Manager Account', '1', 12, 1, 1, 1, 10, 1, 7, 13, 1, 2, '2016-07-22 02:48:24', '2016-11-18 10:11:37', NULL),
(80, 'Officer Account', '2', 10, 1, 1, 1, 10, 1, 7, 13, 1, 2, '2016-07-22 02:48:48', '2016-11-18 10:12:30', NULL),
(81, 'Checker', '0', 2, 1, 1, 1, 11, 3, 8, 14, 1, 2, '2016-07-22 02:49:26', '2016-11-18 08:07:01', NULL),
(82, 'Cleaner', '0', 10, 1, 1, 1, 1, 2, 30, 14, 1, 2, '2016-07-22 02:57:43', '2016-11-18 08:07:11', NULL),
(83, 'Sweeper', '0', 10, 1, 1, 1, 1, 2, 30, 14, 1, 2, '2016-07-22 02:58:05', '2016-11-18 08:07:48', NULL),
(84, 'Security Guard', '3', 10, 1, 1, 1, 1, 2, 30, 14, 1, 2, '2016-07-22 02:58:28', '2016-11-18 10:17:32', NULL),
(85, 'Security In Charge', '2', 10, 1, 1, 1, 1, 2, 30, 14, 1, 2, '2016-07-22 02:58:51', '2016-11-18 10:18:16', NULL),
(86, 'Driver', '3', 10, 1, 1, 1, 1, 2, 30, 14, 1, 2, '2016-07-22 02:59:13', '2016-11-18 10:19:03', NULL),
(87, 'Quality Manager', '0', 5, 1, 1, 1, 8, 14, 25, 11, 1, NULL, '2016-07-22 02:59:41', '2016-07-22 02:59:41', NULL),
(88, 'Security In Charge', '0', 10, 1, 1, 1, 11, 3, 8, 14, 1, 2, '2016-07-22 03:00:06', '2016-11-18 08:08:48', NULL),
(89, 'Sample Man', '0', 11, 1, 1, 1, 9, 15, 26, 12, 1, NULL, '2016-07-22 03:00:39', '2016-07-22 03:00:39', NULL),
(90, 'Supervisor', '0', 20, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-22 03:01:50', '2016-07-22 03:01:50', NULL),
(91, 'Sample Q. C', '0', 20, 1, 1, 1, 9, 15, 26, 12, 1, NULL, '2016-07-22 03:04:20', '2016-07-22 03:04:20', NULL),
(92, 'Maintenance In Charge', '0', 20, 1, 1, 1, 4, 11, 21, 7, 1, 2, '2016-07-22 03:05:00', '2016-11-18 08:10:33', NULL),
(93, 'Mechanic', '0', 20, 1, 1, 1, 4, 11, 21, 7, 1, NULL, '2016-07-22 03:05:28', '2016-07-22 03:05:28', NULL),
(94, 'Febric Q. C', '0', 20, 1, 1, 1, 6, 17, 27, 9, 1, NULL, '2016-07-22 03:05:51', '2016-07-22 03:05:51', NULL),
(95, 'Patten Master', '0', 20, 1, 1, 1, 9, 15, 26, 12, 1, NULL, '2016-07-22 03:06:22', '2016-07-22 03:06:22', NULL),
(96, 'Patten Asst.', '0', 20, 1, 1, 1, 9, 15, 26, 12, 1, 2, '2016-07-22 03:07:05', '2016-11-18 08:12:46', NULL),
(97, 'Floor In Charge', '0', 20, 1, 1, 1, 3, 19, 24, 6, 1, NULL, '2016-07-22 03:07:41', '2016-07-22 03:07:41', NULL),
(98, 'Electric In Charge', '0', 20, 1, 1, 1, 4, 6, 11, 7, 1, NULL, '2016-07-22 03:08:10', '2016-07-22 03:08:10', NULL),
(99, 'Cad Marker Man', '0', 20, 1, 1, 1, 9, 15, 26, 12, 1, NULL, '2016-07-22 03:08:42', '2016-07-22 03:08:42', NULL),
(100, 'Sample Asst.', '0', 20, 1, 1, 1, 9, 15, 26, 12, 1, NULL, '2016-07-22 03:09:08', '2016-07-22 03:09:08', NULL),
(101, 'Line Chief', '0', 20, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-22 03:09:51', '2016-07-22 03:09:51', NULL),
(102, 'Line Iron Man', '7', 20, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-22 03:10:42', '2016-07-22 03:10:42', NULL),
(104, 'Electric Asst.', '0', 20, 1, 1, 1, 4, 6, 11, 7, 1, NULL, '2016-07-22 03:11:51', '2016-07-22 03:11:51', NULL),
(105, 'Asst. Mechanic', '0', 20, 1, 1, 1, 4, 11, 21, 7, 1, NULL, '2016-07-22 03:12:29', '2016-07-22 03:12:29', NULL),
(106, 'Line Chief', '0', 20, 1, 1, 1, 3, 16, 1, 6, 1, NULL, '2016-07-22 03:13:24', '2016-07-22 03:13:24', NULL),
(107, 'Jr. Marker', '0', 20, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-22 03:14:11', '2016-07-22 03:14:11', NULL),
(108, 'JSMO', '5', 100, 1, 1, 1, 3, 5, 10, 6, 1, NULL, '2016-07-22 03:15:09', '2016-07-22 03:15:09', NULL),
(109, 'Store Officer', '0', 20, 1, 1, 1, 6, 17, 27, 6, 1, NULL, '2016-07-22 03:17:42', '2016-07-22 03:17:42', NULL),
(110, 'SR.Q.I', '3', 5, 1, 1, 1, 8, 14, 25, 11, 2, NULL, '2016-11-18 07:03:59', '2016-11-18 07:03:59', NULL),
(111, 'Sr. Cutter', '3', 10, 1, 1, 1, 3, 5, 10, 6, 2, NULL, '2016-11-18 07:15:23', '2016-11-18 07:15:23', NULL),
(112, 'Sr.Marker', '3', 10, 1, 1, 1, 3, 5, 10, 6, 2, NULL, '2016-11-18 07:16:05', '2016-11-18 07:16:05', NULL),
(113, 'Cutter', '4', 20, 1, 1, 1, 3, 5, 10, 6, 2, NULL, '2016-11-18 07:17:29', '2016-11-18 07:17:29', NULL),
(114, 'Jr.Folder', '5', 20, 1, 1, 1, 3, 7, 12, 6, 2, NULL, '2016-11-18 07:27:00', '2016-11-18 07:27:00', NULL),
(115, 'Packering Man', '4', 20, 1, 1, 1, 3, 7, 12, 6, 2, NULL, '2016-11-18 07:38:19', '2016-11-18 07:38:19', NULL),
(116, 'Poly Man', '4', 20, 1, 1, 1, 3, 7, 12, 6, 2, NULL, '2016-11-18 07:40:54', '2016-11-18 07:40:54', NULL),
(117, 'Compliance', '2', 2, 1, 1, 1, 13, 20, 31, 16, 2, 2, '2016-11-18 07:54:21', '2016-11-18 07:55:32', NULL),
(118, 'Store Officer', '1', 20, 1, 1, 1, 6, 17, 27, 9, 2, NULL, '2016-11-18 09:50:57', '2016-11-18 09:50:57', NULL),
(119, 'GM Production', '1', 2, 1, 1, 1, 3, 21, 32, 17, 2, NULL, '2016-11-18 10:05:46', '2016-11-18 10:05:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `designation_groups`
--

CREATE TABLE IF NOT EXISTS `designation_groups` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `designation_groups`
--

INSERT INTO `designation_groups` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(4, 'HR & Admin', 1, 2, '2016-07-21 08:05:46', '2016-11-18 08:03:20', NULL),
(5, 'Commercial', 1, NULL, '2016-07-21 08:05:51', '2016-07-21 08:05:51', NULL),
(6, 'Production', 1, NULL, '2016-07-21 08:05:57', '2016-07-21 08:05:57', NULL),
(7, 'Maintenance', 1, NULL, '2016-07-21 08:06:01', '2016-07-21 08:06:01', NULL),
(8, 'Merchandising', 1, NULL, '2016-07-21 08:06:06', '2016-07-21 08:06:06', NULL),
(9, 'Store', 1, NULL, '2016-07-21 08:06:17', '2016-07-21 08:06:17', NULL),
(10, 'Washing', 1, NULL, '2016-07-21 08:06:22', '2016-07-21 08:06:22', NULL),
(11, 'Quality', 1, NULL, '2016-07-21 08:06:28', '2016-07-21 08:06:28', NULL),
(12, 'Sample', 1, NULL, '2016-07-21 08:06:34', '2016-07-21 08:06:34', NULL),
(13, 'Account / Finance', 1, NULL, '2016-07-21 08:06:40', '2016-07-21 08:06:40', NULL),
(14, 'Admin', 1, NULL, '2016-07-21 08:06:46', '2016-07-21 08:06:46', NULL),
(15, 'General Staff', 1, NULL, '2016-07-21 08:06:52', '2016-07-21 08:06:52', NULL),
(16, 'Compliance', 2, NULL, '2016-11-18 07:53:57', '2016-11-18 07:53:57', NULL),
(17, 'GM Production', 2, NULL, '2016-11-18 10:05:21', '2016-11-18 10:05:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `divisions`
--

CREATE TABLE IF NOT EXISTS `divisions` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `divisions`
--

INSERT INTO `divisions` (`id`, `name`, `address`, `organization_id`, `unit_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'DK Global Fashion', '', 1, 1, 1, 1, '2016-07-21 07:11:57', '2016-07-21 07:13:18', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `earn_leave_opening_balance`
--

CREATE TABLE IF NOT EXISTS `earn_leave_opening_balance` (
`id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `posting_date` date DEFAULT NULL,
  `balance_days` int(11) NOT NULL,
  `payment_days` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE IF NOT EXISTS `employees` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `card_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `old_card_no` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `line_id` int(10) unsigned NOT NULL,
  `designation_group_id` int(10) unsigned NOT NULL,
  `designation_id` int(10) unsigned NOT NULL,
  `employee_category_id` int(10) unsigned NOT NULL,
  `employee_type_id` int(10) unsigned NOT NULL,
  `holiday_id` int(10) unsigned NOT NULL,
  `probation_period_id` int(10) unsigned NOT NULL,
  `sex` enum('Male','Female','Others') COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `attendance_bonus` enum('Yes','No') COLLATE utf8_unicode_ci DEFAULT NULL,
  `ot_status` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `incentive` int(11) NOT NULL,
  `others` int(11) NOT NULL,
  `car_bill` int(11) NOT NULL,
  `special_bill` int(11) NOT NULL,
  `mobile_bill` int(11) NOT NULL,
  `conveyance` int(11) NOT NULL DEFAULT '200',
  `food_all` int(11) NOT NULL DEFAULT '650',
  `medical` int(11) NOT NULL DEFAULT '250',
  `hra` int(11) NOT NULL,
  `basic_salary` int(11) NOT NULL,
  `gross_salary` int(11) NOT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_holder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `branch_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `payment_mode` enum('Cash','Bank') COLLATE utf8_unicode_ci NOT NULL,
  `confirm_date` date NOT NULL,
  `date_of_separation` date NOT NULL,
  `reason_of_separation` text COLLATE utf8_unicode_ci NOT NULL,
  `increment_month` int(11) NOT NULL,
  `last_increment_date` date NOT NULL,
  `increment_date` date NOT NULL,
  `current_salary` double(8,2) NOT NULL,
  `joining_salary` double(8,2) NOT NULL,
  `date_of_joining` date NOT NULL,
  `last_organization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorized_percent_for_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `relation_with_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_of_nominees` text COLLATE utf8_unicode_ci NOT NULL,
  `name_of_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `number_of_children` int(11) NOT NULL,
  `marital_status` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `religion` enum('Islam','Judaism','Christianity','Hinduism','Buddhism','Others') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Islam',
  `date_of_birth` date NOT NULL,
  `nationality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `national_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `emergency_contact_no` int(11) NOT NULL,
  `contact_no` int(11) NOT NULL,
  `bangla_present_address` text COLLATE utf8_unicode_ci NOT NULL,
  `present_address` text COLLATE utf8_unicode_ci NOT NULL,
  `bangla_permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `husband_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mothers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fathers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_categories`
--

CREATE TABLE IF NOT EXISTS `employee_categories` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_categories`
--

INSERT INTO `employee_categories` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Regular', 1, NULL, '2016-07-23 08:29:32', '2016-07-23 08:29:32', NULL),
(2, 'Resign', 2, NULL, '2016-09-27 03:03:50', '2016-09-27 03:03:50', NULL),
(3, 'Lefty', 2, NULL, '2016-09-27 03:03:58', '2016-09-27 03:03:58', NULL),
(4, 'New', 2, NULL, '2016-09-27 03:04:05', '2016-09-27 03:04:05', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_leave_assigns`
--

CREATE TABLE IF NOT EXISTS `employee_leave_assigns` (
`id` int(10) unsigned NOT NULL,
  `year` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `leave_type_id` int(10) unsigned NOT NULL,
  `leave_days` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_org_profiles`
--

CREATE TABLE IF NOT EXISTS `employee_org_profiles` (
`id` int(10) unsigned NOT NULL,
  `name_of_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_of_nominees` text COLLATE utf8_unicode_ci NOT NULL,
  `relation_with_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `authorized_percent_for_nominees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_organization` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_joining` date NOT NULL,
  `joining_salary` double(8,2) NOT NULL,
  `current_salary` double(8,2) NOT NULL,
  `increment_month` date NOT NULL,
  `reason_of_separation` text COLLATE utf8_unicode_ci NOT NULL,
  `date_of_separation` date NOT NULL,
  `confirm_date` date NOT NULL,
  `payment_mode` enum('Cash','Bank') COLLATE utf8_unicode_ci NOT NULL,
  `bank_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `branch_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_holder` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `account_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_persona_infos`
--

CREATE TABLE IF NOT EXISTS `employee_persona_infos` (
`id` int(10) unsigned NOT NULL,
  `fathers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mothers_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `husband_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `bangla_permanent_address` text COLLATE utf8_unicode_ci NOT NULL,
  `present_address` text COLLATE utf8_unicode_ci NOT NULL,
  `bangla_present_address` text COLLATE utf8_unicode_ci NOT NULL,
  `contact_no` int(11) NOT NULL,
  `emergency_contact_no` int(11) NOT NULL,
  `national_id` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nationality` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `religion` enum('Islam','Judaism','Christianity','Hinduism','Buddhism','Others') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Islam',
  `marital_status` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `number_of_children` int(11) NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `employee_types`
--

CREATE TABLE IF NOT EXISTS `employee_types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `employee_types`
--

INSERT INTO `employee_types` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Staff', 1, NULL, '2016-07-21 08:13:02', '2016-07-21 08:13:02', NULL),
(2, 'worker', 1, NULL, '2016-07-21 08:13:10', '2016-07-21 08:13:10', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holidays`
--

CREATE TABLE IF NOT EXISTS `holidays` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `year` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `holidays`
--

INSERT INTO `holidays` (`id`, `name`, `year`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'For All Employee', 2016, 1, NULL, '2016-07-23 08:30:02', '2016-07-23 08:30:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holiday_children`
--

CREATE TABLE IF NOT EXISTS `holiday_children` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `holiday_type_id` int(10) unsigned NOT NULL,
  `holiday_id` int(10) unsigned NOT NULL,
  `date` date NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `holiday_children`
--

INSERT INTO `holiday_children` (`id`, `name`, `holiday_type_id`, `holiday_id`, `date`, `details`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'weekly', 1, 1, '2016-01-01', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(2, 'weekly', 1, 1, '2016-01-08', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(3, 'weekly', 1, 1, '2016-01-15', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(4, 'weekly', 1, 1, '2016-01-22', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(5, 'weekly', 1, 1, '2016-01-29', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(6, 'weekly', 1, 1, '2016-02-05', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(7, 'weekly', 1, 1, '2016-02-12', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(8, 'weekly', 1, 1, '2016-02-19', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(9, 'weekly', 1, 1, '2016-02-26', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(10, 'weekly', 1, 1, '2016-03-04', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(11, 'weekly', 1, 1, '2016-03-11', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(12, 'weekly', 1, 1, '2016-03-18', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(13, 'weekly', 1, 1, '2016-03-25', '', 1, NULL, '2016-07-23 08:30:44', '2016-07-23 08:30:44', NULL),
(14, 'weekly', 1, 1, '2016-04-01', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(15, 'weekly', 1, 1, '2016-04-08', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(16, 'weekly', 1, 1, '2016-04-15', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(17, 'weekly', 1, 1, '2016-04-22', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(18, 'weekly', 1, 1, '2016-04-29', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(19, 'weekly', 1, 1, '2016-05-06', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(20, 'weekly', 1, 1, '2016-05-13', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(21, 'weekly', 1, 1, '2016-05-20', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(22, 'weekly', 1, 1, '2016-05-27', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(23, 'weekly', 1, 1, '2016-06-03', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(24, 'weekly', 1, 1, '2016-06-10', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(25, 'weekly', 1, 1, '2016-06-17', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(26, 'weekly', 1, 1, '2016-06-24', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(27, 'weekly', 1, 1, '2016-07-01', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(28, 'weekly', 1, 1, '2016-07-08', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(29, 'weekly', 1, 1, '2016-07-15', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(30, 'weekly', 1, 1, '2016-07-22', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(31, 'weekly', 1, 1, '2016-07-29', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(32, 'weekly', 1, 1, '2016-08-05', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(33, 'weekly', 1, 1, '2016-08-12', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(34, 'weekly', 1, 1, '2016-08-19', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(35, 'weekly', 1, 1, '2016-08-26', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(36, 'weekly', 1, 1, '2016-09-02', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(37, 'weekly', 1, 1, '2016-09-09', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(38, 'weekly', 1, 1, '2016-09-16', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(39, 'weekly', 1, 1, '2016-09-23', '', 1, NULL, '2016-07-23 08:30:45', '2016-07-23 08:30:45', NULL),
(40, 'weekly', 1, 1, '2016-09-30', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(41, 'weekly', 1, 1, '2016-10-07', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(42, 'weekly', 1, 1, '2016-10-14', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(43, 'weekly', 1, 1, '2016-10-21', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(44, 'weekly', 1, 1, '2016-10-28', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(45, 'weekly', 1, 1, '2016-11-04', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(46, 'weekly', 1, 1, '2016-11-11', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(47, 'weekly', 1, 1, '2016-11-18', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(48, 'weekly', 1, 1, '2016-11-25', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(49, 'weekly', 1, 1, '2016-12-02', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(50, 'weekly', 1, 1, '2016-12-09', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(51, 'weekly', 1, 1, '2016-12-16', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL),
(52, 'weekly', 1, 1, '2016-12-23', '', 1, NULL, '2016-07-23 08:30:46', '2016-07-23 08:30:46', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `holiday_types`
--

CREATE TABLE IF NOT EXISTS `holiday_types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `holiday_types`
--

INSERT INTO `holiday_types` (`id`, `name`, `details`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'For All Employee', '', 1, NULL, '2016-07-23 08:30:27', '2016-07-23 08:30:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `import_errors`
--

CREATE TABLE IF NOT EXISTS `import_errors` (
  `id` int(10) unsigned NOT NULL,
  `import_for` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Pending','On Hold','Fixed') COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_details`
--

CREATE TABLE IF NOT EXISTS `leave_details` (
`id` int(10) unsigned NOT NULL,
  `leave_register_id` int(10) unsigned NOT NULL,
  `leave_type_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `days` double(8,2) NOT NULL,
  `payable` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_registers`
--

CREATE TABLE IF NOT EXISTS `leave_registers` (
`id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `p_days` double(8,2) NOT NULL,
  `p_end_date` date NOT NULL,
  `p_start_date` date NOT NULL,
  `status` enum('Pending','Approved','Rejected') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Pending',
  `is_permanent_save` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'No',
  `leave_approval` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `leave_types`
--

CREATE TABLE IF NOT EXISTS `leave_types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `details` text COLLATE utf8_unicode_ci NOT NULL,
  `is_earn` int(11) DEFAULT NULL,
  `is_payable` int(11) DEFAULT NULL,
  `is_maternity` int(11) DEFAULT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `leave_types`
--

INSERT INTO `leave_types` (`id`, `name`, `details`, `is_earn`, `is_payable`, `is_maternity`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Ml', '', NULL, NULL, 1, 2, NULL, '2016-11-18 09:20:11', '2016-11-18 09:20:11', NULL),
(2, 'CL', '', NULL, 1, NULL, 2, NULL, '2016-11-18 09:20:42', '2016-11-18 09:20:42', NULL),
(3, 'SL', '', NULL, NULL, NULL, 2, NULL, '2016-11-18 09:21:54', '2016-11-18 09:21:54', NULL),
(4, 'EL', '', 1, NULL, NULL, 2, NULL, '2016-11-18 09:22:15', '2016-11-18 09:22:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lines`
--

CREATE TABLE IF NOT EXISTS `lines` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `lines`
--

INSERT INTO `lines` (`id`, `name`, `description`, `organization_id`, `unit_id`, `division_id`, `department_id`, `section_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'A - 1', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:39:26', '2016-07-21 07:39:26', NULL),
(2, 'A - 2', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:39:48', '2016-07-21 07:39:48', NULL),
(3, 'A - 3', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:40:10', '2016-07-21 07:40:10', NULL),
(4, 'A - 4', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:40:24', '2016-07-21 07:40:24', NULL),
(5, 'A - 5', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:40:39', '2016-07-21 07:40:39', NULL),
(6, 'A - 6', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 07:40:55', '2016-07-21 07:40:55', NULL),
(7, 'Account', '', 1, 1, 1, 10, 1, 1, NULL, '2016-07-21 07:41:12', '2016-07-21 07:41:12', NULL),
(8, 'Admin', '', 1, 1, 1, 11, 3, 1, NULL, '2016-07-21 07:41:26', '2016-07-21 07:41:26', NULL),
(9, 'Commercial', '', 1, 1, 1, 2, 4, 1, NULL, '2016-07-21 07:41:58', '2016-07-21 07:41:58', NULL),
(10, 'Cutting', '', 1, 1, 1, 3, 5, 1, NULL, '2016-07-21 07:42:20', '2016-07-21 07:42:20', NULL),
(11, 'Electric', '', 1, 1, 1, 4, 6, 1, NULL, '2016-07-21 07:42:38', '2016-07-21 07:42:38', NULL),
(12, 'Finishing', '', 1, 1, 1, 3, 7, 1, NULL, '2016-07-21 07:42:55', '2016-07-21 07:42:55', NULL),
(13, 'General Staff', '', 1, 1, 1, 12, 8, 1, NULL, '2016-07-21 07:43:13', '2016-07-21 07:43:13', NULL),
(14, 'HR & Compliance 	', '', 1, 1, 1, 1, 9, 1, NULL, '2016-07-21 07:43:29', '2016-07-21 07:43:29', NULL),
(21, 'Mechanic', '', 1, 1, 1, 4, 11, 1, NULL, '2016-07-21 07:46:45', '2016-07-21 07:46:45', NULL),
(22, 'Merchandising', '', 1, 1, 1, 5, 12, 1, NULL, '2016-07-21 07:47:01', '2016-07-21 07:47:01', NULL),
(23, 'Office Staff', '', 1, 1, 1, 1, 9, 1, NULL, '2016-07-21 07:48:46', '2016-07-21 07:48:46', NULL),
(24, 'Production Staff', '', 1, 1, 1, 3, 19, 1, NULL, '2016-07-21 07:50:35', '2016-07-21 07:50:35', NULL),
(25, 'Quality', '', 1, 1, 1, 8, 14, 1, NULL, '2016-07-21 07:51:03', '2016-07-21 07:51:03', NULL),
(26, 'Sample', '', 1, 1, 1, 9, 15, 1, NULL, '2016-07-21 07:51:23', '2016-07-21 07:51:23', NULL),
(27, 'Store', '', 1, 1, 1, 6, 17, 1, NULL, '2016-07-21 07:51:49', '2016-07-21 07:51:49', NULL),
(28, 'Washing', '', 1, 1, 1, 7, 18, 1, NULL, '2016-07-21 07:52:05', '2016-07-21 07:52:05', NULL),
(29, '0', '', 1, 1, 1, 3, 16, 1, NULL, '2016-07-21 21:07:08', '2016-07-21 21:07:08', NULL),
(30, 'Office Staff', '', 1, 1, 1, 1, 2, 1, 1, '2016-07-21 21:25:11', '2016-07-21 21:25:53', NULL),
(31, 'Compliance', '', 1, 1, 1, 13, 20, 2, NULL, '2016-11-18 07:53:51', '2016-11-18 07:53:51', NULL),
(32, 'GM Production', '', 1, 1, 1, 3, 21, 2, NULL, '2016-11-18 10:05:16', '2016-11-18 10:05:16', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE IF NOT EXISTS `menus` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `url` text COLLATE utf8_unicode_ci NOT NULL,
  `order_id` int(11) NOT NULL,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `slug`, `icon`, `url`, `order_id`, `parent_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dashboard', '', 'fa fa-tachometer', '', 0, NULL, '2016-04-07 04:45:55', '2016-04-07 04:46:29', NULL),
(2, 'Menu Setting', '', 'fa fa-bars', '', 1, NULL, '2016-04-07 04:47:18', '2016-04-07 04:53:57', NULL),
(3, 'Create Menu', '', '', '/menu/create', 0, 2, '2016-04-07 04:47:35', '2016-04-07 04:48:25', NULL),
(4, 'Manage Menu', '', '', '/menu', 1, 2, '2016-04-07 04:49:22', '2016-04-07 04:50:06', NULL),
(5, 'Company Settings', '', 'fa fa-cog fa-fw', '', 1, NULL, '2016-04-07 04:53:09', '2016-04-07 05:52:08', NULL),
(6, 'Organization', '', '', '', 0, 5, '2016-04-07 04:55:30', '2016-04-07 04:55:30', NULL),
(7, 'Create Organization', '', '', '/organization/create', 0, 6, '2016-04-07 04:55:59', '2016-04-07 04:55:59', NULL),
(8, 'Manage Organization', '', '', '/organization', 0, 6, '2016-04-07 04:56:43', '2016-04-07 04:56:43', NULL),
(9, 'Unit', '', '', '', 0, 5, '2016-04-07 05:01:59', '2016-04-07 05:01:59', NULL),
(10, 'Create Unit', '', '', '/unit/create', 0, 9, '2016-04-07 05:02:39', '2016-04-07 05:02:39', NULL),
(11, 'Manage Unit', '', '', '/unit', 0, 9, '2016-04-07 05:02:59', '2016-04-07 05:02:59', NULL),
(12, 'Division', '', '', '', 0, 5, '2016-04-07 05:05:48', '2016-04-07 05:05:48', NULL),
(13, 'Create Division', '', '', '/division/create', 0, 12, '2016-04-07 05:06:23', '2016-04-07 05:06:23', NULL),
(14, 'Manage Division', '', '', '/division', 0, 12, '2016-04-07 05:06:54', '2016-04-07 05:06:54', NULL),
(15, 'Department', '', '', '', 0, 5, '2016-04-07 05:09:29', '2016-04-07 05:09:29', NULL),
(16, 'Create Department', '', '', '/department/create', 0, 15, '2016-04-07 05:10:38', '2016-04-07 05:10:38', NULL),
(17, 'Manage Department', '', '', '/department', 0, 15, '2016-04-07 05:11:12', '2016-04-07 05:11:12', NULL),
(18, 'Section', '', '', '', 0, 5, '2016-04-07 05:12:38', '2016-04-07 05:12:38', NULL),
(19, 'Create Section', '', '', '/section/create', 0, 18, '2016-04-07 05:13:04', '2016-04-07 05:13:04', NULL),
(20, 'Manage Section', '', '', '/section', 0, 18, '2016-04-07 05:13:29', '2016-04-07 05:13:29', NULL),
(21, 'Line/Sub Section', '', '', '', 0, 5, '2016-04-07 05:16:17', '2016-04-07 05:16:17', NULL),
(22, 'Create Line ', '', '', '/line/create', 0, 21, '2016-04-07 05:16:48', '2016-04-07 05:17:28', NULL),
(23, 'Manage Line', '', '', '/line', 0, 21, '2016-04-07 05:18:42', '2016-04-07 05:19:18', NULL),
(24, 'Designation Group', '', '', '', 0, 5, '2016-04-07 05:20:07', '2016-04-07 05:20:07', NULL),
(25, 'Create Desig Group', '', '', '/designation-group/create', 0, 24, '2016-04-07 05:21:02', '2016-04-07 05:21:48', NULL),
(26, 'Manage Desig Group', '', '', '/designation-group', 0, 24, '2016-04-07 05:22:30', '2016-04-07 05:22:30', NULL),
(27, 'Designation', '', '', '', 0, 5, '2016-04-07 05:26:37', '2016-04-07 05:26:37', NULL),
(28, 'Create Designation', '', '', '/designation/create', 0, 27, '2016-04-07 05:27:39', '2016-04-07 05:27:39', NULL),
(29, 'Manage Designation', '', '', '/designation', 0, 27, '2016-04-07 05:28:51', '2016-04-07 05:28:51', NULL),
(30, 'Holiday Setting', '', 'fa fa-thumbs-up', '', 3, NULL, '2016-04-07 05:53:17', '2016-04-07 05:54:53', NULL),
(31, 'Holiday Type', '', '', '', 0, 30, '2016-04-07 06:13:48', '2016-04-07 06:13:48', NULL),
(32, 'Create Holiday Type', '', '', '/holiday-type/create', 0, 31, '2016-04-07 06:14:44', '2016-04-07 06:14:44', NULL),
(33, 'Manage Holiday Type', '', '', '/holiday-type', 0, 31, '2016-04-07 06:15:24', '2016-04-07 06:15:24', NULL),
(34, 'Assine Holiday', '', '', '', 0, 30, '2016-04-07 06:20:44', '2016-04-07 06:33:08', NULL),
(35, 'Create Holiday', '', '', '/holiday/create', 0, 34, '2016-04-07 06:33:40', '2016-04-07 06:33:40', NULL),
(36, 'Manage Holiday', '', '', '/holiday', 0, 34, '2016-04-07 06:34:02', '2016-04-07 06:34:19', NULL),
(37, 'Employee Setting', '', 'fa fa-users', '', 4, NULL, '2016-04-07 06:38:30', '2016-07-27 10:22:42', NULL),
(38, 'Employee Category', '', '', '', 0, 37, '2016-04-07 06:42:41', '2016-04-07 06:42:41', NULL),
(39, 'Create Employee Cat', '', '', '/employee-category/create', 0, 38, '2016-04-07 06:43:41', '2016-04-07 06:43:41', NULL),
(40, 'Manage Employee Cat', '', '', '/employee-category', 0, 38, '2016-04-07 06:45:35', '2016-04-07 06:45:35', NULL),
(41, 'Employee Type', '', '', '', 0, 37, '2016-04-07 06:49:32', '2016-04-07 06:49:32', NULL),
(42, 'Create Employee Type', '', '', '/employee-type/create', 0, 41, '2016-04-07 06:52:00', '2016-04-07 06:52:00', NULL),
(43, 'Manage Employee Type', '', '', '/employee-type', 0, 41, '2016-04-07 06:53:07', '2016-04-07 06:53:07', NULL),
(46, 'Probation Period', '', '', '', 0, 37, '2016-04-07 06:58:00', '2016-04-07 06:58:00', NULL),
(47, 'Create Probation Period', '', '', '/probation-period/create', 0, 46, '2016-04-07 06:59:36', '2016-04-07 06:59:52', NULL),
(48, 'Manage  Probation', '', '', '/probation-period', 0, 46, '2016-04-07 07:01:55', '2016-04-07 07:02:11', NULL),
(49, 'Employee', '', '', '', 0, 37, '2016-04-07 07:05:41', '2016-04-07 07:05:57', NULL),
(50, 'Create Employee', '', '', '/employee/create', 0, 49, '2016-04-07 07:06:38', '2016-04-07 07:06:38', NULL),
(51, 'Manage Employee ', '', '', '/employee', 0, 49, '2016-04-07 07:08:25', '2016-04-07 07:08:25', NULL),
(52, 'Attendance Setting', '', 'fa fa-signal', '', 5, NULL, '2016-04-07 07:10:42', '2016-04-07 07:10:42', NULL),
(53, 'Office Time Setting', '', '', '', 0, 52, '2016-04-07 07:11:11', '2016-04-07 07:11:11', NULL),
(54, 'Manage Setting', '', '', '/setting', 0, 53, '2016-04-07 07:13:07', '2016-04-07 07:18:48', NULL),
(55, 'Attendance', '', '', '/attendance/create', 0, 52, '2016-04-07 07:19:40', '2016-04-07 07:19:40', NULL),
(56, 'Create Attendance', '', '', '/attendance/create', 0, 55, '2016-04-07 07:20:17', '2016-04-07 07:20:33', NULL),
(57, 'Manage Attendance', '', '', '/attendance', 0, 55, '2016-04-07 07:25:20', '2016-04-07 07:25:20', NULL),
(58, 'Leave Setting', '', 'glyphicon glyphicon-user', '', 5, NULL, '2016-06-21 17:06:29', '2016-07-27 10:30:20', NULL),
(60, 'Leave Type', '', '', '', 0, 58, '2016-06-21 17:08:52', '2016-06-21 17:08:52', NULL),
(61, 'Add Leave Type', '', '', '/leave-type/create', 0, 60, '2016-06-21 17:09:12', '2016-06-21 17:09:12', NULL),
(62, 'Manage Leave Type', '', '', '/leave-type', 1, 60, '2016-06-21 17:09:47', '2016-06-21 17:09:47', NULL),
(63, 'Leave-Assign', '', '', '', 1, 58, '2016-06-21 17:18:40', '2016-06-21 17:18:40', NULL),
(64, 'Leave Assign', '', '', '/leave-assign/create', 0, 63, '2016-06-21 17:19:18', '2016-06-21 17:19:18', NULL),
(65, 'Manage Leave Assign', '', '', '/leave-assign/', 1, 63, '2016-06-21 17:21:01', '2016-06-21 17:21:01', NULL),
(66, 'Leave Register', '', '', '', 3, 58, '2016-06-21 17:22:08', '2016-06-21 17:22:08', NULL),
(67, 'Create Leave Register', '', '', '/leave-register/create', 0, 66, '2016-06-21 17:24:56', '2016-06-21 17:25:25', NULL),
(68, 'Manage Leave Register', '', '', '/leave-register/', 1, 66, '2016-06-21 17:26:45', '2016-06-21 17:26:45', NULL),
(69, 'Report', '', 'fa fa-line-chart', '', 8, NULL, '2016-06-21 17:30:53', '2016-07-27 11:11:08', NULL),
(70, 'Salary Report Setting', '', '', '', 0, 69, '2016-06-21 17:31:56', '2016-07-27 10:52:28', NULL),
(73, 'Multi-Attendance', '', '', '/multi-attendance', 3, 52, '2016-06-22 04:57:57', '2016-06-22 04:57:57', NULL),
(74, 'Multi-Leave-Assign', '', '', '/multiple-leave-assign', 3, 58, '2016-06-22 04:58:55', '2016-06-22 04:59:48', NULL),
(77, 'Salary Summery', '', '', '/report/salary-designation-wise/', 1, 70, '2016-07-27 10:49:14', '2016-07-27 10:49:14', NULL),
(78, 'Pay Slip', '', '', '/report/payslip/', 2, 70, '2016-07-27 10:53:20', '2016-07-27 10:54:04', NULL),
(79, 'Salary Sheet', '', '', '/report/salary/{comp?}', 3, 70, '2016-07-27 10:56:44', '2016-07-27 10:56:44', NULL),
(80, 'Extra Salary Report', '', '', '/report/salary/', 4, 70, '2016-07-27 10:58:35', '2016-07-27 10:59:29', NULL),
(81, 'Attendance Report', '', '', '', 1, 69, '2016-07-27 11:00:56', '2016-07-27 11:00:56', NULL),
(82, 'Attendance Summary', '', '', '/report/attendance-summary/', 0, 81, '2016-07-27 11:02:06', '2016-07-27 11:02:06', NULL),
(83, 'Daily-Attendance', '', '', '/report/daily-attendance/', 1, 81, '2016-07-27 11:03:07', '2016-07-27 11:03:07', NULL),
(84, 'Absent Report', '', '', '/report/absent-list/', 2, 81, '2016-07-27 11:04:13', '2016-07-27 11:04:13', NULL),
(85, 'Job Card Report', '', '', '', 2, 69, '2016-07-27 11:06:13', '2016-07-27 11:06:13', NULL),
(86, 'Job Card', '', '', '/report/job-card/', 0, 85, '2016-07-27 11:07:03', '2016-07-27 11:07:03', NULL),
(87, 'Salary Generation', '', 'fa fa-money', '', 7, NULL, '2016-07-27 11:11:15', '2016-07-27 11:11:36', NULL),
(88, 'Create Salary', '', '', '/salary/create', 0, 87, '2016-07-27 11:12:41', '2016-07-27 11:13:11', NULL),
(89, 'Manage Salary', '', '', '/salary', 1, 87, '2016-07-27 11:14:01', '2016-07-27 11:14:01', NULL),
(90, 'Upload', '', '', '/import-attendance', 3, 52, '2016-08-03 03:40:22', '2016-08-03 03:40:22', NULL),
(91, 'Salary-Setting', '', '', '', 8, 5, '2016-08-03 05:18:40', '2016-08-03 05:18:40', NULL),
(92, 'Create Setting', '', '', '/salary-setting/create', 0, 91, '2016-08-03 05:20:08', '2016-08-03 05:20:08', NULL),
(93, 'Manage salary setting', '', '', '/salary-setting', 1, 91, '2016-08-03 05:20:49', '2016-08-03 05:20:49', NULL),
(94, 'Error Log', '', '', '/error-log', 3, 69, '2016-08-31 01:45:41', '2016-08-31 01:45:41', NULL),
(95, 'Employee Report', '', '', '/report/employee', 4, 69, '2016-08-31 01:49:11', '2016-08-31 01:49:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE IF NOT EXISTS `organizations` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `address`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dk Global Fashion ware Ltd.', 'Corporate & Buying Office: Flat-b/4,House-2/c,Road-13\r\nNikunja-2,Dhaka-1229,Tel:880-2-8900829\r\n\r\nFacrory:\r\nMolla Super Market,\r\nBeron,Jamgpra,Ashulia,\r\nDhaka-1349,Bangladesh\r\n\r\nE-mail:kiron@dkglobalclothing.com\r\nKiron@dkglobalfashion.com\r\nwww.dkglobalfashion.com\r\n', 1, NULL, '2016-07-21 07:02:24', '2016-07-21 07:02:24', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE IF NOT EXISTS `permissions` (
`id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `probation_periods`
--

CREATE TABLE IF NOT EXISTS `probation_periods` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `probation_periods`
--

INSERT INTO `probation_periods` (`id`, `name`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, '8 Month', 1, NULL, '2016-07-21 08:13:43', '2016-07-21 08:13:43', NULL),
(2, '6 Month', 1, NULL, '2016-07-21 08:13:47', '2016-07-21 08:13:47', NULL),
(3, '3 Month', 1, NULL, '2016-07-21 08:13:53', '2016-07-21 08:13:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_permission`
--

CREATE TABLE IF NOT EXISTS `role_permission` (
`id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `permission_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salaries`
--

CREATE TABLE IF NOT EXISTS `salaries` (
`id` int(10) unsigned NOT NULL,
  `year` int(10) unsigned NOT NULL,
  `month` int(11) NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `gross_salary` int(11) NOT NULL DEFAULT '0',
  `basic_salary` int(11) NOT NULL DEFAULT '0',
  `hra` int(11) NOT NULL DEFAULT '0',
  `medical` int(11) NOT NULL DEFAULT '250',
  `food_all` int(11) NOT NULL DEFAULT '650',
  `conveyance` int(11) NOT NULL DEFAULT '200',
  `mobile_bill` int(11) NOT NULL DEFAULT '0',
  `special_bill` int(11) NOT NULL DEFAULT '0',
  `car_bill` int(11) NOT NULL DEFAULT '0',
  `others` int(11) NOT NULL DEFAULT '0',
  `incentive` int(11) NOT NULL DEFAULT '0',
  `attendance_bonus` int(11) NOT NULL DEFAULT '0',
  `ab_deduction` int(11) NOT NULL DEFAULT '0',
  `ot_rate` double NOT NULL DEFAULT '0',
  `ot_hours` int(11) NOT NULL DEFAULT '0',
  `ext_ot_hours` int(11) NOT NULL DEFAULT '0',
  `ot_amount` int(11) NOT NULL DEFAULT '0',
  `ext_ot_amount` int(11) NOT NULL DEFAULT '0',
  `holiday_all` int(11) NOT NULL DEFAULT '0',
  `arrear` int(11) NOT NULL DEFAULT '0',
  `fest_bonus` int(11) NOT NULL DEFAULT '0',
  `advance` int(11) NOT NULL DEFAULT '0',
  `others_deduction` int(11) NOT NULL DEFAULT '0',
  `tax` int(11) NOT NULL DEFAULT '0',
  `revenue_stamp` int(11) NOT NULL DEFAULT '10',
  `payable_amount` int(11) NOT NULL DEFAULT '0',
  `total_days` double NOT NULL,
  `p_days` double NOT NULL,
  `ab_days` double NOT NULL,
  `payable_days` double NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_settings`
--

CREATE TABLE IF NOT EXISTS `salary_settings` (
`id` int(10) unsigned NOT NULL,
  `designation_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `section_id` int(10) unsigned NOT NULL,
  `employee_type_id` int(10) unsigned NOT NULL,
  `holiday_bonus` int(11) NOT NULL DEFAULT '0',
  `probation_period_id` int(10) unsigned NOT NULL,
  `all_bonus` double NOT NULL,
  `grade` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `attendance_bonus` int(11) NOT NULL DEFAULT '0',
  `ot_status` enum('Yes','No') COLLATE utf8_unicode_ci NOT NULL,
  `max_others` int(11) NOT NULL,
  `min_others` int(11) NOT NULL,
  `max_car` int(11) NOT NULL,
  `min_car` int(11) NOT NULL,
  `max_special` int(11) NOT NULL,
  `min_special` int(11) NOT NULL,
  `max_conveyance` int(11) NOT NULL,
  `min_conveyance` int(11) NOT NULL,
  `max_mobile` int(11) NOT NULL,
  `min_mobile` int(11) NOT NULL,
  `max_food` int(11) NOT NULL,
  `min_food` int(11) NOT NULL,
  `max_medical` int(11) NOT NULL,
  `min_medical` int(11) NOT NULL,
  `max_hra` int(11) NOT NULL,
  `min_hra` int(11) NOT NULL,
  `max_basic` int(11) NOT NULL,
  `min_basic` int(11) NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=212 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `salary_settings`
--

INSERT INTO `salary_settings` (`id`, `designation_id`, `department_id`, `section_id`, `employee_type_id`, `holiday_bonus`, `probation_period_id`, `all_bonus`, `grade`, `attendance_bonus`, `ot_status`, `max_others`, `min_others`, `max_car`, `min_car`, `max_special`, `min_special`, `max_conveyance`, `min_conveyance`, `max_mobile`, `min_mobile`, `max_food`, `min_food`, `max_medical`, `min_medical`, `max_hra`, `min_hra`, `max_basic`, `min_basic`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(135, 114, 3, 7, 2, 0, 3, 0, '5', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6042, 6042, 2, NULL, '2016-11-18 07:29:19', '2016-11-18 07:29:19', NULL),
(136, 115, 3, 7, 2, 0, 3, 0, '4', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6520, 6420, 2, NULL, '2016-11-18 07:38:47', '2016-11-18 07:38:47', NULL),
(137, 116, 3, 7, 2, 0, 2, 0, '4', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6420, 6420, 2, NULL, '2016-11-18 07:42:21', '2016-11-18 07:42:21', NULL),
(138, 117, 13, 20, 1, 0, 2, 0, '1', 0, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 8000, 2, NULL, '2016-11-18 07:55:19', '2016-11-18 07:55:19', NULL),
(139, 1, 3, 16, 2, 0, 2, 0, '5', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 2000, 2, 2, '2016-11-18 08:16:49', '2016-11-18 10:31:27', NULL),
(140, 2, 3, 16, 2, 0, 2, 0, '4', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6804, 2000, 2, 2, '2016-11-18 08:18:03', '2016-11-18 10:31:45', NULL),
(141, 3, 3, 16, 2, 0, 2, 0, '4', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 10899, 2000, 2, 2, '2016-11-18 08:18:46', '2016-11-18 10:31:56', NULL),
(142, 4, 3, 16, 2, 0, 2, 0, '5', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 6042, 2, NULL, '2016-11-18 08:26:16', '2016-11-18 08:26:16', NULL),
(144, 5, 3, 16, 2, 0, 2, 0, '7', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 5677, 5300, 2, 2, '2016-11-18 08:29:14', '2016-11-18 08:42:22', NULL),
(145, 7, 3, 16, 2, 0, 2, 0, '7', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 5677, 5300, 2, NULL, '2016-11-18 08:30:07', '2016-11-18 08:30:07', NULL),
(146, 8, 3, 16, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 15000, 2, 2, '2016-11-18 08:30:39', '2016-11-18 08:42:09', NULL),
(147, 9, 3, 16, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 5000, 2, 2, '2016-11-18 08:41:14', '2016-11-18 08:42:02', NULL),
(148, 10, 3, 16, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:41:54', '2016-11-18 08:41:54', NULL),
(149, 14, 1, 2, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 5000, 2, 2, '2016-11-18 08:42:59', '2016-11-18 08:43:06', NULL),
(150, 15, 1, 9, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:43:49', '2016-11-18 08:43:49', NULL),
(151, 16, 1, 9, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:44:25', '2016-11-18 08:44:25', NULL),
(152, 17, 1, 9, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:44:41', '2016-11-18 08:44:41', NULL),
(153, 18, 1, 9, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:45:02', '2016-11-18 08:45:02', NULL),
(154, 19, 13, 20, 1, 0, 2, 0, '0', 0, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:45:22', '2016-11-18 08:45:22', NULL),
(155, 20, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:45:50', '2016-11-18 08:45:50', NULL),
(156, 21, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:46:07', '2016-11-18 08:46:07', NULL),
(157, 22, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:46:22', '2016-11-18 08:46:22', NULL),
(158, 23, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:47:47', '2016-11-18 08:47:47', NULL),
(159, 24, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:48:04', '2016-11-18 08:48:04', NULL),
(160, 25, 2, 4, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:48:26', '2016-11-18 08:48:26', NULL),
(161, 26, 3, 5, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:48:57', '2016-11-18 08:48:57', NULL),
(162, 27, 3, 5, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:49:18', '2016-11-18 08:49:18', NULL),
(163, 28, 3, 5, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:49:42', '2016-11-18 08:49:42', NULL),
(164, 29, 3, 5, 2, 0, 2, 0, '0', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 5677, 5300, 2, NULL, '2016-11-18 08:50:11', '2016-11-18 08:50:11', NULL),
(165, 32, 8, 14, 2, 0, 2, 0, '4', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6804, 6420, 2, NULL, '2016-11-18 08:51:19', '2016-11-18 08:51:19', NULL),
(166, 33, 3, 5, 2, 0, 2, 0, '0', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 6042, 2, NULL, '2016-11-18 08:52:49', '2016-11-18 08:52:49', NULL),
(167, 35, 3, 5, 2, 0, 2, 0, '0', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 6042, 2, NULL, '2016-11-18 08:55:33', '2016-11-18 08:55:33', NULL),
(168, 38, 3, 7, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 08:56:22', '2016-11-18 08:56:22', NULL),
(169, 40, 3, 7, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 30000, 10000, 2, NULL, '2016-11-18 08:57:17', '2016-11-18 08:57:17', NULL),
(170, 41, 3, 7, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 5000, 2, 2, '2016-11-18 08:57:54', '2016-11-18 08:58:50', NULL),
(171, 42, 3, 7, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 5000, 2, NULL, '2016-11-18 08:58:27', '2016-11-18 08:58:27', NULL),
(172, 114, 3, 7, 2, 0, 2, 0, '5', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 6042, 2, NULL, '2016-11-18 09:05:37', '2016-11-18 09:05:37', NULL),
(173, 43, 3, 7, 2, 0, 2, 0, '4', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6819, 6420, 2, NULL, '2016-11-18 09:09:03', '2016-11-18 09:09:03', NULL),
(174, 46, 3, 7, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:09:32', '2016-11-18 09:09:32', NULL),
(175, 48, 3, 7, 2, 0, 2, 0, '7', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6419, 6042, 2, NULL, '2016-11-18 09:10:36', '2016-11-18 09:10:36', NULL),
(176, 49, 3, 7, 2, 0, 2, 0, '4', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6804, 6420, 2, NULL, '2016-11-18 09:11:53', '2016-11-18 09:11:53', NULL),
(177, 50, 3, 7, 2, 0, 2, 0, '7', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 5667, 5300, 2, NULL, '2016-11-18 09:12:18', '2016-11-18 09:12:18', NULL),
(178, 53, 4, 6, 1, 0, 2, 0, '0', 0, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 09:12:55', '2016-11-18 09:12:55', NULL),
(179, 54, 4, 6, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:38:22', '2016-11-18 09:38:22', NULL),
(180, 55, 4, 6, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:38:57', '2016-11-18 09:38:57', NULL),
(181, 56, 4, 11, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:40:17', '2016-11-18 09:40:17', NULL),
(182, 57, 4, 11, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 09:40:38', '2016-11-18 09:40:38', NULL),
(183, 58, 4, 11, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:41:02', '2016-11-18 09:41:02', NULL),
(184, 59, 4, 11, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:43:25', '2016-11-18 09:43:25', NULL),
(185, 60, 5, 12, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:44:26', '2016-11-18 09:44:26', NULL),
(186, 61, 5, 12, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:45:19', '2016-11-18 09:45:19', NULL),
(187, 62, 5, 12, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:46:25', '2016-11-18 09:46:25', NULL),
(188, 63, 5, 12, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:47:03', '2016-11-18 09:47:03', NULL),
(189, 64, 5, 12, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:47:48', '2016-11-18 09:47:48', NULL),
(190, 65, 6, 17, 1, 0, 2, 0, '1', 0, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 10200, 2, NULL, '2016-11-18 09:48:54', '2016-11-18 09:48:54', NULL),
(191, 118, 6, 17, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 5000, 2, NULL, '2016-11-18 09:52:32', '2016-11-18 09:52:32', NULL),
(192, 66, 6, 17, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 10000, 2, NULL, '2016-11-18 09:53:48', '2016-11-18 09:53:48', NULL),
(193, 67, 6, 17, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 09:56:42', '2016-11-18 09:56:42', NULL),
(194, 68, 7, 18, 1, 0, 2, 0, '7', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 09:57:45', '2016-11-18 09:57:45', NULL),
(195, 69, 1, 9, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 09:58:02', '2016-11-18 09:58:02', NULL),
(196, 70, 8, 14, 2, 0, 2, 0, '4', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 6804, 6420, 2, 2, '2016-11-18 09:59:16', '2016-11-18 09:59:30', NULL),
(197, 72, 8, 14, 2, 0, 2, 0, '2', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 9899, 6805, 2, NULL, '2016-11-18 10:00:35', '2016-11-18 10:00:35', NULL),
(198, 119, 3, 21, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 100000, 3000, 2, NULL, '2016-11-18 10:06:08', '2016-11-18 10:06:08', NULL),
(199, 71, 8, 14, 2, 0, 2, 0, '3', 500, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 10899, 6805, 2, NULL, '2016-11-18 10:07:36', '2016-11-18 10:07:36', NULL),
(200, 73, 8, 14, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 10:08:55', '2016-11-18 10:08:55', NULL),
(201, 76, 5, 12, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 10:10:39', '2016-11-18 10:10:39', NULL),
(202, 78, 11, 3, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 10:11:13', '2016-11-18 10:11:13', NULL),
(203, 79, 10, 1, 1, 0, 2, 0, '1', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 100000, 3000, 2, NULL, '2016-11-18 10:11:55', '2016-11-18 10:11:55', NULL),
(204, 80, 10, 1, 1, 0, 2, 0, '2', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 50000, 3000, 2, NULL, '2016-11-18 10:13:26', '2016-11-18 10:13:26', NULL),
(205, 81, 11, 3, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 10:13:50', '2016-11-18 10:13:50', NULL),
(206, 82, 1, 2, 2, 0, 2, 0, '0', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 7339, 5650, 2, NULL, '2016-11-18 10:15:42', '2016-11-18 10:15:42', NULL),
(207, 83, 1, 2, 2, 0, 2, 0, '0', 150, 'Yes', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 7399, 5650, 2, NULL, '2016-11-18 10:16:18', '2016-11-18 10:16:18', NULL),
(208, 84, 1, 2, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 8099, 7400, 2, NULL, '2016-11-18 10:17:13', '2016-11-18 10:17:13', NULL),
(209, 85, 1, 2, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 3000, 2, NULL, '2016-11-18 10:17:55', '2016-11-18 10:17:55', NULL),
(210, 86, 1, 2, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 20000, 7400, 2, NULL, '2016-11-18 10:18:51', '2016-11-18 10:18:51', NULL),
(211, 87, 8, 14, 1, 0, 2, 0, '0', 0, 'No', 0, 0, 0, 0, 0, 0, 200, 200, 0, 0, 650, 650, 250, 250, 0, 0, 100000, 3000, 2, NULL, '2016-11-18 10:19:26', '2016-11-18 10:19:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `salary_setting_children`
--

CREATE TABLE IF NOT EXISTS `salary_setting_children` (
`id` int(10) unsigned NOT NULL,
  `salary_setting_id` int(10) unsigned NOT NULL,
  `salary_type_id` int(10) unsigned NOT NULL,
  `min_amount` double NOT NULL,
  `max_amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_structures`
--

CREATE TABLE IF NOT EXISTS `salary_structures` (
`id` int(10) unsigned NOT NULL,
  `employee_id` int(10) unsigned NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `total_earning` double NOT NULL,
  `total_deduction` double NOT NULL,
  `gross_total` double NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_structure_children`
--

CREATE TABLE IF NOT EXISTS `salary_structure_children` (
`id` int(10) unsigned NOT NULL,
  `salary_structure_id` int(10) unsigned NOT NULL,
  `salary_type_id` int(10) unsigned NOT NULL,
  `amount` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `salary_types`
--

CREATE TABLE IF NOT EXISTS `salary_types` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `earning` int(11) DEFAULT NULL,
  `deduction` int(11) DEFAULT NULL,
  `details` text COLLATE utf8_unicode_ci,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE IF NOT EXISTS `sections` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `unit_id` int(10) unsigned NOT NULL,
  `division_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `name`, `address`, `organization_id`, `unit_id`, `division_id`, `department_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Account', '', 1, 1, 1, 10, 1, 1, '2016-07-21 07:17:05', '2016-07-21 07:17:27', NULL),
(2, 'Admin', '', 1, 1, 1, 1, 1, 1, '2016-07-21 07:18:26', '2016-07-21 07:19:52', NULL),
(3, 'Admin', '', 1, 1, 1, 11, 1, NULL, '2016-07-21 07:18:48', '2016-07-21 07:18:48', NULL),
(4, 'Commercial', '', 1, 1, 1, 2, 1, NULL, '2016-07-21 07:20:33', '2016-07-21 07:20:33', NULL),
(5, 'Cutting', '', 1, 1, 1, 3, 1, NULL, '2016-07-21 07:20:47', '2016-07-21 07:20:47', NULL),
(6, 'Electric', '', 1, 1, 1, 4, 1, NULL, '2016-07-21 07:21:45', '2016-07-21 07:21:45', NULL),
(7, 'Finishing', '', 1, 1, 1, 3, 1, NULL, '2016-07-21 07:22:44', '2016-07-21 07:22:44', NULL),
(8, 'General Staff', '', 1, 1, 1, 12, 1, NULL, '2016-07-21 07:24:25', '2016-07-21 07:24:25', NULL),
(9, 'HR & Compliance', '', 1, 1, 1, 1, 1, NULL, '2016-07-21 07:25:28', '2016-07-21 07:25:28', NULL),
(11, 'Mechanic', '', 1, 1, 1, 4, 1, NULL, '2016-07-21 07:26:16', '2016-07-21 07:26:16', NULL),
(12, 'Merchandising', '', 1, 1, 1, 5, 1, NULL, '2016-07-21 07:27:18', '2016-07-21 07:27:18', NULL),
(14, 'Quality', '', 1, 1, 1, 8, 1, NULL, '2016-07-21 07:34:06', '2016-07-21 07:34:06', NULL),
(15, 'Sample', '', 1, 1, 1, 9, 1, NULL, '2016-07-21 07:34:26', '2016-07-21 07:34:26', NULL),
(16, 'Sewing', '', 1, 1, 1, 3, 1, 1, '2016-07-21 07:34:44', '2016-07-21 07:39:01', NULL),
(17, 'Store', '', 1, 1, 1, 6, 1, NULL, '2016-07-21 07:35:59', '2016-07-21 07:35:59', NULL),
(18, 'Wash', '', 1, 1, 1, 7, 1, NULL, '2016-07-21 07:36:22', '2016-07-21 07:36:22', NULL),
(19, 'Production Staff', '', 1, 1, 1, 3, 1, NULL, '2016-07-21 07:50:11', '2016-07-21 07:50:11', NULL),
(20, 'Compliance', '', 1, 1, 1, 13, 2, NULL, '2016-11-18 07:53:36', '2016-11-18 07:53:36', NULL),
(21, 'GM Production', '', 1, 1, 1, 3, 2, NULL, '2016-11-18 10:04:59', '2016-11-18 10:04:59', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
`id` int(10) unsigned NOT NULL,
  `property` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `property`, `value`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'attendance_buffer_time', '00:04:45', 1, 1, NULL, NULL, NULL),
(2, 'office_closing_time', '17:00:00', 1, 1, NULL, NULL, NULL),
(3, 'office_duration_time', '8', 1, 1, NULL, NULL, NULL),
(5, 'office_opening_time', '08:00:00', 1, 1, NULL, NULL, NULL),
(6, 'lunch_break', '13:00:00-14:00:00', 1, NULL, NULL, NULL, NULL),
(7, 'min_working_hours', '5', 1, NULL, NULL, NULL, NULL),
(8, 'employee_card_no_start_form', '100', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `units`
--

CREATE TABLE IF NOT EXISTS `units` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `created_by` int(10) unsigned NOT NULL,
  `updated_by` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `units`
--

INSERT INTO `units` (`id`, `name`, `address`, `organization_id`, `created_by`, `updated_by`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Dk Global Fashion ware Ltd.', 'Molla Super Market, Beron, Jamgora, Ashulia, Savar, Dhaka.', 1, 1, NULL, '2016-07-21 07:11:14', '2016-07-21 07:11:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
`id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('Active','Inactive') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Active',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'raju', 'raju@mail.com', '$2y$10$JNLTtp4QMb/30MmA.LzsAOMAp3wm0j62ZkjJJMQMsgMHWGc2nA7.2', 'Active', '1MUN6HnvP0kxGfkYBZ2hHS8cl8UlVUvw8029mQVCjHVbWA0UCaycMUKhdGFK', '2016-04-07 15:17:03', '2016-09-27 02:17:09', NULL),
(2, 'Biplob', 'rony.bg.bd@gmail.com', '$2y$10$JNLTtp4QMb/30MmA.LzsAOMAp3wm0j62ZkjJJMQMsgMHWGc2nA7.2', 'Active', 'nSeQ815NHM3eCNc7uzd02gu8N01WsJx3r0szDXosY7x4IezJvjgRdFzwDY7c', NULL, '2016-11-18 10:37:56', NULL),
(3, 'Alamin', 'alamin@gmail.com', '$2y$10$JNLTtp4QMb/30MmA.LzsAOMAp3wm0j62ZkjJJMQMsgMHWGc2nA7.2', 'Active', 'NgbbtMuqSNpgQs0W042wHTm86CTRhChuWaD0D9V4iB1K2ELsTE6WBn6JjCfE', NULL, '2016-07-26 21:59:53', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE IF NOT EXISTS `user_role` (
`id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `attendances`
--
ALTER TABLE `attendances`
 ADD PRIMARY KEY (`id`), ADD KEY `attendances_created_by_foreign` (`created_by`), ADD KEY `attendances_updated_by_foreign` (`updated_by`), ADD KEY `attendances_employee_id_date_index` (`employee_id`,`date`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
 ADD PRIMARY KEY (`id`), ADD KEY `departments_created_by_foreign` (`created_by`), ADD KEY `departments_updated_by_foreign` (`updated_by`), ADD KEY `departments_organization_id_foreign` (`organization_id`), ADD KEY `departments_unit_id_foreign` (`unit_id`), ADD KEY `departments_division_id_foreign` (`division_id`);

--
-- Indexes for table `designations`
--
ALTER TABLE `designations`
 ADD PRIMARY KEY (`id`), ADD KEY `designations_created_by_foreign` (`created_by`), ADD KEY `designations_updated_by_foreign` (`updated_by`), ADD KEY `designations_organization_id_foreign` (`organization_id`), ADD KEY `designations_unit_id_foreign` (`unit_id`), ADD KEY `designations_division_id_foreign` (`division_id`), ADD KEY `designations_department_id_foreign` (`department_id`), ADD KEY `designations_section_id_foreign` (`section_id`), ADD KEY `designations_line_id_foreign` (`line_id`), ADD KEY `designations_designation_group_id_foreign` (`designation_group_id`);

--
-- Indexes for table `designation_groups`
--
ALTER TABLE `designation_groups`
 ADD PRIMARY KEY (`id`), ADD KEY `designation_groups_created_by_foreign` (`created_by`), ADD KEY `designation_groups_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `divisions`
--
ALTER TABLE `divisions`
 ADD PRIMARY KEY (`id`), ADD KEY `divisions_created_by_foreign` (`created_by`), ADD KEY `divisions_updated_by_foreign` (`updated_by`), ADD KEY `divisions_organization_id_foreign` (`organization_id`), ADD KEY `divisions_unit_id_foreign` (`unit_id`);

--
-- Indexes for table `earn_leave_opening_balance`
--
ALTER TABLE `earn_leave_opening_balance`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `earn_leave_opening_balance_employee_id_unique` (`employee_id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
 ADD PRIMARY KEY (`id`), ADD KEY `employees_organization_id_foreign` (`organization_id`), ADD KEY `employees_unit_id_foreign` (`unit_id`), ADD KEY `employees_division_id_foreign` (`division_id`), ADD KEY `employees_department_id_foreign` (`department_id`), ADD KEY `employees_section_id_foreign` (`section_id`), ADD KEY `employees_line_id_foreign` (`line_id`), ADD KEY `employees_designation_group_id_foreign` (`designation_group_id`), ADD KEY `employees_designation_id_foreign` (`designation_id`), ADD KEY `employees_employee_category_id_foreign` (`employee_category_id`), ADD KEY `employees_employee_type_id_foreign` (`employee_type_id`), ADD KEY `employees_holiday_id_foreign` (`holiday_id`), ADD KEY `employees_probation_period_id_foreign` (`probation_period_id`), ADD KEY `employees_created_by_foreign` (`created_by`), ADD KEY `employees_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `employee_categories`
--
ALTER TABLE `employee_categories`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_categories_created_by_foreign` (`created_by`), ADD KEY `employee_categories_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `employee_leave_assigns`
--
ALTER TABLE `employee_leave_assigns`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_leave_assigns_employee_id_foreign` (`employee_id`), ADD KEY `employee_leave_assigns_created_by_foreign` (`created_by`), ADD KEY `employee_leave_assigns_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `employee_org_profiles`
--
ALTER TABLE `employee_org_profiles`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_org_profiles_employee_id_foreign` (`employee_id`), ADD KEY `employee_org_profiles_created_by_foreign` (`created_by`), ADD KEY `employee_org_profiles_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `employee_persona_infos`
--
ALTER TABLE `employee_persona_infos`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_persona_infos_employee_id_foreign` (`employee_id`), ADD KEY `employee_persona_infos_created_by_foreign` (`created_by`), ADD KEY `employee_persona_infos_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `employee_types`
--
ALTER TABLE `employee_types`
 ADD PRIMARY KEY (`id`), ADD KEY `employee_types_created_by_foreign` (`created_by`), ADD KEY `employee_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `holidays`
--
ALTER TABLE `holidays`
 ADD PRIMARY KEY (`id`), ADD KEY `holidays_created_by_foreign` (`created_by`), ADD KEY `holidays_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `holiday_children`
--
ALTER TABLE `holiday_children`
 ADD PRIMARY KEY (`id`), ADD KEY `holiday_children_holiday_type_id_foreign` (`holiday_type_id`), ADD KEY `holiday_children_holiday_id_foreign` (`holiday_id`), ADD KEY `holiday_children_created_by_foreign` (`created_by`), ADD KEY `holiday_children_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `holiday_types`
--
ALTER TABLE `holiday_types`
 ADD PRIMARY KEY (`id`), ADD KEY `holiday_types_created_by_foreign` (`created_by`), ADD KEY `holiday_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `leave_details`
--
ALTER TABLE `leave_details`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_details_leave_register_id_foreign` (`leave_register_id`), ADD KEY `leave_details_leave_type_id_foreign` (`leave_type_id`), ADD KEY `leave_details_created_by_foreign` (`created_by`), ADD KEY `leave_details_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `leave_registers`
--
ALTER TABLE `leave_registers`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_registers_employee_id_foreign` (`employee_id`), ADD KEY `leave_registers_created_by_foreign` (`created_by`), ADD KEY `leave_registers_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `leave_types`
--
ALTER TABLE `leave_types`
 ADD PRIMARY KEY (`id`), ADD KEY `leave_types_created_by_foreign` (`created_by`), ADD KEY `leave_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `lines`
--
ALTER TABLE `lines`
 ADD PRIMARY KEY (`id`), ADD KEY `lines_created_by_foreign` (`created_by`), ADD KEY `lines_updated_by_foreign` (`updated_by`), ADD KEY `lines_organization_id_foreign` (`organization_id`), ADD KEY `lines_unit_id_foreign` (`unit_id`), ADD KEY `lines_division_id_foreign` (`division_id`), ADD KEY `lines_department_id_foreign` (`department_id`), ADD KEY `lines_section_id_foreign` (`section_id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
 ADD PRIMARY KEY (`id`), ADD KEY `menus_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
 ADD PRIMARY KEY (`id`), ADD KEY `organizations_created_by_foreign` (`created_by`), ADD KEY `organizations_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
 ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
 ADD PRIMARY KEY (`id`), ADD KEY `permissions_created_by_foreign` (`created_by`), ADD KEY `permissions_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `probation_periods`
--
ALTER TABLE `probation_periods`
 ADD PRIMARY KEY (`id`), ADD KEY `probation_periods_created_by_foreign` (`created_by`), ADD KEY `probation_periods_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
 ADD PRIMARY KEY (`id`), ADD KEY `roles_created_by_foreign` (`created_by`), ADD KEY `roles_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `role_permission`
--
ALTER TABLE `role_permission`
 ADD PRIMARY KEY (`id`), ADD KEY `role_permission_role_id_foreign` (`role_id`), ADD KEY `role_permission_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `salaries`
--
ALTER TABLE `salaries`
 ADD PRIMARY KEY (`id`), ADD KEY `salaries_employee_id_foreign` (`employee_id`), ADD KEY `salaries_created_by_foreign` (`created_by`), ADD KEY `salaries_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `salary_settings`
--
ALTER TABLE `salary_settings`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_settings_designation_id_foreign` (`designation_id`), ADD KEY `salary_settings_department_id_foreign` (`department_id`), ADD KEY `salary_settings_section_id_foreign` (`section_id`), ADD KEY `salary_settings_employee_type_id_foreign` (`employee_type_id`), ADD KEY `salary_settings_created_by_foreign` (`created_by`), ADD KEY `salary_settings_updated_by_foreign` (`updated_by`), ADD KEY `salary_settings_designation_id_employee_type_id_index` (`designation_id`,`employee_type_id`);

--
-- Indexes for table `salary_setting_children`
--
ALTER TABLE `salary_setting_children`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_setting_children_salary_setting_id_foreign` (`salary_setting_id`), ADD KEY `salary_setting_children_salary_type_id_foreign` (`salary_type_id`);

--
-- Indexes for table `salary_structures`
--
ALTER TABLE `salary_structures`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_structures_employee_id_foreign` (`employee_id`), ADD KEY `salary_structures_created_by_foreign` (`created_by`), ADD KEY `salary_structures_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `salary_structure_children`
--
ALTER TABLE `salary_structure_children`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_structure_children_salary_structure_id_foreign` (`salary_structure_id`), ADD KEY `salary_structure_children_salary_type_id_foreign` (`salary_type_id`);

--
-- Indexes for table `salary_types`
--
ALTER TABLE `salary_types`
 ADD PRIMARY KEY (`id`), ADD KEY `salary_types_created_by_foreign` (`created_by`), ADD KEY `salary_types_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
 ADD PRIMARY KEY (`id`), ADD KEY `sections_created_by_foreign` (`created_by`), ADD KEY `sections_updated_by_foreign` (`updated_by`), ADD KEY `sections_organization_id_foreign` (`organization_id`), ADD KEY `sections_unit_id_foreign` (`unit_id`), ADD KEY `sections_division_id_foreign` (`division_id`), ADD KEY `sections_department_id_foreign` (`department_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
 ADD PRIMARY KEY (`id`), ADD KEY `settings_created_by_foreign` (`created_by`), ADD KEY `settings_updated_by_foreign` (`updated_by`);

--
-- Indexes for table `units`
--
ALTER TABLE `units`
 ADD PRIMARY KEY (`id`), ADD KEY `units_created_by_foreign` (`created_by`), ADD KEY `units_updated_by_foreign` (`updated_by`), ADD KEY `units_organization_id_foreign` (`organization_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
 ADD PRIMARY KEY (`id`), ADD KEY `user_role_user_id_foreign` (`user_id`), ADD KEY `user_role_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `attendances`
--
ALTER TABLE `attendances`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `designations`
--
ALTER TABLE `designations`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=120;
--
-- AUTO_INCREMENT for table `designation_groups`
--
ALTER TABLE `designation_groups`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `divisions`
--
ALTER TABLE `divisions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `earn_leave_opening_balance`
--
ALTER TABLE `earn_leave_opening_balance`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_categories`
--
ALTER TABLE `employee_categories`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `employee_leave_assigns`
--
ALTER TABLE `employee_leave_assigns`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_org_profiles`
--
ALTER TABLE `employee_org_profiles`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_persona_infos`
--
ALTER TABLE `employee_persona_infos`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_types`
--
ALTER TABLE `employee_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `holidays`
--
ALTER TABLE `holidays`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `holiday_children`
--
ALTER TABLE `holiday_children`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `holiday_types`
--
ALTER TABLE `holiday_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `leave_details`
--
ALTER TABLE `leave_details`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leave_registers`
--
ALTER TABLE `leave_registers`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `leave_types`
--
ALTER TABLE `leave_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `lines`
--
ALTER TABLE `lines`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `probation_periods`
--
ALTER TABLE `probation_periods`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `role_permission`
--
ALTER TABLE `role_permission`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salaries`
--
ALTER TABLE `salaries`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_settings`
--
ALTER TABLE `salary_settings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=212;
--
-- AUTO_INCREMENT for table `salary_setting_children`
--
ALTER TABLE `salary_setting_children`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_structures`
--
ALTER TABLE `salary_structures`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_structure_children`
--
ALTER TABLE `salary_structure_children`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `salary_types`
--
ALTER TABLE `salary_types`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `units`
--
ALTER TABLE `units`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `attendances`
--
ALTER TABLE `attendances`
ADD CONSTRAINT `attendances_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `attendances_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `attendances_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `departments`
--
ALTER TABLE `departments`
ADD CONSTRAINT `departments_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `departments_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
ADD CONSTRAINT `departments_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `departments_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `departments_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `designations`
--
ALTER TABLE `designations`
ADD CONSTRAINT `designations_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `designations_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
ADD CONSTRAINT `designations_designation_group_id_foreign` FOREIGN KEY (`designation_group_id`) REFERENCES `designation_groups` (`id`),
ADD CONSTRAINT `designations_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
ADD CONSTRAINT `designations_line_id_foreign` FOREIGN KEY (`line_id`) REFERENCES `lines` (`id`),
ADD CONSTRAINT `designations_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `designations_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
ADD CONSTRAINT `designations_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `designations_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `designation_groups`
--
ALTER TABLE `designation_groups`
ADD CONSTRAINT `designation_groups_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `designation_groups_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `divisions`
--
ALTER TABLE `divisions`
ADD CONSTRAINT `divisions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `divisions_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `divisions_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `divisions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `earn_leave_opening_balance`
--
ALTER TABLE `earn_leave_opening_balance`
ADD CONSTRAINT `earn_leave_opening_balance_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`);

--
-- Constraints for table `employees`
--
ALTER TABLE `employees`
ADD CONSTRAINT `employees_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employees_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
ADD CONSTRAINT `employees_designation_group_id_foreign` FOREIGN KEY (`designation_group_id`) REFERENCES `designation_groups` (`id`),
ADD CONSTRAINT `employees_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`),
ADD CONSTRAINT `employees_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
ADD CONSTRAINT `employees_employee_category_id_foreign` FOREIGN KEY (`employee_category_id`) REFERENCES `employee_categories` (`id`),
ADD CONSTRAINT `employees_employee_type_id_foreign` FOREIGN KEY (`employee_type_id`) REFERENCES `employee_types` (`id`),
ADD CONSTRAINT `employees_holiday_id_foreign` FOREIGN KEY (`holiday_id`) REFERENCES `holidays` (`id`),
ADD CONSTRAINT `employees_line_id_foreign` FOREIGN KEY (`line_id`) REFERENCES `lines` (`id`),
ADD CONSTRAINT `employees_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `employees_probation_period_id_foreign` FOREIGN KEY (`probation_period_id`) REFERENCES `probation_periods` (`id`),
ADD CONSTRAINT `employees_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
ADD CONSTRAINT `employees_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `employees_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employee_categories`
--
ALTER TABLE `employee_categories`
ADD CONSTRAINT `employee_categories_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employee_categories_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employee_leave_assigns`
--
ALTER TABLE `employee_leave_assigns`
ADD CONSTRAINT `employee_leave_assigns_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employee_leave_assigns_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `employee_leave_assigns_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employee_org_profiles`
--
ALTER TABLE `employee_org_profiles`
ADD CONSTRAINT `employee_org_profiles_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employee_org_profiles_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `employee_org_profiles_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employee_persona_infos`
--
ALTER TABLE `employee_persona_infos`
ADD CONSTRAINT `employee_persona_infos_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employee_persona_infos_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `employee_persona_infos_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `employee_types`
--
ALTER TABLE `employee_types`
ADD CONSTRAINT `employee_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `employee_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `holidays`
--
ALTER TABLE `holidays`
ADD CONSTRAINT `holidays_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `holidays_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `holiday_children`
--
ALTER TABLE `holiday_children`
ADD CONSTRAINT `holiday_children_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `holiday_children_holiday_id_foreign` FOREIGN KEY (`holiday_id`) REFERENCES `holidays` (`id`),
ADD CONSTRAINT `holiday_children_holiday_type_id_foreign` FOREIGN KEY (`holiday_type_id`) REFERENCES `holiday_types` (`id`),
ADD CONSTRAINT `holiday_children_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `holiday_types`
--
ALTER TABLE `holiday_types`
ADD CONSTRAINT `holiday_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `holiday_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `leave_details`
--
ALTER TABLE `leave_details`
ADD CONSTRAINT `leave_details_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `leave_details_leave_register_id_foreign` FOREIGN KEY (`leave_register_id`) REFERENCES `leave_registers` (`id`),
ADD CONSTRAINT `leave_details_leave_type_id_foreign` FOREIGN KEY (`leave_type_id`) REFERENCES `leave_types` (`id`),
ADD CONSTRAINT `leave_details_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `leave_registers`
--
ALTER TABLE `leave_registers`
ADD CONSTRAINT `leave_registers_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `leave_registers_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `leave_registers_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `leave_types`
--
ALTER TABLE `leave_types`
ADD CONSTRAINT `leave_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `leave_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `lines`
--
ALTER TABLE `lines`
ADD CONSTRAINT `lines_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `lines_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
ADD CONSTRAINT `lines_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
ADD CONSTRAINT `lines_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `lines_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
ADD CONSTRAINT `lines_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `lines_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `menus`
--
ALTER TABLE `menus`
ADD CONSTRAINT `menus_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `menus` (`id`);

--
-- Constraints for table `organizations`
--
ALTER TABLE `organizations`
ADD CONSTRAINT `organizations_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `organizations_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `permissions`
--
ALTER TABLE `permissions`
ADD CONSTRAINT `permissions_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `permissions_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `probation_periods`
--
ALTER TABLE `probation_periods`
ADD CONSTRAINT `probation_periods_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `probation_periods_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `roles`
--
ALTER TABLE `roles`
ADD CONSTRAINT `roles_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `roles_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `role_permission`
--
ALTER TABLE `role_permission`
ADD CONSTRAINT `role_permission_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`),
ADD CONSTRAINT `role_permission_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`);

--
-- Constraints for table `salaries`
--
ALTER TABLE `salaries`
ADD CONSTRAINT `salaries_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `salaries_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `salaries_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `salary_settings`
--
ALTER TABLE `salary_settings`
ADD CONSTRAINT `salary_settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `salary_settings_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
ADD CONSTRAINT `salary_settings_designation_id_foreign` FOREIGN KEY (`designation_id`) REFERENCES `designations` (`id`),
ADD CONSTRAINT `salary_settings_employee_type_id_foreign` FOREIGN KEY (`employee_type_id`) REFERENCES `employee_types` (`id`),
ADD CONSTRAINT `salary_settings_section_id_foreign` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`),
ADD CONSTRAINT `salary_settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `salary_setting_children`
--
ALTER TABLE `salary_setting_children`
ADD CONSTRAINT `salary_setting_children_salary_setting_id_foreign` FOREIGN KEY (`salary_setting_id`) REFERENCES `salary_settings` (`id`),
ADD CONSTRAINT `salary_setting_children_salary_type_id_foreign` FOREIGN KEY (`salary_type_id`) REFERENCES `salary_types` (`id`);

--
-- Constraints for table `salary_structures`
--
ALTER TABLE `salary_structures`
ADD CONSTRAINT `salary_structures_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `salary_structures_employee_id_foreign` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`),
ADD CONSTRAINT `salary_structures_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `salary_structure_children`
--
ALTER TABLE `salary_structure_children`
ADD CONSTRAINT `salary_structure_children_salary_structure_id_foreign` FOREIGN KEY (`salary_structure_id`) REFERENCES `salary_structures` (`id`),
ADD CONSTRAINT `salary_structure_children_salary_type_id_foreign` FOREIGN KEY (`salary_type_id`) REFERENCES `salary_types` (`id`);

--
-- Constraints for table `salary_types`
--
ALTER TABLE `salary_types`
ADD CONSTRAINT `salary_types_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `salary_types_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `sections`
--
ALTER TABLE `sections`
ADD CONSTRAINT `sections_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `sections_department_id_foreign` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
ADD CONSTRAINT `sections_division_id_foreign` FOREIGN KEY (`division_id`) REFERENCES `divisions` (`id`),
ADD CONSTRAINT `sections_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `sections_unit_id_foreign` FOREIGN KEY (`unit_id`) REFERENCES `units` (`id`),
ADD CONSTRAINT `sections_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `settings`
--
ALTER TABLE `settings`
ADD CONSTRAINT `settings_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `settings_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `units`
--
ALTER TABLE `units`
ADD CONSTRAINT `units_created_by_foreign` FOREIGN KEY (`created_by`) REFERENCES `users` (`id`),
ADD CONSTRAINT `units_organization_id_foreign` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`),
ADD CONSTRAINT `units_updated_by_foreign` FOREIGN KEY (`updated_by`) REFERENCES `users` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
ADD CONSTRAINT `user_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`),
ADD CONSTRAINT `user_role_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
